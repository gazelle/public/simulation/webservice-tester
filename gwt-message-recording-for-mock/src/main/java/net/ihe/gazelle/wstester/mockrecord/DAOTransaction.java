package net.ihe.gazelle.wstester.mockrecord;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;


class DAOTransaction {

    private DBConnection connection;
    private int requestId;
    private int responseId;


    DAOTransaction(DBConnection connection) {
        this.connection = connection;
    }


    private int getRequestId() {
        return requestId;
    }

    private void setRequestId(int requestId) {
        this.requestId = requestId;
    }

    private int getResponseId() {
        return responseId;
    }

    private void setResponseId(int responseId) {
        this.responseId = responseId;
    }


    /**
     * insertRequest messages in table
     *
     * @throws MessageException
     */
    void insertRequest(Message message) throws MessageException {

        String SQL = "INSERT INTO cmn_message_instance (id, content, issuer, issuer_ip_address, type, validation_detailed_result, validation_status, issuing_actor) "
                + "VALUES (nextval('cmn_message_instance_id_seq'),?,?,?,?, null, null,?) returning id;";

        try {
            DAOModel daoModel = new DAOModel(connection);
            Connection conn = connection.getConnection();
            try (PreparedStatement st = conn.prepareStatement(SQL)) {

                st.setBytes(1, message.getContent());
                st.setString(2, message.getIssuer());
                st.setString(3, message.getIssuerIP());
                st.setString(4, message.getMessageType());
                st.setInt(5, daoModel.getOrCreateActorId(message.getIssuingActor()));
                try (ResultSet resultSet = st.executeQuery()) {

                    if (resultSet.next()) {
                        this.setRequestId(resultSet.getInt("id"));
                    }
                }
            }
        } catch (Exception e) {
            throw new MessageException("An error occurred while recording the request message : " + e.getMessage());
        }
    }

    /**
     * insert Response messages in table
     *
     * @throws MessageException
     */
    void insertResponse(Message message) throws MessageException {

        String SQL = "INSERT INTO cmn_message_instance (id, content, issuer, issuer_ip_address, type, validation_detailed_result, validation_status, issuing_actor) "
                + "VALUES (nextval('cmn_message_instance_id_seq'),?,?,?,?, null, null,?) returning id;";

        try {
            DAOModel daoModel = new DAOModel(connection);
            Connection conn = connection.getConnection();
            try (PreparedStatement st = conn.prepareStatement(SQL)) {

                st.setBytes(1, message.getContent());
                st.setString(2, message.getIssuer());
                st.setString(3, message.getIssuerIP());
                st.setString(4, message.getMessageType());
                st.setInt(5, daoModel.getOrCreateActorId(message.getIssuingActor()));
                try (ResultSet resultSet = st.executeQuery()) {

                    if (resultSet.next()) {
                        this.setResponseId(resultSet.getInt("id"));
                    }
                }
            }
        } catch (Exception e) {
            throw new MessageException("An error occurred while recording the response message : " + e.getMessage());
        }
    }


    /**
     * insert transaction in table
     *
     * @throws MessageException
     */
    void insertTransactionInstance(Transaction transaction) throws MessageException {

        this.insertRequest(transaction.getRequest());
        this.insertResponse(transaction.getResponse());

        String SQL = "INSERT INTO cmn_transaction_instance (id, company_keyword, standard, timestamp, is_visible, domain_id, request_id, response_id, simulated_actor_id, transaction_id) "
                + "VALUES (nextval('cmn_transaction_instance_id_seq'), null, ?, ?, ?, ?, ?, ?, ?, ? );";
        try {
            DAOModel daoModel = new DAOModel(connection);
            Connection conn = connection.getConnection();
            try (PreparedStatement st = conn.prepareStatement(SQL)) {

                st.setString(1, transaction.getStandard().name());
                st.setTimestamp(2, transaction.getTimestamp());
                st.setBoolean(3, true);
                st.setInt(4, daoModel.getOrCreateDomainId(transaction.getDomainKeyword()));
                st.setInt(5, this.getRequestId());
                st.setInt(6, this.getResponseId());
                st.setInt(7, daoModel.getOrCreateActorId(transaction.getSimulatedActorKeyword()));
                st.setInt(8, daoModel.getOrCreateTransactionId(transaction.getTransactionKeyword()));
                st.executeUpdate();

            }
        } catch (Exception e) {
            throw new MessageException("An error occurred while recording the transaction" + e.getMessage());
        }
    }


}
