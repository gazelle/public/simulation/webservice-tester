package net.ihe.gazelle.wstester.mockrecord;


public class Message {

    private String issuer;
    private String issuerIP;
    private String messageType;
    private String issuingActor;
    private byte[] content;


    public Message(String issuer, String issuerIP, String messageType, String issuingActor, byte[] content) {
        this.issuer = issuer;
        this.issuerIP = issuerIP;
        this.messageType = messageType;
        this.issuingActor = issuingActor;
        this.content = content;
    }

    public String getIssuer() {
        return issuer;
    }

    public String getIssuerIP() {
        return issuerIP;
    }

    public String getMessageType() {
        return messageType;
    }

    public String getIssuingActor() {
        return issuingActor;
    }

    public byte[] getContent() {
        return content;
    }


}
