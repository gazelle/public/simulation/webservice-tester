package net.ihe.gazelle.wstester.mockrecord;

import java.sql.Timestamp;
import java.util.Date;

public class MessageRecorder {

    private String urlDB;
    private String userDB;
    private String passwordDB;


    public MessageRecorder(String urlDB, String userDB, String passwordDB) {
        this.urlDB = urlDB;
        this.userDB = userDB;
        this.passwordDB = passwordDB;
    }

    public void record(EStandard standardKeyword, String transactionKeyword, String domainKeyword, String simulatedActorKeyword, Message request, Message response) throws MessageException {

        try {
            Date date = new Date();
            Timestamp timestamp = new Timestamp(date.getTime());
            DBConnection dbConnection = new DBConnection(urlDB, userDB, passwordDB);
            DAOTransaction daoTransaction = new DAOTransaction(dbConnection);
            Transaction transaction = new Transaction(standardKeyword, timestamp, domainKeyword, simulatedActorKeyword, transactionKeyword, request, response);
            daoTransaction.insertTransactionInstance(transaction);
            dbConnection.close();

        } catch (Exception e) {
            throw new MessageException("An error occurred while inserting the transaction : " + e.getMessage());
        }
    }


}
