package net.ihe.gazelle.wstester.mockrecord;

public class MessageException extends Exception {

    public MessageException(String s) {
        super(s);
    }

}
