package net.ihe.gazelle.wstester.mockrecord;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

class DBConnection {

    private String url;
    private String user;
    private String password;
    private Connection connection;


    DBConnection(String url, String user, String password) {
        this.url = url;
        this.user = user;
        this.password = password;
    }

    void connect() throws SQLException {
        connection = DriverManager.getConnection(url, user, password);
    }

    void close() throws SQLException {
        getConnection().close();
    }


    Connection getConnection() throws SQLException {
        if (connection == null) {
            connect();
        }
        return connection;
    }

}
