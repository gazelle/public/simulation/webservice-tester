package net.ihe.gazelle.wstester.mockrecord;


import java.sql.Timestamp;

class Transaction {

    private EStandard standard;
    private Timestamp timestamp;
    private String domainKeyword;
    private String simulatedActorKeyword;
    private String transactionKeyword;
    private Message request;
    private Message response;


    Transaction(EStandard standard, Timestamp timestamp, String domainKeyword, String simulatedActorKeyword, String transactionKeyword, Message request, Message response) {
        this.standard = standard;
        this.timestamp = timestamp;
        this.domainKeyword = domainKeyword;
        this.simulatedActorKeyword = simulatedActorKeyword;
        this.transactionKeyword = transactionKeyword;
        this.request = request;
        this.response = response;
    }


    EStandard getStandard() {
        return standard;
    }

    Timestamp getTimestamp() {
        return timestamp;
    }

    String getDomainKeyword() {
        return domainKeyword;
    }

    String getSimulatedActorKeyword() {
        return simulatedActorKeyword;
    }

    String getTransactionKeyword() {
        return transactionKeyword;
    }

    Message getRequest() {
        return request;
    }

    Message getResponse() {
        return response;
    }

}
