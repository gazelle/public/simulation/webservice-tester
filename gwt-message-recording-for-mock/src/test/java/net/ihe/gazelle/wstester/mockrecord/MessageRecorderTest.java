package net.ihe.gazelle.wstester.mockrecord;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.Timestamp;
import java.util.Date;

@RunWith(MockitoJUnitRunner.class)
public class MessageRecorderTest {


    @Test
    public void TransactionTest() {
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        Message request = new Message("Consumer", "127.0.0.1", "SubmitObjectsRequest", "Consumer", null);
        Message response = new Message("Provider", "soapui mock", "RegistryResponse", "Provider", null);
        Transaction transaction = new Transaction(EStandard.XDS, timestamp, "UNKNOWN", "simulatedActorKeyword", "ITI", request, response);
        Assert.assertEquals("XDS", transaction.getStandard().name());
        Assert.assertEquals("UNKNOWN", transaction.getDomainKeyword());
        Assert.assertEquals("simulatedActorKeyword", transaction.getSimulatedActorKeyword());
        Assert.assertEquals("ITI", transaction.getTransactionKeyword());
    }


    @Test
    public void MessageTest() {
        Message message = new Message("Consumer", "127.0.0.1", "SubmitObjectsRequest", "Consumer", null);
        Assert.assertEquals(null, message.getContent());
        Assert.assertEquals("127.0.0.1", message.getIssuerIP());
        Assert.assertEquals("SubmitObjectsRequest", message.getMessageType());
        Assert.assertEquals("Consumer", message.getIssuingActor());
        Assert.assertEquals("Consumer", message.getIssuer());
    }


//    @Test
//    public void MessageRecorderTest() throws MessageException {
//        MessageRecorder messageRecorder = new MessageRecorder("", "", "");
//        Message request = new Message("Consumer", "127.0.0.1", "SubmitObjectsRequest", "Consumer", null);
//        Message response = new Message("Provider", "soapui mock", "RegistryResponse", "Provider", null);
//        messageRecorder.record(EStandard.XDS, "ADR", "CH", "CONSUMER", request, response);
//    }


//    /**
//     * TEST
//     *
//     * @throws java.sql.SQLException
//     */
//    public static void main(String args[]) throws SQLException, NotUniqueException {
//
//        DBConnection mockedDbConnection = new DBConnection("jdbc:postgresql://localhost:5432/gazelle-webservice-tester", "gazelle", "gazelle");
//        DAOTransaction daoTransaction = new DAOTransaction(mockedDbConnection);
//
//        Date date = new Date();
//        Timestamp ts = new Timestamp(date.getTime());
//
//        Message request = new Message("Test", "127.0.0.1", "soap:Fault", "test", null);
//        Message response = new Message("Test", "soap mock", "soap:Fault", "test", null);
//
//        Transaction transaction = new Transaction("standardKeyword", ts, "", "simulatedActorKeyword", "transactionKeyword", request, response);
//
//        daoTransaction.insertRequest(request);
//        daoTransaction.insertResponse(response);
//        daoTransaction.insertTransactionInstance(transaction);
//
//    }


}
