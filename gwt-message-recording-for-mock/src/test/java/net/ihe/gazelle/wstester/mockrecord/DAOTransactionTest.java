package net.ihe.gazelle.wstester.mockrecord;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.sql.DataSource;
import java.sql.*;
import java.util.Date;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DAOTransactionTest {

    @Mock
    private DataSource ds;
    @Mock
    private Connection c;
    @Mock
    private PreparedStatement stmt;
    @Mock
    private ResultSet rs;

    private DBConnection mockedDbConnection;


    @Before
    public void setUp() throws SQLException {
        assertNotNull(ds);
        mockedDbConnection = mock(DBConnection.class);
        when(c.prepareStatement(any(String.class))).thenReturn(stmt);
        when(mockedDbConnection.getConnection()).thenReturn(c);
        when(ds.getConnection()).thenReturn(c);
        when(rs.next()).thenReturn(true);
        when(rs.isLast()).thenReturn(true);
        when(stmt.executeQuery()).thenReturn(rs);
    }


    @Test
    public void insertRequestTest() throws MessageException, SQLException {
        DAOTransaction daoTransaction = new DAOTransaction(mockedDbConnection);
        Message request = new Message("Consumer", "127.0.0.1", "SubmitObjectsRequest", "Consumer", null);
        daoTransaction.insertRequest(request);
        Assert.assertEquals(rs.isClosed(), false);
    }

    @Test
    public void insertResponseTest() throws MessageException, SQLException {
        DAOTransaction daoTransaction = new DAOTransaction(mockedDbConnection);
        Message response = new Message("Provider", "soapui mock", "RegistryResponse", "Provider", null);
        daoTransaction.insertResponse(response);
        Assert.assertEquals(rs.isClosed(), false);
    }

    @Test
    public void insertTransactionInstanceTest() throws MessageException, SQLException {
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        Message request = new Message("Consumer", "127.0.0.1", "SubmitObjectsRequest", "Consumer", null);
        Message response = new Message("Provider", "soapui mock", "RegistryResponse", "Provider", null);
        Transaction transaction = new Transaction(EStandard.XDS, timestamp, "UNKNOWN", "simulatedActorKeyword", "ITI", request, response);
        DAOTransaction daoTransaction = new DAOTransaction(mockedDbConnection);
        daoTransaction.insertTransactionInstance(transaction);
        Assert.assertEquals(rs.isClosed(), false);
    }


}