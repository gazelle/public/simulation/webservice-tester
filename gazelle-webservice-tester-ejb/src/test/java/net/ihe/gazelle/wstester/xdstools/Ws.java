package net.ihe.gazelle.wstester.xdstools;

import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.wstester.xdstools.model.CallingTool;
import net.ihe.gazelle.wstester.xdstools.model.TestInstanceTm;
import net.ihe.gazelle.wstester.xdstools.ws.Xdstools;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.BufferedReader;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

@RunWith(PowerMockRunner.class)
@PrepareForTest({CallingTool.class, TestInstanceTm.class, PreferenceService.class, URL.class, BufferedReader.class, HttpURLConnection.class, InputStream.class})
@PowerMockIgnore({"javax.management.*"})
public class Ws {

    BufferedReader bufferedReader;
    HttpURLConnection httpURLConnection;
    URL url;
    InputStream inputStream;

    @Before
    public void setUp() throws Exception {
        PowerMockito.mockStatic(CallingTool.class);
        PowerMockito.mockStatic(TestInstanceTm.class);
        PowerMockito.mockStatic(PreferenceService.class);
        bufferedReader = PowerMockito.mock(BufferedReader.class);
        inputStream = PowerMockito.mock(InputStream.class);
        url = PowerMockito.mock(URL.class);
        httpURLConnection = PowerMockito.mock(HttpURLConnection.class);
    }

    @Test
    public void getNonceWithUnknownCallingToolOid() throws Exception {
        //Mock getToolByOid
        PowerMockito.when(CallingTool.getToolByOid("test")).thenReturn(null);

        Xdstools xdstools = new Xdstools();
        String result = xdstools.getNonce(1, "1");
        Assert.assertEquals("Error : there is no calling tool with this OID", result);
    }

    @Test
    public void noNonceReturned() throws Exception {
        int testInstanceId = 1;
        int testingSession = 1;
        String callingToolOid = "test";
        String systemKeyword = "TEST_TEST";

        //Mock getToolByOid
        CallingTool callingTool = new CallingTool();
        PowerMockito.when(CallingTool.getToolByOid("test")).thenReturn(callingTool);

        //Mock getInstanceTM
        PowerMockito.when(TestInstanceTm.getTestInstance(testingSession, systemKeyword, callingTool)).thenReturn(null);

        //Mock getPreference
        PowerMockito.when(PreferenceService.getString("xdstools_url")).thenReturn("http://localhost/xdstools/");

        //Mock URL
        PowerMockito.whenNew(URL.class).withArguments("http://localhost/xdstools5/CasSessionBuilder?testingSessionId=" + testingSession + ";systemName=" + systemKeyword).thenReturn(null);
        PowerMockito.when(url.openConnection()).thenReturn(httpURLConnection);
        PowerMockito.when(httpURLConnection.getInputStream()).thenReturn(inputStream);

        //Mock BufferedReader
        PowerMockito.whenNew(BufferedReader.class).withAnyArguments().thenReturn(bufferedReader);
        PowerMockito.when(bufferedReader.readLine()).thenReturn("test");

        Xdstools xdstools = new Xdstools();
        String result = xdstools.getNonce(testInstanceId, callingToolOid);
        Assert.assertEquals("Error : Could not retrieve the testing session or the system keyword from Gazelle TM", result);
    }


}



