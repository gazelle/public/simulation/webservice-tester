package net.ihe.gazelle.wstester.action;

import net.ihe.gazelle.wstester.model.GwtTestStepResultTest;
import net.ihe.gazelle.wstester.util.DateUtilTest;
import net.ihe.gazelle.wstester.xdstools.Ws;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({RunInstanceActionTest2.class, RunInstanceActionTest.class, GwtTestStepResultTest.class, DateUtilTest.class, Ws.class})
public class AllTests {

}
