package net.ihe.gazelle.wstester.action;

import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.wstester.model.Execution;
import net.ihe.gazelle.wstester.model.TestInstanceResult;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ApplicationConfiguration.class})
@PowerMockIgnore({"org.xml.*", "org.w3c.*", "javax.xml.*", "javax.net.ssl.*","javax.security.*"})
public class RunInstanceActionTest2 {

    @Before
    public void setUp() throws Exception {
        mockApplicationConfiguration();
    }

    @After
    public void tearDown() throws Exception {
    }

    /*@Test
    public void getSystemKeywordInfoOK() {
        RunInstanceAction runInstanceAction = new RunInstanceAction();
        runInstanceAction.setTestInstanceId(36575);
        Execution execution = new Execution();
        runInstanceAction.setExecution(execution);
        execution.setTestInstanceResult(new TestInstanceResult());
        List<String> transaction = Arrays.asList("ITI-18");
        String res = runInstanceAction.getSystemKeywordInfo(transaction).get(0).getResponderSystemKeyword();
        Assert.assertEquals("Core Community Component_Post_0", res);
    }*/

    @Test
    public void getEndPointBySystemOK() {
        StepInfo stepInfo = new StepInfo("ITI-18", 2133341);
        stepInfo.setTestRoleInitiatorId(2133346);
        stepInfo.setParticipantRoleInTestId(13017);
        stepInfo.setResponderSystemKeyword("Core Community Component_Post_0");
        stepInfo.setInitiatorSystemKeyword("HPP_DOCUMENT_CONSUMER");
        stepInfo.setRoleInTest("CC_INITIATING_GATEWAY");
        stepInfo.setTransactionUsage("Stored Query:sq.b");
        List<StepInfo> stepInfoList = new ArrayList<>();
        stepInfoList.add(stepInfo);

        RunInstanceAction runInstanceAction = new RunInstanceAction();
        runInstanceAction.getEndPointBySystem(stepInfoList);

        Assert.assertEquals("http://post0:28070/SpiritXDSRegistry/Reg1", stepInfoList.get(0).getEndpoint());
    }

    private void mockApplicationConfiguration() {
        PowerMockito.mockStatic(ApplicationConfiguration.class);
        when(ApplicationConfiguration.getValueOfVariable("tm_url")).thenReturn("https://ehealthsuisse.ihe-europe.net/gazelle/");
    }


}
