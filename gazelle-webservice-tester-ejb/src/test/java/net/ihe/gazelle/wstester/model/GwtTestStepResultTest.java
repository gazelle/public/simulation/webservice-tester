package net.ihe.gazelle.wstester.model;

import org.junit.Assert;
import org.junit.Test;

public class GwtTestStepResultTest {

    @Test
    public void getStatusForCssOK() {
        GwtTestStepResult gt = new GwtTestStepResult();
        gt.setStatus("OK");
        Assert.assertEquals("PASSED", gt.getStatusForCss());
    }

    @Test
    public void getStatusForCssKO() {
        GwtTestStepResult gt = new GwtTestStepResult();
        gt.setStatus("KO");
        Assert.assertEquals("KO", gt.getStatusForCss());
    }

    @Test
    public void getStatusForCssNull() {
        GwtTestStepResult gt = new GwtTestStepResult();
        gt.setStatus(null);
        Assert.assertEquals("UNKNOWN", gt.getStatusForCss());
    }

}