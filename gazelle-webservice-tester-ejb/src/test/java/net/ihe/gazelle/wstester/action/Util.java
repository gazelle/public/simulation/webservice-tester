package net.ihe.gazelle.wstester.action;

import net.ihe.gazelle.wstester.util.Application;
import org.junit.Assert;
import org.junit.Test;

public class Util {

    @Test
    public void getTMDomain() throws Exception {
        String url = "https://ehealthsuisse.ihe-europe.net/gazelle/";
        String actual = Application.getTMDomain(url);
        String expected = "https://ehealthsuisse.ihe-europe.net/";
        Assert.assertEquals(expected, actual);
    }
}
