package net.ihe.gazelle.wstester.util;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtilTest {

    Date date = null;

    @Before
    public void setUp() throws Exception {
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS");
        try {
            date = df.parse("01/02/2018 15:20:30.500");
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void addMsToDate1() {
        Assert.assertEquals("01/02/2018 15:20:30.502", DateUtil.dateToString(DateUtil.addMsToDate(date,2)));
    }

    @Test
    public void addMsToDate2() {
        Assert.assertEquals("01/02/2018 15:20:31.002", DateUtil.dateToString(DateUtil.addMsToDate(date,502)));
    }

    @Test
    public void dateToString() {
        Assert.assertEquals("01/02/2018 15:20:30.500", DateUtil.dateToString(date));
    }
}
