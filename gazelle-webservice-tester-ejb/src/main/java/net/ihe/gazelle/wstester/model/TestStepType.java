package net.ihe.gazelle.wstester.model;

public enum TestStepType {

    SOAP,
    REST
}
