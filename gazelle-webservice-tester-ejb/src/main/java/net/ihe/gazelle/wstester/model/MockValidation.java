package net.ihe.gazelle.wstester.model;

import net.ihe.gazelle.evsclient.connector.api.EVSClientResults;
import net.ihe.gazelle.evsclient.connector.api.EVSClientServletConnector;
import net.ihe.gazelle.evsclient.connector.model.EVSClientValidatedObject;
import net.ihe.gazelle.evsclient.connector.utils.PartSourceUtils;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.simulator.message.model.MessageInstance;
import net.ihe.gazelle.simulator.message.model.MessageInstanceDAO;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.simulator.message.model.TransactionInstanceDAO;
import net.ihe.gazelle.wstester.action.MockMessageDisplay;
import org.apache.commons.httpclient.methods.multipart.PartSource;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import java.io.IOException;
import java.io.Serializable;

public class MockValidation implements EVSClientValidatedObject, Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(MockMessageDisplay.class);

    private MessageInstance selectedMessageInstanceToValidate;

    private MockValidationResult mockValidationResult;

    public MessageInstance getSelectedMessageInstanceToValidate() {
        return selectedMessageInstanceToValidate;
    }

    public void setSelectedMessageInstanceToValidate(MessageInstance selectedMessageInstanceToValidate) {
        this.selectedMessageInstanceToValidate = selectedMessageInstanceToValidate;
    }

    public MockValidation() {
    }

    public MockValidation(MessageInstance selectedMessageInstanceToValidate) {
        this.selectedMessageInstanceToValidate = selectedMessageInstanceToValidate;
    }

    /**
     * Init method called by MockMessageDisplay bean when a validation has been performed
     *
     * @param messageInstanceId Selected message instance id
     * @param validationOid Oid of the validation report
     *
     */
    public void init(String messageInstanceId, String validationOid) {

            EntityManager entityManager = EntityManagerService.provideEntityManager();

            selectedMessageInstanceToValidate = entityManager.find(MessageInstance.class, Integer.parseInt(messageInstanceId));
            if (selectedMessageInstanceToValidate == null) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The given id does not match any message instance in the database");
            } else {
                if (validationOid != null) {
                    MockValidationResult validationResult = new MockValidationResult(selectedMessageInstanceToValidate, validationOid);
                    getResultDetails(validationResult);
                }
            }

            TransactionInstance transactionInstance = TransactionInstanceDAO.getMockTransactionInstanceByMessageIdAndIssuer
                    (selectedMessageInstanceToValidate.getId(), selectedMessageInstanceToValidate.getIssuer());

            try {
                if (selectedMessageInstanceToValidate != null) {
                    FacesContext.getCurrentInstance().getExternalContext().redirect("/gazelle-webservice-tester/mock/mockMessageDisplay.seam?id="
                            + transactionInstance.getId());
                }
            } catch (IOException e) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Unable to redirect to the selected test instance");
                LOG.error(e.getMessage());
            }
    }

    /**
     * Validate a message instance with evs client connector redirection
     *
     *
     */
    public void validateMessage() {
        String evsClientUrl = ApplicationConfiguration.getValueOfVariable("evs_client_url");
        String toolOid = ApplicationConfiguration.getValueOfVariable("tool_instance_oid_mock");
        if (evsClientUrl == null) {
            LOG.error("evs_client_url is missing in app_configuration table");
        } else if (toolOid == null) {
            LOG.error("tool_instance_oid_mock is missing in app_configuration table");
        } else {
            ExternalContext extContext = FacesContext.getCurrentInstance().getExternalContext();
            EVSClientServletConnector.sendToValidation(this, extContext, evsClientUrl, toolOid);
        }
    }

    /**
     * Retrieve validation result
     *
     * @param validationResult Validation result
     *
     */
    private void getResultDetails(final MockValidationResult validationResult) {
        String evsclientUrl = ApplicationConfiguration.getValueOfVariable("evs_client_url");
        String validationDate = EVSClientResults.getValidationDate(validationResult.getOid(), evsclientUrl);
        validationResult.setValidationDate(validationDate);
        String permanentLink = EVSClientResults.getValidationPermanentLink(validationResult.getOid(), evsclientUrl);
        validationResult.setPermanentLink(permanentLink);
        mockValidationResult = validationResult.save();
        String validationStatus = EVSClientResults.getValidationStatus(validationResult.getOid(), evsclientUrl);
        if (validationStatus != null) {
            selectedMessageInstanceToValidate.setValidationStatus(validationStatus);
            selectedMessageInstanceToValidate = MessageInstanceDAO.save(selectedMessageInstanceToValidate);
        }
    }

    @Override
    public String getUniqueKey() {
        if (selectedMessageInstanceToValidate != null) {
            return selectedMessageInstanceToValidate.getId().toString();
        } else {
            return null;
        }
    }

    @Override
    public PartSource getPartSource() {
        if (selectedMessageInstanceToValidate != null && selectedMessageInstanceToValidate.getContent() != null) {
            return PartSourceUtils.buildEncoded64PartSource(selectedMessageInstanceToValidate.getContent(), getType());
        } else {
            return null;
        }
    }

    @Override
    public String getType() {
        return "xml";
    }
}
