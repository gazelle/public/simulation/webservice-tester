package net.ihe.gazelle.wstester.model;

import net.ihe.gazelle.evsclient.connector.api.EVSClientResults;
import net.ihe.gazelle.evsclient.connector.model.EVSClientCrossValidatedObject;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.wstester.action.GwtProjectXValidation;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "gwt_project_result")
@SequenceGenerator(name = "gwt_project_result_sequence", sequenceName = "gwt_project_result_id_seq", allocationSize = 1)
public class GwtProjectResult implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(GwtProjectResult.class);

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gwt_project_result_sequence")
    private Integer id;

    @ManyToOne(targetEntity = GwtProject.class)
    @JoinColumn(name = "project_id")
    private GwtProject project;

    @Basic(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_validator_oid_index")
    private String xvalidatorOidIndex;

    @Basic(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_validator_oid")
    private String xvalidatorOid;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "x_validation_result_id")
    private XValidationResult xvalidationResult;

    @ManyToOne(fetch = FetchType.EAGER,
            cascade = {CascadeType.ALL})
    @JoinColumn(name = "execution_id")
    @Fetch(value = FetchMode.SELECT)
    private Execution execution;

    @OneToMany(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private List<TestStepInfo> testStepInfoList;

    @OneToMany(fetch = FetchType.EAGER,
            cascade = CascadeType.ALL)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<XValidatorInputInfo> xValidatorInputInfoList;

    public GwtProjectResult() {
        super();
    }

    public GwtProjectResult(GwtProject project, String xvalidatorOidIndex, String xvalidatorOid, Execution execution) {
        this.project = project;
        this.xvalidatorOidIndex = xvalidatorOidIndex;
        this.xvalidatorOid = xvalidatorOid;
        this.execution = execution;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public XValidationResult getXvalidationResult() {
        return xvalidationResult;
    }

    public void setXvalidationResult(XValidationResult xvalidationResult) {
        this.xvalidationResult = xvalidationResult;
    }

    public String getXvalidatorOidIndex() {
        return xvalidatorOidIndex;
    }

    public void setXvalidatorOidIndex(String xvalidatorOidIndex) {
        this.xvalidatorOidIndex = xvalidatorOidIndex;
    }

    public String getXvalidatorOid() {
        return xvalidatorOid;
    }

    public void setXvalidatorOid(String xvalidatorOid) {
        this.xvalidatorOid = xvalidatorOid;
    }

    public GwtProject getProject() {
        return project;
    }

    public void setProject(GwtProject project) {
        this.project = project;
    }

    public Execution getExecution() {
        return execution;
    }

    public void setExecution(Execution execution) {
        this.execution = execution;
    }

    public List<TestStepInfo> getTestStepInfoList() {
        return testStepInfoList;
    }

    public void setTestStepInfoList(List<TestStepInfo> testStepInfoList) {
        this.testStepInfoList = testStepInfoList;
    }

    public void addTestStepInfoToList(List<TestStepInfo> testStepinfoList) {
        for (TestStepInfo testStepInfo : testStepinfoList) {
            if (testStepInfo != null) {
                this.testStepInfoList.add(testStepInfo);
            }
        }
    }

    public List<XValidatorInputInfo> getxValidatorInputInfoList() {
        return xValidatorInputInfoList;
    }

    public void setxValidatorInputInfoList(List<XValidatorInputInfo> xValidatorInputInfoList) {
        this.xValidatorInputInfoList = xValidatorInputInfoList;
    }

    public void addxValidatorInputInfoToList(List<XValidatorInputInfo> xValidatorInputInfoList) {
        for (XValidatorInputInfo xValidatorInputInfo : xValidatorInputInfoList) {
            if (xValidatorInputInfo != null) {
                this.xValidatorInputInfoList.add(xValidatorInputInfo);
            }
        }
    }

    public void xvalidate(String xvalidatorOid) {

        if (xvalidatorOid != null) {
            if (project.getId().toString() != null) {//TODO CHECK
                if (!xValidatorInputInfoList.isEmpty()) {
                    GwtProjectXValidation gwtProjectXValidation = new GwtProjectXValidation(this, new ArrayList<EVSClientCrossValidatedObject>(xValidatorInputInfoList));
                    try {
                        gwtProjectXValidation.xvalidate();
                    } catch (IOException e) {
                        LOG.error("Error with xvalidate()" + e.getMessage());
                        FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Error while calling function xvalidate()");
                    }
                } else {
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR, "There isn't any content to cross validate : " +
                            "please make sure you have defined test steps for this oid in your SoapUI project");
                }
            } else {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "This teststep result's id is null");
            }
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The oid must be null, please make sure there is a well formed one before calling x validation");
        }
    }

    public String getValidatorNameFromOid() {
        String evsclientUrl = ApplicationConfiguration.getValueOfVariable("evs_client_url");
        return EVSClientResults.getXValidatorName(xvalidatorOid, evsclientUrl);
    }

    public String getValidatorDescriptionFromOid() {
        String evsclientUrl = ApplicationConfiguration.getValueOfVariable("evs_client_url");
        String description = EVSClientResults.getXValidatorDescription(xvalidatorOid, evsclientUrl);
        if (description != null && !description.isEmpty()) {
            return description;
        } else {
            return "DOES NOT EXIST";

        }
    }


    public String redirectToXValidatorDocFromOid(String oid) {

        String evsClientUrl = ApplicationConfiguration.getValueOfVariable("evs_client_url");
        String toolOid = ApplicationConfiguration.getValueOfVariable("tool_instance_oid");

        if (evsClientUrl == null) {
            LOG.error("evs_client_url is missing in app_configuration table");
        } else if (toolOid == null) {
            LOG.error("tool_instance_oid is missing in app_configuration table");
        } else {
            StringBuilder url = new StringBuilder(evsClientUrl);
            url.append("xvalidation/doc/validator.seam");
            url.append("?oid=");
            url.append(oid);
            return String.valueOf(url);
        }
        return null;
    }


    public GwtProjectResult save() {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        GwtProjectResult result = entityManager.merge(this);
        entityManager.flush();
        return result;
    }


}
