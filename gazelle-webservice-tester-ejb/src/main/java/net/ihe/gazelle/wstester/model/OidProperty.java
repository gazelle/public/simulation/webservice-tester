package net.ihe.gazelle.wstester.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name = "oid_property_info")
@SequenceGenerator(name = "oid_property_info_sequence", sequenceName = "oid_property_info_id_seq", allocationSize = 1)
public class OidProperty {


    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "oid_property_info_sequence")
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "gwt_project_id")
    private GwtProject gwtProject;

    @Column(name = "index")
    private String index;

    @Column(name = "oid")
    private String oid;

    public OidProperty() {super();}

    public OidProperty(GwtProject gwtProject, String index, String oid) {
        this.gwtProject = gwtProject;
        this.index = index;
        this.oid = oid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public GwtProject getGwtProject() {
        return gwtProject;
    }

    public void setGwtProject(GwtProject gwtProject) {
        this.gwtProject = gwtProject;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }




}


