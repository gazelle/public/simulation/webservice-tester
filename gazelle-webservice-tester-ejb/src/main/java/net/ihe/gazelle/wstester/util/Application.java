package net.ihe.gazelle.wstester.util;

import net.ihe.gazelle.ssov7.authn.interlay.adapter.GazelleIdentityImpl;
import org.jboss.seam.security.Identity;

public class Application {

    public static String getUser() {
        String user;
        if (Identity.instance().isLoggedIn()) {
            user = GazelleIdentityImpl.instance().getDisplayName();
        } else {
            user = "anonymous";
        }
        return user;
    }

    public static String getCompanyKeyword() {
        return GazelleIdentityImpl.instance().getOrganisationKeyword();
    }

    public static String getTMDomain(String url) {
        return url.replaceAll("/[^/]+/*$", "") + "/";
    }
}
