package net.ihe.gazelle.wstester.model;

import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "gwt_test_instance_result")
@SequenceGenerator(name = "gwt_test_instance_result_sequence", sequenceName = "gwt_test_instance_result_id_seq", allocationSize = 1)
public class TestInstanceResult implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(TestInstanceResult.class);

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gwt_test_instance_result_sequence")
    private Integer id;

    @Column(name = "testInstance", nullable = false)
    private int testInstance;

    @Column(name = "lastStatus")
    private String lastStatus;

    @Column(name = "testKeyword")
    private String testKeyword;

    @Column(name = "testName")
    private String testName;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "test_suite_id")
    private GwtTestSuite testSuite;

    @OneToOne(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    @JoinColumn(name = "test_execution_id")
    private Execution execution;

    public TestInstanceResult() {
    }

    public TestInstanceResult(int testInstance, GwtTestSuite testSuite) {
        this.testInstance = testInstance;
        this.testSuite = testSuite;
    }

    public TestInstanceResult(int testInstance, String lastStatus, String testKeyword, String testName, GwtTestSuite testSuite, Execution execution) {
        this.testInstance = testInstance;
        this.lastStatus = lastStatus;
        this.testKeyword = testKeyword;
        this.testName = testName;
        this.testSuite = testSuite;
        this.execution = execution;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getTestInstance() {
        return testInstance;
    }

    public void setTestInstance(int testInstance) {
        this.testInstance = testInstance;
    }

    public GwtTestSuite getTestSuite() {
        return testSuite;
    }

    public void setTestSuite(GwtTestSuite testSuite) {
        this.testSuite = testSuite;
    }

    public Execution getExecution() {
        return execution;
    }

    public void setExecution(Execution execution) {
        this.execution = execution;
    }

    public String getLastStatus() {
        return lastStatus;
    }

    public void setLastStatus(String lastStatus) {
        this.lastStatus = lastStatus;
    }

    public String getTestKeyword() {
        return testKeyword;
    }

    public void setTestKeyword(String testKeyword) {
        this.testKeyword = testKeyword;
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public String getInstanceLink() {
        return ApplicationConfiguration.getValueOfVariable("tm_url") + "gazelle/testInstance.seam?id=" + this.getTestInstance();
    }

    public String getTestInstanceToString() {
        return Integer.toString(testInstance);
    }
}
