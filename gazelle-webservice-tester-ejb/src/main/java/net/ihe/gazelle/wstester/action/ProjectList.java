package net.ihe.gazelle.wstester.action;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.ssov7.gum.client.application.UserAttributeCommon;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.ssov7.gum.client.interlay.filter.UserValueFormatter;
import net.ihe.gazelle.wstester.dao.ExecutionDao;
import net.ihe.gazelle.wstester.model.*;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.Map;

@Name("projectList")
@Scope(ScopeType.PAGE)
public class ProjectList implements Serializable, UserAttributeCommon {

    public static final String OWNER = "owner";
    private FilterDataModel<SoapuiProject> soapuiProjects;
    private Filter<SoapuiProject> filter;

    @In(value="gumUserService")
    private UserService userService;

    public FilterDataModel<SoapuiProject> getSoapuiProjects() {
        return new FilterDataModel<SoapuiProject>(getFilter()) {
            @Override
            protected Object getId(SoapuiProject soapuiProject) {
                return soapuiProject.getId();
            }
        };
    }

    public Filter<SoapuiProject> getFilter() {
        if (filter == null) {
            Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
            this.filter = new Filter(this.getHqlCriterions(), params);
            this.filter.getFormatters().put(OWNER, new UserValueFormatter(this.filter, OWNER));
        }
        return this.filter;
    }

    private HQLCriterionsForFilter getHqlCriterions() {
        SoapuiProjectQuery query = new SoapuiProjectQuery();
        HQLCriterionsForFilter criterion = query.getHQLCriterionsForFilter();
        criterion.addPath("label", query.label());
        criterion.addPath(OWNER, query.owner());
        criterion.addPath("creationDate", query.creationDate());
        criterion.addPath("active", query.active());
        return criterion;
    }

    public void reset() {
        if (this.filter != null) {
            this.filter.clear();
        }
    }

    /**
     * Called in projectList.xhtml
     */
    @Override
    public String getUserName(String userId) {
        return userService.getUserDisplayNameWithoutException(userId);
    }

    public boolean deleteProject(SoapuiProject project) {
        if (ExecutionDao.isProjectHaveBeenExecuted(project) == true) {
            FacesMessages.instance().add(StatusMessage.Severity.WARN, "This project have been executed : deletion impossible");
            return false;
        } else {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            for (GwtTestSuite gwtTestSuite : project.getGwtProject().getTestSuites()) {
                for (GwtTestCase gwtTestCase : gwtTestSuite.getTestCases()) {
                    for (GwtTestStep gwtTestStep : gwtTestCase.getTestSteps()) {
                        deleteCustomProperties(gwtTestStep, entityManager);
                        entityManager.remove(gwtTestStep);
                    }
                    deleteCustomProperties(gwtTestCase, entityManager);
                    entityManager.remove(gwtTestCase);
                }
                deleteCustomProperties(gwtTestSuite, entityManager);
                entityManager.remove(gwtTestSuite);
            }
            deleteCustomProperties(project.getGwtProject(), entityManager);
            entityManager.remove(project.getGwtProject());
            entityManager.remove(project);
            entityManager.flush();
            return true;
        }
    }

    public void deleteCustomProperties(TestComponent testComponent, EntityManager entityManager) {
        for (CustomProperty customProperty : testComponent.getCustomProperties()) {
            for (CustomPropertyUsed customPropertyUsed : customProperty.getCustomPropertyUsedList()) {
                entityManager.remove(customPropertyUsed);
                entityManager.flush();
            }
            entityManager.remove(customProperty);
            entityManager.flush();
        }
    }
}