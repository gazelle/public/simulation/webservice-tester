package net.ihe.gazelle.wstester.action;

import net.ihe.gazelle.common.tree.GazelleTreeNodeImpl;
import net.ihe.gazelle.wstester.dao.SoapuiProjectDao;
import net.ihe.gazelle.wstester.model.CustomProperty;
import net.ihe.gazelle.wstester.model.SoapuiProject;
import net.ihe.gazelle.wstester.model.TestComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.List;

public class ProjectDetail implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(ProjectDetail.class);

    private SoapuiProject soapuiProject;
    private List<CustomProperty> customPropertyList;
    private GazelleTreeNodeImpl<TestComponent> tree;

    public SoapuiProject getSoapuiProject() {
        return soapuiProject;
    }

    public void setSoapuiProject(SoapuiProject soapuiProject) {
        this.soapuiProject = soapuiProject;
    }

    public GazelleTreeNodeImpl<TestComponent> getTree() {
        return tree;
    }

    public void setTree(GazelleTreeNodeImpl<TestComponent> tree) {
        this.tree = tree;
    }

    public List<CustomProperty> getCustomPropertyList() {
        return customPropertyList;
    }

    public void setCustomPropertyList(List<CustomProperty> customPropertyList) {
        this.customPropertyList = customPropertyList;
    }

    public void setActive(boolean active) {
        soapuiProject.setActive(active);
        SoapuiProjectDao.mergeSoapuiProject(soapuiProject);
    }

    public void saveEditProject() {
        SoapuiProjectDao.mergeSoapuiProject(soapuiProject);
    }
}
