package net.ihe.gazelle.wstester.action;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.paths.HQLSafePathBasicNumber;
import net.ihe.gazelle.ssov7.gum.client.application.UserAttributeCommon;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.ssov7.gum.client.interlay.filter.UserValueFormatter;
import net.ihe.gazelle.wstester.dao.CompanyValueProvider;
import net.ihe.gazelle.wstester.dao.ExecutionDao;
import net.ihe.gazelle.wstester.model.Execution;
import net.ihe.gazelle.wstester.model.ExecutionQuery;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.Map;

@Name("executionList")
@Scope(ScopeType.PAGE)
public class ExecutionList implements Serializable, UserAttributeCommon {

    public static final String LAUNCH_BY = "launchBy";
    @In
    private EntityManager entityManager;

    private FilterDataModel<Execution> executions;
    private Filter<Execution> filter;

    @In(value="gumUserService")
    private UserService userService;

    public FilterDataModel<Execution> getExecutions() {
        return new FilterDataModel<Execution>(getFilter()) {
            @Override
            protected Object getId(Execution execution) {
                return execution.getId();
            }
        };
    }

    public Filter<Execution> getFilter() {
        if (filter == null) {
            Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
            this.filter = new Filter(this.getHqlCriterions(), params);
            this.filter.getFormatters().put(LAUNCH_BY, new UserValueFormatter(this.filter, LAUNCH_BY));
        }
        return this.filter;
    }

    private HQLCriterionsForFilter getHqlCriterions() {
        ExecutionQuery query = new ExecutionQuery();
        HQLCriterionsForFilter criterion = query.getHQLCriterionsForFilter();
        criterion.addPath(LAUNCH_BY, query.launchBy());
        criterion.addPath("launchDate", query.launchDate());
        criterion.addPath("company", query.company(), null, CompanyValueProvider.SINGLETON());
        criterion.addPath("soapuiProject", query.soapuiProject().label());
        criterion.addPath("testComponent", query.testComponent().label());
        HQLSafePathBasicNumber instance = query.testInstanceResult().testInstance();
        instance.setMinValue(0);
        instance.setMaxValue(100000);
        criterion.addPath("testInstanceResult", instance);
        return criterion;
    }

    public void reset() {
        if (this.filter != null) {
            this.filter.clear();
        }
    }

    public void refreshStatus() {
        for (Execution execution : ExecutionDao.getAllExecution()) {
            ExecutionDao.mergeExecution(execution);
        }
    }

    /**
     * Called in executionList.xhtml
     */
    @Override
    public String getUserName(String userId) {
        return userService.getUserDisplayNameWithoutException(userId);
    }

    public String reRun(Execution execution) {
        RunInstanceAction runner = new RunInstanceAction();
        execution = entityManager.find(Execution.class, execution.getId());
        runner.reRunTestInstance(execution);
        return "/executionResult.seam?id=" + runner.getExecution().getId();
    }
}