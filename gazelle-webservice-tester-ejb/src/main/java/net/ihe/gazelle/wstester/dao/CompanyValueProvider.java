package net.ihe.gazelle.wstester.dao;

import net.ihe.gazelle.hql.criterion.ValueProvider;
import net.ihe.gazelle.wstester.util.Application;
import org.jboss.seam.security.Identity;

public class CompanyValueProvider implements ValueProvider {

    public static CompanyValueProvider SINGLETON() {
        return new CompanyValueProvider();
    }

    @Override
    public Object getValue() {
        if (Identity.instance().hasRole("monitor_role") || Identity.instance().hasRole("admin_role")) {
            return null;
        } else {
            return Application.getCompanyKeyword();
        }
    }

}
