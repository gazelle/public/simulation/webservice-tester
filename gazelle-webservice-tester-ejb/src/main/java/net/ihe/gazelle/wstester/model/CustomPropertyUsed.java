package net.ihe.gazelle.wstester.model;

import net.ihe.gazelle.simulator.sut.model.SystemConfiguration;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "gwt_custom_property_used")
@SequenceGenerator(name = "gwt_custom_property_used_sequence", sequenceName = "gwt_custom_property_used_id_seq", allocationSize = 1)
public class CustomPropertyUsed implements Serializable {

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gwt_custom_property_used_sequence")
    private Integer id;

    @Column(name = "value", nullable = false)
    @Lob
    @Type(type = "text")
    private String value;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "execution_id")
    private Execution execution;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "sut")
    private SystemConfiguration systemConfiguration;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "custom_property_id")
    private CustomProperty customProperty;

    public CustomPropertyUsed() {
        super();
    }

    public CustomPropertyUsed(String value, Execution execution) {
        this.value = value;
        this.execution = execution;
    }

    public CustomPropertyUsed(String value, Execution execution, CustomProperty customProperty) {
        this.value = value;
        this.execution = execution;
        this.customProperty = customProperty;
    }

    public CustomPropertyUsed(CustomPropertyUsed customPropertyUsed) {
        this.value = customPropertyUsed.getValue();
        this.execution = customPropertyUsed.getExecution();
        this.systemConfiguration = customPropertyUsed.getSystemConfiguration();
        this.customProperty = customPropertyUsed.getCustomProperty();
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Execution getExecution() {
        return execution;
    }

    public void setExecution(Execution execution) {
        this.execution = execution;
    }

    public SystemConfiguration getSystemConfiguration() {
        return systemConfiguration;
    }

    public void setSystemConfiguration(SystemConfiguration systemConfiguration) {
        this.systemConfiguration = systemConfiguration;
    }

    public CustomProperty getCustomProperty() {
        return customProperty;
    }

    public void setCustomProperty(CustomProperty customProperty) {
        this.customProperty = customProperty;
    }
}
