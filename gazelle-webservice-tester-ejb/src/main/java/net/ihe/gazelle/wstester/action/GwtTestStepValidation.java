package net.ihe.gazelle.wstester.action;

import net.ihe.gazelle.evsclient.connector.api.EVSClientResults;
import net.ihe.gazelle.evsclient.connector.api.EVSClientServletConnector;
import net.ihe.gazelle.evsclient.connector.model.EVSClientValidatedObject;
import net.ihe.gazelle.evsclient.connector.utils.PartSourceUtils;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.wstester.model.GwtTestStepResult;
import net.ihe.gazelle.wstester.model.ValidationResult;
import org.apache.commons.httpclient.methods.multipart.PartSource;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import java.io.IOException;
import java.util.Map;

public class GwtTestStepValidation extends GwtValidation implements EVSClientValidatedObject {

    private static final Logger LOG = LoggerFactory.getLogger(GwtTestStepValidation.class);

    private String gwtTestStepResultId;
    private byte[] selectedMessage;

    private GwtTestStepResult gwtTestStepResultSelected;

    private ValidationResult validationResult;


    public GwtTestStepValidation() {
    }

    //Validation
    public GwtTestStepValidation(String gwtTestStepResultId, byte[] selectedMessage) {
        this.gwtTestStepResultId = gwtTestStepResultId;
        this.selectedMessage = selectedMessage.clone();
    }


    public byte[] getSelectedMessage() {
        return selectedMessage;
    }

    public void setSelectedMessage(byte[] selectedMessage) {
        this.selectedMessage = selectedMessage.clone();
    }

    public String getGwtTestStepResultId() {
        return gwtTestStepResultId;
    }

    public void setGwtTestStepResultId(String gwtTestStepResultId) {
        this.gwtTestStepResultId = gwtTestStepResultId;
    }

    public GwtTestStepResult getGwtTestStepResultSelected() {
        return gwtTestStepResultSelected;
    }

    public void setGwtTestStepResultSelected(GwtTestStepResult gwtTestStepResultSelected) {
        this.gwtTestStepResultSelected = gwtTestStepResultSelected;
    }

    public void init() {
        Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();

        if (urlParams != null && urlParams.size() > 0) {
            String[] parts = null;
            //Validation process
            if (urlParams.containsKey("id")) {
                String externalId = ((String) urlParams.get("id"));
                parts = externalId.split("_");
                EntityManager entityManager = EntityManagerService.provideEntityManager();

                int testStepResultId = Integer.parseInt(parts[0]);

                gwtTestStepResultSelected = entityManager.find(GwtTestStepResult.class, testStepResultId);
                if (gwtTestStepResultSelected == null) {
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The given id does not match any test step result in the database");
                } else {


                    String validationOid = EVSClientResults.getResultOidFromUrl(urlParams);
                    String resultType = parts[1];
                    if (validationOid != null && parts != null && resultType != null) {

                        ValidationResult result = new ValidationResult(gwtTestStepResultSelected, validationOid, resultType);
                        if (result.isRequest()) {
                            gwtTestStepResultSelected.setRequestValidation(result);
                        } else if (result.isResponse()) {
                            gwtTestStepResultSelected.setResponseValidation(result);
                        }
                        getResultDetails(result);
                        gwtTestStepResultSelected.save();
                    }
                }
            }

            gwtTestStepResultSelected.save();

            try {
                if (gwtTestStepResultSelected != null) {
                    FacesContext.getCurrentInstance().getExternalContext().redirect("/gazelle-webservice-tester/executionResult.seam?id="
                            + gwtTestStepResultSelected.getExecution().getId() + "&validation=" + urlParams.get("id"));
                }
            } catch (IOException e) {
                LOG.error(e.getMessage());
            }
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The url used must have a problem, it is not possible to redirect to the " +
                    "execution test step.");
        }
    }

    private void getResultDetails(final ValidationResult result) {
        String evsclientUrl = ApplicationConfiguration.getValueOfVariable("evs_client_url");
        String validationDate = EVSClientResults.getValidationDate(result.getOid(), evsclientUrl);
        result.setValidationDate(validationDate);
        String validationStatus = EVSClientResults.getValidationStatus(result.getOid(), evsclientUrl);
        result.setValidationStatus(validationStatus);
        String linkToResult = EVSClientResults.getValidationPermanentLink(result.getOid(), evsclientUrl);
        result.setPermanentLink(linkToResult);
        validationResult = result.save();
    }

    @Override
    public String getUniqueKey() {
        return gwtTestStepResultId;
    }

    @Override
    public PartSource getPartSource() {
        if (selectedMessage != null) {
            return PartSourceUtils.buildEncoded64PartSource(selectedMessage, getType());
        } else {
            return null;
        }
    }

    @Override
    public String getType() {
        return "xml";
    }

    public void validate() {
        String evsClientUrl = ApplicationConfiguration.getValueOfVariable("evs_client_url");
        String toolOid = ApplicationConfiguration.getValueOfVariable("tool_instance_oid");
        if (evsClientUrl == null) {
            LOG.error("evs_client_url is missing in app_configuration table");
        } else if (toolOid == null) {
            LOG.error("tool_instance_oid is missing in app_configuration table");
        } else {
            ExternalContext extContext = FacesContext.getCurrentInstance().getExternalContext();
            EVSClientServletConnector.sendToValidation(this, extContext, evsClientUrl, toolOid);
        }
    }

}
