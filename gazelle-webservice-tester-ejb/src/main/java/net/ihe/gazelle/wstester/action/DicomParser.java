package net.ihe.gazelle.wstester.action;

import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import org.apache.commons.fileupload.MultipartStream;
import org.dcm4che2.data.Tag;
import org.dcm4che2.tool.dcm2xml.Dcm2Xml;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.transform.TransformerConfigurationException;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.UUID;

public class DicomParser {

    private final Logger logger = LoggerFactory.getLogger(DicomParser.class);

    private final byte[] rawResponseData;

    private final String contentType;

    private Integer maxToDisplay;

    public DicomParser(byte[] rawResponseData, String contentType) {
        this.rawResponseData = rawResponseData;
        this.contentType = contentType;
        getMaxToDisplay();
    }

    public String dumpDicomwebResponseToXml() {
        String boundary = null;
        String type = null;
        if (contentType != null) {
            for (String param : contentType.split(";")) {
                if (param.contains("boundary")) {
                    boundary = param.split("=")[1];
                }
                if (param.contains("type")) {
                    type = param.split("=")[1];
                }
            }
        }
        if ("\"application/dicom\"".equals(type) && boundary != null) {
            return dicomResponseToXml(boundary);
        } else {
            return null;
        }
    }

    public String dicomResponseToXml(String boundary) {
        MultipartStream multipartStream = new MultipartStream(new ByteArrayInputStream(rawResponseData), boundary.getBytes(), rawResponseData.length);
        boolean nextPart = false;
        StringBuilder responseDump = new StringBuilder();
        try {
            nextPart = multipartStream.skipPreamble();
            Dcm2Xml dcm2Xml = new Dcm2Xml();
            dcm2Xml.setExclude(new int[]{Tag.PixelData});

            boolean maxToDisplayNotReached = true;
            int currentDisplayed = 0;
            if (maxToDisplay != null && maxToDisplay.equals(0)) {
                responseDump.insert(0, "The application is configured to not display any dicom images" + System.lineSeparator());
            } else {
                while (nextPart && maxToDisplayNotReached) {

                    nextPart = singlePartToXml(multipartStream, dcm2Xml, nextPart, responseDump);
                    if (maxToDisplay != null) {
                        currentDisplayed++;
                        if (currentDisplayed == maxToDisplay) {
                            maxToDisplayNotReached = false;
                            responseDump.insert(0, "Only the first " + maxToDisplay + " DICOM documents (dumped to xml) are displayed." + System.lineSeparator());
                        }
                    }
                }
            }
        } catch (IOException e) {
            logger.error("Error during the dump DICOM to XML process, aborting");
        }
        return responseDump.toString();
    }


    private boolean singlePartToXml(MultipartStream multipartStream, Dcm2Xml dcm2Xml, boolean nextPart, StringBuilder responseDump) throws IOException {
        Path dicomFile = null;
        Path xmlFile = null;
        try (ByteArrayOutputStream output = new ByteArrayOutputStream()) {
            multipartStream.readHeaders();
            multipartStream.readBodyData(output);
            dicomFile = Files.createTempFile(UUID.randomUUID().toString(), ".dcm");
            xmlFile = Files.createTempFile(UUID.randomUUID().toString(), ".xml");
            FileOutputStream fileOutputStream = new FileOutputStream(dicomFile.toFile());
            output.writeTo(fileOutputStream);
            dcm2Xml.convert(dicomFile.toFile(), xmlFile.toFile());
            nextPart = multipartStream.readBoundary();
            responseDump.append(new String(Files.readAllBytes(xmlFile)));
            responseDump.append(System.lineSeparator());
        } catch (TransformerConfigurationException e) {
            logger.error("Error during the dump DICOM to XML process, aborting");
        } finally {
            if (dicomFile != null) {
                Files.delete(dicomFile);
            }
            if (xmlFile != null) {
                Files.delete(xmlFile);
            }
        }
        return nextPart;
    }

    private void getMaxToDisplay() {
        String value = ApplicationConfiguration.getValueOfVariable("dump_dicom_to_xml_max_to_display");
        try {
            this.maxToDisplay = Integer.parseInt(value);
        } catch (NumberFormatException ex) {
            logger.error("Properties dump_dicom_to_xml_max_to_display is not an integer!");
            this.maxToDisplay = null;
        }
    }
}
