package net.ihe.gazelle.wstester.dao;

import net.ihe.gazelle.evsclient.connector.api.EVSClientResults;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GwtProjectResultDao {

    private static final Logger LOG = LoggerFactory.getLogger(ExecutionDao.class);

    public static String getXValidatorStatusFromOid(String oid) {

        String evsClientUrl = ApplicationConfiguration.getValueOfVariable("evs_client_url");
        return EVSClientResults.getXValidatorStatus(oid,evsClientUrl);
    }

}
