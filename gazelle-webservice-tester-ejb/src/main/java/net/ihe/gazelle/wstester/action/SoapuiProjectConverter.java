package net.ihe.gazelle.wstester.action;

import net.ihe.gazelle.wstester.dao.SoapuiProjectDao;
import net.ihe.gazelle.wstester.model.SoapuiProject;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.faces.Converter;
import org.jboss.seam.annotations.intercept.BypassInterceptors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

@Name("soapuiProjectConverter")
@BypassInterceptors
@Converter(forClass = SoapuiProject.class)
public class SoapuiProjectConverter implements javax.faces.convert.Converter {

    private static final Logger LOGGER = LoggerFactory.getLogger(SoapuiProjectConverter.class);

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        return SoapuiProjectDao.getSoapuiProjectByLabel(s);
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        if (o instanceof SoapuiProject) {
            SoapuiProject project = (SoapuiProject) o;
            return project.getLabel();
        } else {
            return null;
        }
    }
}
