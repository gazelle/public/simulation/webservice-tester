package net.ihe.gazelle.wstester.dao;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.wstester.model.Execution;
import net.ihe.gazelle.wstester.model.ExecutionQuery;
import net.ihe.gazelle.wstester.model.SoapuiProject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.util.List;

public class ExecutionDao {

    private static final Logger LOG = LoggerFactory.getLogger(ExecutionDao.class);

    public static Execution getExecutionById(Integer id) {
        return EntityManagerService.provideEntityManager().find(Execution.class, id);
    }

    public static List<Execution> getAllExecution() {
        ExecutionQuery executionQuery = new ExecutionQuery();
        return executionQuery.getListNullIfEmpty();
    }

    public static Execution mergeExecution(Execution execution) {
        if (execution == null) {
            ExecutionDao.LOG.error("The execution is null");
            return null;
        } else {
            execution.updateGlobalStatus();
            final EntityManager em = EntityManagerService.provideEntityManager();
            Execution newExecution = em.merge(execution);
            em.flush();
            return newExecution;
        }
    }

    public static boolean isProjectHaveBeenExecuted(SoapuiProject soapuiProject) {
        ExecutionQuery executionQuery = new ExecutionQuery();
        List<Execution> executionList = executionQuery.getListNullIfEmpty();
        boolean haveBeenExecuted = false;
        if (executionList != null) {
            for (Execution execution : executionList) {
                if (execution.getSoapuiProject().equals(soapuiProject)) {
                    haveBeenExecuted = true;
                }
            }
        }
        return haveBeenExecuted;
    }

}
