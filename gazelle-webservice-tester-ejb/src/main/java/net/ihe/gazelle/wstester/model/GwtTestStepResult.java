package net.ihe.gazelle.wstester.model;

import com.uwyn.jhighlight.renderer.XhtmlRendererFactory;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.wstester.action.GwtTestStepValidation;
import net.ihe.gazelle.wstester.util.*;
import org.apache.commons.fileupload.MultipartStream;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntity;
import org.hibernate.annotations.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Scanner;

@Entity
@Table(name = "gwt_test_step_result")
@SequenceGenerator(name = "gwt_test_step_result_sequence", sequenceName = "gwt_test_step_result_id_seq", allocationSize = 1)
public class GwtTestStepResult implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(GwtTestStepResult.class);

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gwt_test_step_result_sequence")
    private Integer id;

    @Column(name = "status", nullable = false)
    private String status;

    @Column(name = "time_taken", nullable = false)
    private long timeTaken;

    @Column(name = "endpoint")
    private String endpoint;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private TestStepType type;

    @OneToMany(cascade = CascadeType.ALL)
    private List<ValidationResult> validationResults;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "gwt_test_step_result_attachment_request", joinColumns = {@JoinColumn(name = "test_step_id", referencedColumnName = "id")}, inverseJoinColumns = {@JoinColumn(name = "attachment_id", referencedColumnName = "id", unique = true)})
    private List<GwtAttachment> requestAttachments;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "gwt_test_step_result_attachment_response", joinColumns = {@JoinColumn(name = "test_step_id", referencedColumnName = "id")}, inverseJoinColumns = {@JoinColumn(name = "attachment_id", referencedColumnName = "id", unique = true)})
    private List<GwtAttachment> responseAttachments;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "messages")
    private TransactionInstance messages;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "test_step_id")
    private GwtTestStep testStep;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "execution_id")
    private Execution execution;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "gwt_soapui_messages", joinColumns = @JoinColumn(name = "gwt_test_step_result_id"))
    @Column(name = "soapui_messages")
    @Lob
    @Type(type = "text")
    private List<String> soapUIMessages;

    public GwtTestStepResult() {
        super();
    }

    public GwtTestStepResult(String status, long timeTaken, TransactionInstance messages, GwtTestStep testStep, Execution execution, String endpoint, TestStepType type) {
        this.status = status;
        this.timeTaken = timeTaken;
        this.messages = messages;
        this.testStep = testStep;
        this.execution = execution;
        this.endpoint = endpoint;
        this.type = type;
    }

    public GwtTestStepResult(String status, long timeTaken, TransactionInstance messages, GwtTestStep testStep, Execution execution, String endpoint, List<String> soapUIMessages, TestStepType type, List<GwtAttachment> requestAttachments, List<GwtAttachment> responseAttachments) {
        this.status = status;
        this.timeTaken = timeTaken;
        this.endpoint = endpoint;
        this.messages = messages;
        this.testStep = testStep;
        this.execution = execution;
        this.soapUIMessages = soapUIMessages;
        this.type = type;
        this.requestAttachments = requestAttachments;
        this.responseAttachments = responseAttachments;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public TestStepType getType() {
        return type;
    }

    public void setType(TestStepType type) {
        this.type = type;
    }

    public List<ValidationResult> getValidationResults() {
        return validationResults;
    }

    public void setValidationResults(List<ValidationResult> validationResults) {
        this.validationResults = validationResults;
    }

    public long getTimeTaken() {
        return timeTaken;
    }

    public void setTimeTaken(long timeTaken) {
        this.timeTaken = timeTaken;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public TransactionInstance getTransaction() {
        return messages;
    }

    public void setTransaction(TransactionInstance messages) {
        this.messages = messages;
    }

    public TransactionInstance getMessages() {
        return messages;
    }

    public void setMessages(TransactionInstance messages) {
        this.messages = messages;
    }

    public GwtTestStep getTestStep() {
        return testStep;
    }

    public void setTestStep(GwtTestStep testStep) {
        this.testStep = testStep;
    }

    public Execution getExecution() {
        return execution;
    }

    public void setExecution(Execution execution) {
        this.execution = execution;
    }

    public List<String> getSoapUIMessages() {
        return soapUIMessages;
    }

    public void setSoapUIMessages(List<String> soapUIMessages) {
        this.soapUIMessages = soapUIMessages;
    }

    public String getTimestamp() {
        return DateUtil.dateToString(messages.getTimestamp());
    }

    public String getResponseTimestamp() {
        return DateUtil.dateToString(DateUtil.addMsToDate(messages.getTimestamp(), timeTaken));
    }

    public List<GwtAttachment> getRequestAttachments() {
        return requestAttachments;
    }

    public void setRequestAttachments(List<GwtAttachment> requestAttachments) {
        requestAttachments = requestAttachments;
    }

    public List<GwtAttachment> getResponseAttachments() {
        return responseAttachments;
    }

    public void setResponseAttachments(List<GwtAttachment> responseAttachments) {
        responseAttachments = responseAttachments;
    }

    /**
     * Check if the request content is null or empty.
     *
     * @return true if a request content is present.
     */
    public boolean isRequestContent() {
        return messages.getRequest() != null && messages.getRequest().getContent() != null;
    }

    /**
     * Check if the request content is a dicom request.
     *
     * @return boolean
     */
    public boolean isDicomRequest() {
        return messages.getRequest().getContentAsString().contains("application/dicom")
                && !messages.getRequest().getContentAsString().contains("frames");
    }

    /**
     * Check if the request content is a frame dicom request.
     *
     * @return boolean
     */
    public boolean isDicomFrameRequest() {
        return messages.getRequest().getContentAsString().contains("application/dicom")
                && messages.getRequest().getContentAsString().contains("frames");
    }

    public String getRequestContent() {
        try {
            if (isRequestContent()) {
                if (type.equals(TestStepType.SOAP)) {
                    return XhtmlRendererFactory.getRenderer("xml").highlight(null, Xml.prettyFormat(new String(this.messages.getRequest().getContent(), StandardCharsets.UTF_8)), "UTF-8", false).replaceAll("<h1>.*</h1>", "");
                } else {
                    return new String(messages.getRequest().getContent(), StandardCharsets.UTF_8);
                }
            } else {
                return null;
            }
        } catch (IOException e) {
            LOG.error("getRequestContent() : " + e.getMessage());
        }
        return null;
    }

    /**
     * Check if the response content is null or empty.
     *
     * @return true if a response content is present.
     */
    public boolean isResponseContent() {
        return messages.getResponse() != null && messages.getResponse().getContent() != null && !(new String(messages.getResponse().getContent(), StandardCharsets.UTF_8).equals("<missing response>"));
    }

    public boolean doesResponseContainPayload() {
        return messages.getResponse() != null && messages.getResponse().getPayload() != null;
    }

    public boolean doesResponseContainPayloadOrContent() {
        return doesResponseContainPayload() || isResponseContent();
    }

    public String getResponseContent() {
        String response = "";
        try {
            if (isResponseContent()) {
                return XhtmlRendererFactory.getRenderer("xml").highlight("Response", Xml.prettyFormat(new String(this.messages.getResponse()
                        .getContent(), StandardCharsets.UTF_8)), "UTF-8", false).replaceAll("<h1>.*</h1>", "");
            } else {
                return null;
            }
        } catch (IOException e) {
            LOG.error("getResponseContent(): " + e.getMessage());
        }
        return response;
    }

    public String getResponsePayload() {
        if(messages.getResponse() != null && messages.getResponse().getPayload() != null) {
            return new String(this.messages.getResponse().getPayload());
        }
        return null;
    }

    public String getStatusForCss() {
        if (getStatus() != null) {
            if (getStatus().equals("OK")) {
                return "PASSED";
            } else {
                return getStatus();
            }
        } else {
            return "UNKNOWN";
        }
    }

    public void validateRequest() {
        GwtTestStepValidation gwtTestStepValidation = new GwtTestStepValidation(id + "_request", messages.getRequest().getContent());
        gwtTestStepValidation.validate();
    }

    public void validateResponse() {
        GwtTestStepValidation gwtTestStepValidation = new GwtTestStepValidation(id + "_response", messages.getResponse().getContent());
        gwtTestStepValidation.validate();
    }

    public ValidationResult getRequestValidation() {
        if (validationResults != null) {
            for (ValidationResult vr : validationResults) {
                if (vr.isRequest()) {
                    return vr;
                }
            }
        }
        return null;
    }

    public void setRequestValidation(ValidationResult validationResult) {
        validationResults.remove(getRequestValidation());
        validationResults.add(validationResult);
    }

    public ValidationResult getResponseValidation() {
        if (validationResults != null) {
            for (ValidationResult vr : validationResults) {
                if (vr.isResponse()) {
                    return vr;
                }
            }
        }
        return null;
    }

    public void setResponseValidation(ValidationResult validationResult) {
        validationResults.remove(getResponseValidation());
        validationResults.add(validationResult);
    }

    public void downloadRequest() {
        final String content = this.getMessages().getRequest().getContentAsString();
        final String fileName = this.testStep.getLabel() + "_request.xml";
        DownloadFile.downloadMessage(content, fileName);
    }

    public void downloadResponse() {
        final String content = this.getMessages().getResponse().getContentAsString();
        final String fileName = this.testStep.getLabel() + "_response.xml";
        DownloadFile.downloadMessage(content, fileName);
    }

    public String getFullLabel() {
        StringBuilder label = new StringBuilder();
        label.append(testStep.getTestCase().getTestSuite().getLabel());
        label.append(" / ");
        label.append(testStep.getTestCase().getLabel());
        label.append(" / ");
        label.append(testStep.getLabel());
        return label.toString();
    }

    public GwtTestStepResult save() {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        GwtTestStepResult result = entityManager.merge(this);
        entityManager.flush();
        return result;
    }
}
