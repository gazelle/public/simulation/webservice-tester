package net.ihe.gazelle.wstester.dao;

import net.ihe.gazelle.simulator.message.model.MessageInstance;
import net.ihe.gazelle.wstester.model.MockValidationResult;

public interface MockValidationResultDao {

    MockValidationResult getLastMockValidationResultByMessageInstance(MessageInstance messageInstance);
    }
