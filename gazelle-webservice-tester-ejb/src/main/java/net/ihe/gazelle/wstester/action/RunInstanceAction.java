package net.ihe.gazelle.wstester.action;

import com.eviware.soapui.SoapUI;
import com.eviware.soapui.impl.wsdl.WsdlProject;
import com.eviware.soapui.impl.wsdl.WsdlTestSuite;
import com.eviware.soapui.impl.wsdl.submit.transports.http.SinglePartHttpResponse;
import com.eviware.soapui.impl.wsdl.submit.transports.http.support.attachments.BodyPartAttachment;
import com.eviware.soapui.impl.wsdl.submit.transports.http.support.attachments.MimeMessageResponse;
import com.eviware.soapui.impl.wsdl.testcase.WsdlProjectRunner;
import com.eviware.soapui.impl.wsdl.testcase.WsdlTestCase;
import com.eviware.soapui.impl.wsdl.teststeps.*;
import com.eviware.soapui.model.TestPropertyHolder;
import com.eviware.soapui.model.iface.Attachment;
import com.eviware.soapui.model.iface.MessageExchange;
import com.eviware.soapui.model.iface.Response;
import com.eviware.soapui.model.support.PropertiesMap;
import com.eviware.soapui.model.testsuite.*;
import com.eviware.soapui.settings.HttpSettings;
import com.eviware.soapui.settings.SSLSettings;
import com.eviware.soapui.settings.WsdlSettings;
import com.eviware.soapui.support.SoapUIException;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.message.model.MessageInstance;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.ws.client.GazelleProxyWebServiceNameStub;
import net.ihe.gazelle.ws.client.GazelleTRMServiceStub;
import net.ihe.gazelle.wstester.dao.ExecutionDao;
import net.ihe.gazelle.wstester.dao.GwtTestStepDao;
import net.ihe.gazelle.wstester.dao.GwtTestSuiteDao;
import net.ihe.gazelle.wstester.dao.KeystoreDao;
import net.ihe.gazelle.wstester.model.*;
import net.ihe.gazelle.wstester.util.Application;
import net.ihe.gazelle.wstester.util.HibernateUtil;
import net.ihe.gazelle.wstester.util.WsClient;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.xmlbeans.XmlException;
import org.hibernate.Hibernate;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class RunInstanceAction {
    private static final Logger LOG = LoggerFactory.getLogger(RunInstanceAction.class);

    private SoapuiProject currentProject = null;
    private Execution execution = null;
    private int testInstanceId;
    private TestComponent componentToExecute;

    public SoapuiProject getCurrentProject() {
        return currentProject;
    }

    public void setCurrentProject(SoapuiProject currentProject) {
        this.currentProject = currentProject;
    }

    public Execution getExecution() {
        return execution;
    }

    public void setExecution(Execution execution) {
        this.execution = execution;
    }

    public int getTestInstanceId() {
        return testInstanceId;
    }

    public void setTestInstanceId(int testInstanceId) {
        this.testInstanceId = testInstanceId;
    }

    public void setTestInstanceId(String testInstanceId) throws NumberFormatException {
        this.testInstanceId = Integer.valueOf(testInstanceId); // Null check to be added
    }

    public TestComponent getComponentToExecute() {
        return componentToExecute;
    }

    public void setComponentToExecute(TestComponent componentToExecute) {
        this.componentToExecute = componentToExecute;
    }

    public WsdlProject createWsdlProject() {
        WsdlProject wsdlProject = null;
        if (currentProject != null && currentProject.getXmlFilePath() != null) {
            try {
                wsdlProject = new WsdlProject(currentProject.getXmlFilePath());
            } catch (XmlException e) {
                LOG.error(e.getMessage());
            } catch (IOException e) {
                LOG.error(e.getMessage());
            } catch (SoapUIException e) {
                LOG.error(e.getMessage());
            }
        } else {
            LOG.error("Can't create WSDL Project : the soapui project is null or don't have xml file path");
        }
        return wsdlProject;
    }

    public void reRunTestInstance(Execution oldExecution) {
        this.execution = new Execution();
        if (oldExecution.getTestInstanceResult() != null) {
            //Test instance from TM
            TestInstanceResult newTestInstanceResult = initTestInstanceResultForReRun(oldExecution);
            this.execution.setTestInstanceResult(newTestInstanceResult);
        } else {
            componentToExecute = oldExecution.getTestComponent();
            componentToExecute = HibernateUtil.initializeAndUnproxy(componentToExecute);
        }
        prepareExecution();
        this.currentProject = oldExecution.getSoapuiProject();
        execution = ExecutionDao.mergeExecution(execution);
        List<CustomPropertyUsed> copyCustomPropertiesOldExecution = refreshExecutionId(oldExecution.getCustomPropertiesUsed());
        refreshExecutionId(copyCustomPropertiesOldExecution);
        execution.setCustomPropertiesUsed(copyCustomPropertiesOldExecution);
        execution = ExecutionDao.mergeExecution(execution);
        if (oldExecution.getTestInstanceResult() != null) {
            runTestInstance();
        } else {
            run();
        }
    }

    private List<CustomPropertyUsed> refreshExecutionId(List<CustomPropertyUsed> customPropertyUsedList) {
        List<CustomPropertyUsed> newExecutionCustomProperties = new ArrayList<>();
        if (customPropertyUsedList != null) {
            for (CustomPropertyUsed customPropertyUsed : customPropertyUsedList) {
                CustomPropertyUsed copyCustomPropertyUsed = new CustomPropertyUsed(customPropertyUsed);
                copyCustomPropertyUsed.setExecution(execution);
                newExecutionCustomProperties.add(copyCustomPropertyUsed);
            }
        }
        return newExecutionCustomProperties;
    }

    public TestInstanceResult initTestInstanceResultForReRun(Execution oldExecution) {
        TestInstanceResult newTestInstanceResult = new TestInstanceResult();
        newTestInstanceResult.setTestInstance(oldExecution.getTestInstanceResult().getTestInstance());
        newTestInstanceResult.setTestSuite(oldExecution.getTestInstanceResult().getTestSuite());
        newTestInstanceResult.setTestName(oldExecution.getTestInstanceResult().getTestName());
        newTestInstanceResult.setTestKeyword(oldExecution.getTestInstanceResult().getTestKeyword());
        newTestInstanceResult.setLastStatus(oldExecution.getTestInstanceResult().getLastStatus());
        return newTestInstanceResult;
    }

    public Execution runTestInstance() {
        prepareExecution();
        if (PreferenceService.getString("keystore_directory") != null) {
            componentToExecute = execution.getTestInstanceResult().getTestSuite();
            WsdlProject wsdlProject = createWsdlProject();
            List<TestCaseRunner> testCaseRunnerList = (List<TestCaseRunner>) (List<?>) startExecution(wsdlProject);
            if (!testCaseRunnerList.isEmpty()) {
                List<GwtTestStepResult> gwtTestStepResultList = iterateThroughTestCaseRunnerList(testCaseRunnerList);
                execution.setTestStepResults(gwtTestStepResultList);
                execution.setProjectResults(currentProject.getGwtProject().getGwtProjectResults());
                execution = ExecutionDao.mergeExecution(execution);
            }
        } else {
            RunInstanceAction.LOG.error("The app preference keystore_directory is not set");
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The property keystore_directory is not set in the application configuration");
        }
        moveLogs();
        return execution;
    }

    public Execution run() {
        prepareExecution();
        WsdlProject wsdlProject = createWsdlProject();
        List<TestRunner> testRunners = startExecution(wsdlProject);
        if (!testRunners.isEmpty()) {
            List<GwtTestStepResult> gwtTestStepResultList;
            if (componentToExecute instanceof GwtProject) {
                gwtTestStepResultList = iterateThroughTestSuiteRunnerList((List<TestSuiteRunner>) (List<?>) testRunners);
            } else {
                gwtTestStepResultList = iterateThroughTestCaseRunnerList((List<TestCaseRunner>) (List<?>) testRunners);
            }
            execution.setTestStepResults(gwtTestStepResultList);
            execution.setProjectResults(currentProject.getGwtProject().getGwtProjectResults());
            execution = ExecutionDao.mergeExecution(execution);
        }
        moveLogs();
        return execution;
    }

    private void resetGwtProjectResultsAttributes(List<GwtProjectResult> gwtProjectResultList) {
        for (GwtProjectResult gwtProjectResult : gwtProjectResultList) {
            if (gwtProjectResult.getxValidatorInputInfoList() != null && gwtProjectResult.getTestStepInfoList() != null) {
                gwtProjectResult.setxValidatorInputInfoList(new ArrayList<XValidatorInputInfo>());
                gwtProjectResult.setTestStepInfoList(new ArrayList<TestStepInfo>());
                gwtProjectResult.setXvalidationResult(null);
            }
        }
    }

    public List<TestRunner> startExecution(WsdlProject project) {
        List<TestRunner> listResult = new ArrayList<>();
        Map<String, TestProperty> testProperties = project.getProperties();
        detectOidsInProjectCustomProperties(testProperties);
        if (project != null) {
            setCustomPropertiesInSoapUi(project);
            if (currentProject.getKeystore() != null) {
                addKeystore(currentProject.getKeystore());
            } else if (KeystoreDao.getDefaultKeystore() != null) {
                addKeystore(KeystoreDao.getDefaultKeystore());
            }
            project.getSettings().setBoolean(HttpSettings.RESPONSE_COMPRESSION, false);
            project.getSettings().setBoolean(WsdlSettings.PRETTY_PRINT_RESPONSE_MESSAGES, false);
            componentToExecute = HibernateUtil.initializeAndUnproxy(componentToExecute);
            if (componentToExecute instanceof GwtProject) {
                project.setAbortOnError(false);
                WsdlProjectRunner runner = project.run(new PropertiesMap(), false);
                listResult = (List<TestRunner>) (List<?>) runner.getResults();
            } else if (componentToExecute instanceof GwtTestSuite) {
                WsdlTestSuite testSuite = project.getTestSuiteByName(((GwtTestSuite) componentToExecute).getLabel());
                project.getTestSuiteList();
                testSuite.setAbortOnError(false);
                testSuite.setFailOnErrors(false);
                TestSuiteRunner runner = testSuite.run(new PropertiesMap(), false);
                listResult = (List<TestRunner>) (List<?>) runner.getResults();
            } else if (componentToExecute instanceof GwtTestCase) {
                WsdlTestCase testCase = project.getTestSuiteByName((((GwtTestCase) componentToExecute).getTestSuite()).getLabel())
                        .getTestCaseByName(((GwtTestCase) componentToExecute).getLabel());
                TestCaseRunner runner = testCase.run(new PropertiesMap(), false);
                listResult.add(runner);
            }
            SoapUI.shutdown();
        }
        return listResult;
    }

    public void setCustomPropertiesInSoapUi(WsdlProject project) {
        for (CustomPropertyUsed customPropertyUsed : execution.getCustomPropertiesUsed()) {
            TestComponent testComponent = customPropertyUsed.getCustomProperty().getTestComponent();
            testComponent = HibernateUtil.initializeAndUnproxy(testComponent);
            if (testComponent instanceof GwtProject) {
                project.getProperties().get(customPropertyUsed.getCustomProperty().getLabel()).setValue(customPropertyUsed.getValue());
            } else if (testComponent instanceof GwtTestSuite) {
                project.getTestSuiteByName(testComponent.getLabel()).getProperties().get(customPropertyUsed.getCustomProperty().getLabel())
                        .setValue(customPropertyUsed.getValue());
            } else if (testComponent instanceof GwtTestCase) {
                project.getTestSuiteByName(((GwtTestCase) testComponent).getTestSuite().getLabel()).getTestCaseByName(testComponent.getLabel())
                        .getProperties().get(customPropertyUsed.getCustomProperty().getLabel()).setValue(customPropertyUsed.getValue());
            }

        }
    }

    public void setPropertyValue(TestPropertyHolder testPropertyHolder, String property, String value) {
        if (testPropertyHolder.getProperty(property) != null) {
            testPropertyHolder.getProperty(property).setValue(value);
        }
    }

    private List<GwtTestStepResult> iterateThroughTestSuiteRunnerList(List<TestSuiteRunner> listResult) {
        List<GwtTestStepResult> gwtTestStepResultList = new ArrayList<>();
        for (TestSuiteRunner testSuiteRunner : listResult) {
            for (GwtTestStepResult gwtTestStepResult : iterateThroughTestCaseRunnerList(testSuiteRunner.getResults())) {
                gwtTestStepResultList.add(gwtTestStepResult);
            }
        }
        return gwtTestStepResultList;
    }


    private List<GwtTestStepResult> iterateThroughTestCaseRunnerList(List<TestCaseRunner> listResult) {
        List<GwtTestStepResult> gwtTestStepResultList = new ArrayList<>();

        for (TestCaseRunner results : listResult) {

            List<WsdlPropertiesTestStep> propertiesTestSteps = results.getTestCase().getTestStepsOfType(WsdlPropertiesTestStep.class);
            for (WsdlPropertiesTestStep propertytestStep : propertiesTestSteps) {
                String propertyTestStepName = propertytestStep.getLabel();
                if (propertyTestStepName.contains("xval_")) {
                    //We only keep the index in the property name
                    String xvalidatorIndex = propertyTestStepName.split("_")[1].split(" ")[0];
                    affectPropertiesToGwtProjectResult(propertytestStep, results, xvalidatorIndex);
                }
            }

            for (TestStepResult result : results.getResults()) {
                TransactionInstance transactionInstance = new TransactionInstance();
                if (execution.getTestInstanceResult() != null) {
                    if (result instanceof WsdlMessageExchangeTestStepResult) {

                        if (result.getTestStep().getTestCase().getProperty("transaction") != null) {
                            Transaction transaction = new Transaction();
                            transaction.setKeyword(result.getTestStep().getTestCase().getProperty("transaction").getValue());
                            transactionInstance.setTransaction(transaction);
                        }

                        for (MessageExchange messageExchange : ((WsdlMessageExchangeTestStepResult) result).getMessageExchanges()) {
                            gwtTestStepResultList.add(createGwtTestStepResultSoap(result, messageExchange, transactionInstance, execution));
                        }
                    }
                } else {
                    if (result instanceof WsdlMessageExchangeTestStepResult) {
                        for (MessageExchange messageExchange : ((WsdlMessageExchangeTestStepResult) result).getMessageExchanges()) {
                            gwtTestStepResultList.add(createGwtTestStepResultSoap(result, messageExchange, transactionInstance, execution));
                        }
                    } else if (result instanceof WsdlTestRequestStepResult) {
                        for (MessageExchange messageExchange : ((WsdlTestRequestStepResult) result).getMessageExchanges()) {
                            gwtTestStepResultList.add(createGwtTestStepResultSoap(result, messageExchange, transactionInstance, execution));
                        }
                    } else if (result instanceof RestRequestStepResult) {
                        for (MessageExchange messageExchange : ((RestRequestStepResult) result).getMessageExchanges()) {
                            gwtTestStepResultList.add(createGwtTestStepResultRest(result, messageExchange, transactionInstance, execution));
                        }
                    }
                }
            }
        }
        return gwtTestStepResultList;
    }

    // To bean ?
    public void moveLogs() {
        if (PreferenceService.getString("logs_directory") != null) {
            try {
                String path = PreferenceService.getString("logs_directory") + this.execution.getId() + "/";
                File file = new File(path);
                file.mkdirs();
                if (file.exists()) {
                    if (new File("soapui.log").exists()) {
                        Files.move(Paths.get("soapui.log"), Paths.get(path + "soapui.log"));
                    }
                    if (new File("soapui-errors.log").exists()) {
                        Files.move(Paths.get("soapui-errors.log"), Paths.get(path + "soapui-errors.log"));
                    }
                    if (new File("global-groovy.log").exists()) {
                        Files.move(Paths.get("global-groovy.log"), Paths.get(path + "global-groovy.log"));
                    }
                } else {
                    LOG.error("moveLogs() : " + path + " directory couldn't be created : log files will not be moved");
                }
            } catch (IOException e) {
                LOG.error("moveLogs() " + e.getMessage());
            }
        }
    }

    private void detectOidsInProjectCustomProperties(Map<String, TestProperty> testProperties) {

        Set<Map.Entry<String, TestProperty>> setTestProperties = testProperties.entrySet();
        Iterator<Map.Entry<String, TestProperty>> testPropertiesIterator = setTestProperties.iterator();
        List<OidProperty> oidPropertyList = new ArrayList<>();
        List<GwtProjectResult> gwtProjectResultList = new ArrayList<>();
        List<String> indexList = new ArrayList<>();
        while (testPropertiesIterator.hasNext()) {
            Map.Entry<String, TestProperty> entry = testPropertiesIterator.next();
            if (entry.getKey().contains("oid_xval_")) {
                if (!currentProject.getGwtProject().getOidPropertyList().contains(entry.getValue().getValue())) {
                    String[] stringSplitted = entry.getKey().split("_");
                    String index = stringSplitted[stringSplitted.length - 1];
                    if (!indexList.contains(index)) {
                        indexList.add(index);
                        oidPropertyList.add(new OidProperty(currentProject.getGwtProject(), index, entry.getValue().getValue()));
                        GwtProjectResult projectResult = new GwtProjectResult(currentProject.getGwtProject(), index, entry.getValue().getValue(), execution);
                        gwtProjectResultList.add(projectResult);
                    }
                }
            }
        }
        if (currentProject.getGwtProject().getGwtProjectResults() == null) {
            currentProject.getGwtProject().setGwtProjectResults(gwtProjectResultList);
        } else {
            currentProject.getGwtProject().addGwtProjectResult(gwtProjectResultList);
        }
        currentProject.getGwtProject().setOidPropertyList(oidPropertyList);
        execution.setProjectResults(gwtProjectResultList);
    }

    private void affectPropertiesToGwtProjectResult(WsdlPropertiesTestStep propertiesTestStep, TestCaseRunner runner, String xvalidatorIndex) {

        GwtProjectResult correspondingGwtProjectResult = getGwtProjectResultFromXValidatorIndex(xvalidatorIndex);

        if (correspondingGwtProjectResult != null) {
            List<TestStepInfo> testStepInfoList = new ArrayList<>();
            List<XValidatorInputInfo> xValidatorInputInfoList = new ArrayList<>();

            Map<String, TestProperty> testPropertyMap = propertiesTestStep.getProperties();
            Set<Map.Entry<String, TestProperty>> setTestProperties = testPropertyMap.entrySet();
            Iterator<Map.Entry<String, TestProperty>> testPropertiesIterator = setTestProperties.iterator();
            while (testPropertiesIterator.hasNext()) {
                Map.Entry<String, TestProperty> entry = testPropertiesIterator.next();

                String inputName = entry.getKey().trim();

                String propertyValue = entry.getValue().getValue();
                String[] splittedPropertyValue = propertyValue.split("/");

                String testStepName = splittedPropertyValue[0];

                String inputType = splittedPropertyValue[1];

                byte[] messageContent = new byte[0];

                for (TestStepResult testStepResult : runner.getResults()) {
                    TransactionInstance transactionInstance = new TransactionInstance();
                    if (testStepResult.getTestStep().getLabel().equals(testStepName)) {
                        if (testStepResult instanceof WsdlMessageExchangeTestStepResult) {
                            for (MessageExchange messageExchange : ((WsdlMessageExchangeTestStepResult) testStepResult).getMessageExchanges()) {
                                messageContent = extractMessageFromTransactionInstance(messageExchange, transactionInstance, inputType);
                            }
                        } else if (testStepResult instanceof WsdlTestRequestStepResult) {
                            for (MessageExchange messageExchange : ((WsdlTestRequestStepResult) testStepResult).getMessageExchanges()) {
                                messageContent = extractMessageFromTransactionInstance(messageExchange, transactionInstance, inputType);
                            }
                        }
                    }
                }
                TestStepInfo testStepInfo = new TestStepInfo(correspondingGwtProjectResult, testStepName, inputType);
                testStepInfoList.add(testStepInfo);

                if (messageContent.length > 0) {
                    XValidatorInputInfo xValidatorInputInfo = new XValidatorInputInfo(correspondingGwtProjectResult, inputName, messageContent, inputType);
                    xValidatorInputInfoList.add(xValidatorInputInfo);
                } else {
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR, "One of the test step from the project result " +
                            "in empty message content during extraction, an input will be missing for cross validation process");
                }
            }

            if (correspondingGwtProjectResult.getTestStepInfoList() == null || correspondingGwtProjectResult.getTestStepInfoList().isEmpty()) {
                correspondingGwtProjectResult.setTestStepInfoList(testStepInfoList);
            } else {
                correspondingGwtProjectResult.addTestStepInfoToList(testStepInfoList);
            }

            if (correspondingGwtProjectResult.getxValidatorInputInfoList() == null || correspondingGwtProjectResult.getxValidatorInputInfoList().isEmpty()) {
                correspondingGwtProjectResult.setxValidatorInputInfoList(xValidatorInputInfoList);
            } else {
                correspondingGwtProjectResult.addxValidatorInputInfoToList(xValidatorInputInfoList);
            }
        }

    }

    private GwtProjectResult getGwtProjectResultFromXValidatorIndex(String xvalidatorIndex) {
        GwtProjectResultQuery query = new GwtProjectResultQuery();
        query.execution().id().eq(execution.getId());
        query.xvalidatorOidIndex().eq(xvalidatorIndex);
        return query.getUniqueResult();
    }

    private byte[] extractMessageFromTransactionInstance(MessageExchange messageExchange, TransactionInstance transactionInstance, String inputType) {
        byte[] messageContent;
        if (inputType.equals("REQUEST")) {
            MessageInstance request = new MessageInstance();
            request.setContent(messageExchange.getRequestContent());
            transactionInstance.setRequest(request);
            LOG.info("Request : " + messageExchange.getRequestContent());
            messageContent = transactionInstance.getRequest().getContent();
            return messageContent;
        } else if (inputType.equals("RESPONSE")) {
            MessageInstance response = new MessageInstance();
            response.setContent(messageExchange.getResponseContent());
            transactionInstance.setResponse(response);
            LOG.info("Response : " + messageExchange.getResponseContent());
            messageContent = transactionInstance.getResponse().getContent();
            return messageContent;
        } else {
            LOG.error("Error wrong inputType" + inputType);
            return null;
        }
    }

    public GwtTestStepResult createGwtTestStepResultSoap(TestStepResult result, MessageExchange messageExchange, TransactionInstance
            transactionInstance, Execution execution) {
        MessageInstance request = new MessageInstance();
        request.setContent(messageExchange.getRequestContent());
        request.setIssuer("GWT");
        transactionInstance.setRequest(request);
        LOG.info("Request : " + messageExchange.getRequestContent());

        List<GwtAttachment> attachmentListRequest = getGwtAttachmentsRequest(messageExchange.getRequestAttachments());
        List<GwtAttachment> attachmentListResponse = getGwtAttachmentsRequest(messageExchange.getResponseAttachments());

        MessageInstance response = new MessageInstance();
        response.setContent(messageExchange.getResponseContent());
        transactionInstance.setResponse(response);
        LOG.info("Response : " + messageExchange.getResponseContent());

        transactionInstance.setTimestamp(new Date(result.getTimeStamp()));

        return new GwtTestStepResult(result.getStatus().toString(), result.getTimeTaken(), transactionInstance, GwtTestStepDao.getGwtTestStep
                (result, this.currentProject.getLabel()), execution, messageExchange.getEndpoint(), Arrays.asList(result.getMessages()), TestStepType.SOAP, attachmentListRequest, attachmentListResponse);
    }

    public GwtTestStepResult createGwtTestStepResultRest(TestStepResult result, MessageExchange messageExchange, TransactionInstance
            transactionInstance, Execution execution) {
        MessageInstance request = new MessageInstance();
        request.setIssuer("GWT");
        Response responseSoapui = messageExchange.getResponse();
        if (responseSoapui instanceof SinglePartHttpResponse) {
            request.setContent(new String(messageExchange.getRawRequestData()) + "\n\n" + messageExchange.getRequestContent());
        }
        if (responseSoapui instanceof MimeMessageResponse) {
            request.setContent(new String(messageExchange.getRawRequestData()) + "\n\n" + messageExchange.getRequestContent());
        }
        transactionInstance.setRequest(request);
        LOG.info("Request : " + messageExchange.getEndpoint());

        MessageInstance response = new MessageInstance();

        if (responseSoapui != null) {
            if (Boolean.TRUE.equals(ApplicationConfiguration.getBooleanValue("dump_dicom_to_xml"))) {
                DicomParser dicomParser = new DicomParser(messageExchange.getRawResponseData(), responseSoapui.getContentType());
                String dumpToXml = dicomParser.dumpDicomwebResponseToXml();
                response.setPayload(messageExchange.getResponseHeaders().toString().getBytes());
                if (dumpToXml != null) {
                    response.setContent(dumpToXml);
                } else {
                    response.setContent(messageExchange.getResponseContent());
                }
            } else {
                response.setPayload(messageExchange.getResponseHeaders().toString().getBytes());
                response.setContent(messageExchange.getResponseContent());
            }
        }

        transactionInstance.setResponse(response);
        LOG.info("Response : " + messageExchange.getResponseContent());


        // message.exchange.requestContent to display
        transactionInstance.setTimestamp(new Date(result.getTimeStamp()));

        return new GwtTestStepResult(result.getStatus().toString(), result.getTimeTaken(), transactionInstance, GwtTestStepDao.getGwtTestStep
                (result, this.currentProject.getLabel()), execution, messageExchange.getEndpoint(), Arrays.asList(result.getMessages()), TestStepType.REST, null, null);
    }

    private List<GwtAttachment> getGwtAttachmentsResponse(MessageExchange messageExchange) {
        Attachment[] attachments = messageExchange.getResponseAttachments();
        if (attachments != null) {
            for (Attachment attachment : attachments) {
                if (attachment instanceof BodyPartAttachment) {
                    BodyPartAttachment bodyPartAttachment = (BodyPartAttachment) attachment;

                    GwtAttachment gwtAttachment = new GwtAttachment();

                }
            }
        }
        return null;
    }

    private List<GwtAttachment> getGwtAttachmentsRequest(Attachment[] attachments) {
        List<GwtAttachment> attachmentList = new ArrayList<>();
        if (attachments != null) {
            for (Attachment attachment : attachments) {
                try {
                    StringBuilder builder = new StringBuilder();
                    InputStream in = attachment.getInputStream();
                    byte[] contents = new byte[1024];

                    int bytesRead = 0;
                    while ((bytesRead = in.read(contents)) != -1) {
                        builder.append(new String(contents, 0, bytesRead));
                    }

                    GwtAttachment gwtAttachment = new GwtAttachment();
                    gwtAttachment.setContent(builder.toString().getBytes());
                    gwtAttachment.setContentId(attachment.getContentID());
                    gwtAttachment.setContentType(attachment.getContentType());
                    gwtAttachment.setEncoding(attachment.getEncoding().name());
                    gwtAttachment.setName(attachment.getName());
                    gwtAttachment.setPart(attachment.getPart());
                    gwtAttachment.setSize(attachment.getSize());
                    gwtAttachment.setUrl(attachment.getUrl());
                    gwtAttachment.save();
                    attachmentList.add(gwtAttachment);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return attachmentList;
    }

    public void prepareExecution() {
        execution.setLaunchBy(Application.getUser());
        execution.setCompany(Application.getCompanyKeyword());
        execution.setLaunchDate(new Date());
        execution.setSoapuiProject(currentProject);
        if (componentToExecute != null) {
            execution.setTestComponent((TestComponent) componentToExecute);
        }
    }

    public void createExecutionWithCustomProperties() {
        execution = new Execution();
        componentToExecute = HibernateUtil.initializeAndUnproxy(componentToExecute);
        Hibernate.initialize(componentToExecute.getCustomProperties());
        execution.setCustomPropertiesUsed(getCustomPropertiesUsed(componentToExecute));
    }

    public boolean createExecutionAndTestInstanceResult() {
        GazelleTRMServiceStub.TrmTestInstance testInstance = WsClient.getTestInstanceById(testInstanceId);
        if (testInstance != null) {
            execution = new Execution();
            prepareExecution();
            GwtTestSuite gwtTestSuite = GwtTestSuiteDao.getGwtTestSuite(testInstance.getTest().getKeyword(), currentProject.getLabel());
            if (gwtTestSuite != null) {
                execution.setTestInstanceResult(new TestInstanceResult(testInstanceId, gwtTestSuite));
                execution.getTestInstanceResult().setExecution(execution);
                execution = ExecutionDao.mergeExecution(execution);
                return true;
            } else {
                execution = null;
                RunInstanceAction.LOG.error("There is no test available for this test instance in this project");
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "There is no test available for this test instance in this project");
                return false;
            }
        } else {
            RunInstanceAction.LOG.error("There is no test instance with this id");
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "There is no test instance with this id");
            return false;
        }
    }

    public List<StepInfo> getSystemKeywordInfo(List<String> transactions) {
        List<StepInfo> stepInfos = null;
        try {
            GazelleTRMServiceStub.TrmTestInstance trmTestInstance = WsClient.getTestInstanceById(testInstanceId);
            stepInfos = new ArrayList<>();

            execution.getTestInstanceResult().setLastStatus(trmTestInstance.getLastStatus());

            GazelleTRMServiceStub.TrmTest trmTest = trmTestInstance.getTest();
            execution.getTestInstanceResult().setTestKeyword(trmTest.getKeyword());
            execution.getTestInstanceResult().setTestName(trmTest.getName());

            GazelleTRMServiceStub.TrmTestStep[] trmTestSteps = trmTest.getTrmTestSteps();
            for (GazelleTRMServiceStub.TrmTestStep trmTestStep : trmTestSteps) {
                if (transactions.contains(trmTestStep.getTransaction())) {
                    StepInfo newStepInfo = new StepInfo(trmTestStep.getTransaction(), trmTestStep.getTestRoleResponderId());
                    newStepInfo.setTestRoleInitiatorId(trmTestStep.getTestRoleInitiatorId());
                    newStepInfo.setTransactionUsage(trmTestStep.getWsTransactionUsage());

                    GazelleTRMServiceStub.TrmTestRole[] trmTestRoles = trmTestInstance.getTest().getTrmTestRoles();
                    for (GazelleTRMServiceStub.TrmTestRole trmTestRole : trmTestRoles) {
                        if ((trmTestRole.getRoleInTest().getKeyword().substring(0, 2).equals("CC") || trmTestRole.getRoleInTest().getKeyword()
                                .substring(0, 2).equals("SS")) && newStepInfo.getTestRoleResponderId() == trmTestRole.getId()) {
                            newStepInfo.setParticipantRoleInTestId(trmTestRole.getRoleInTest().getId());
                            newStepInfo.setRoleInTest(trmTestRole.getRoleInTest().getKeyword());

                            GazelleTRMServiceStub.TrmParticipant[] trmParticipants = trmTestInstance.getParticipants();
                            for (GazelleTRMServiceStub.TrmParticipant trmParticipant : trmParticipants) {
                                if (newStepInfo.getParticipantRoleInTestId() == trmParticipant.getRoleInTestId()) {
                                    newStepInfo.setResponderSystemKeyword(trmParticipant.getSystem());
                                }
                            }
                        } else if (newStepInfo.getTestRoleInitiatorId() == trmTestRole.getId()) {
                            newStepInfo.setInitiatorSystemKeyword(trmTestRole.getRoleInTest().getKeyword());
                        }
                    }
                    if (newStepInfo.getInitiatorSystemKeyword().substring(0, 3).equals("HPP") || newStepInfo.getInitiatorSystemKeyword().substring
                            (0, 2).equals("PP")) {
                        stepInfos.add(newStepInfo);
                    }
                }
            }

        } catch (Exception e) {
            RunInstanceAction.LOG.error(e.getMessage());
        }

        return stepInfos;
    }

    public List<StepInfo> getEndPointBySystem(List<StepInfo> stepInfoList) {
        List<String> systemList = new ArrayList<>();
        for (StepInfo stepInfo : stepInfoList) {
            if (!systemList.contains(stepInfo.getResponderSystemKeyword())) {
                systemList.add(stepInfo.getResponderSystemKeyword());
            }
        }
        for (String system : systemList) {
            List<GazelleProxyWebServiceNameStub.ListConfigurationsBySystemBySessionByTypeAsCSVResponse> listResponse = new ArrayList<>();
            listResponse.add(WsClient.listConfigurationsBySystemBySessionByTypeAsCSV("Webservice", system, "1"));
            listResponse.add(WsClient.listConfigurationsBySystemBySessionByTypeAsCSV("HL7V3Responder", system, "1"));

            for (GazelleProxyWebServiceNameStub.ListConfigurationsBySystemBySessionByTypeAsCSVResponse response : listResponse) {
                if (response != null) {
                    String responseAsCsv = response.get_return();

                    //The column assigningAuthority is duplicated when the configurationType is HL7V3Responder causing an error in CSVFormat.
                    responseAsCsv = responseAsCsv.replaceFirst("assigningAuthority", "");

                    Reader in = new StringReader(responseAsCsv);
                    try {
                        Iterable<CSVRecord> records = CSVFormat.RFC4180.withFirstRecordAsHeader().parse(in);
                        for (CSVRecord record : records) {
                            String wstype = record.get("ws-type");
                            for (StepInfo stepInfo : stepInfoList) {
                                if (stepInfo.getResponderSystemKeyword() != null && stepInfo.getTransaction() != null) {
                                    if (stepInfo.getTransactionUsage() != null) {
                                        if (stepInfo.getResponderSystemKeyword().equals(system) && wstype.contains(stepInfo.getTransaction()) &&
                                                wstype.contains(stepInfo.getTransactionUsage())) {
                                            stepInfo.setEndpoint(record.get("url"));
                                        }
                                    } else {
                                        if (stepInfo.getResponderSystemKeyword().equals(system) && wstype.contains(stepInfo.getTransaction())) {
                                            stepInfo.setEndpoint(record.get("url"));
                                        }
                                    }
                                }
                            }
                        }
                        in.close();
                    } catch (IOException e) {
                        LOG.error("getEndPointBySystem() : " + e.getMessage());
                    }
                }
            }
        }
        return stepInfoList;
    }

    public void getAllInfo() {
        if (createExecutionAndTestInstanceResult()) {
            initializeCustomPropertiesUsedForTestInstance();
            List<StepInfo> stepInfoList = getSystemKeywordInfo(retrieveTransaction());
            stepInfoList = getEndPointBySystem(stepInfoList);
            List<CustomPropertyUsed> customPropertyUsedList = execution.getCustomPropertiesUsed();
            for (StepInfo stepInfo : stepInfoList) {
                for (CustomPropertyUsed customPropertyUsed : customPropertyUsedList) {
                    if (customPropertyUsed.getCustomProperty().getLabel().equals(transactionToSoapProperty(stepInfo.getTransaction()))) {
                        customPropertyUsed.setValue(stepInfo.getEndpoint());
                    }
                }
            }
        }
    }

    public List<String> retrieveTransaction() {
        WsdlProject project = createWsdlProject();
        List<String> transactions = new ArrayList<>();
        if (project != null) {
            List<TestCase> testCaseList = project.getTestSuiteByName(execution.getTestInstanceResult().getTestSuite().getLabel()).getTestCaseList();
            for (TestCase testCase : testCaseList) {
                for (TestStep testStep : testCase.getTestStepList()) {
                    if (testStep instanceof WsdlRunTestCaseTestStep) {
                        if (((WsdlRunTestCaseTestStep) testStep).getTargetTestCase().getProperty("transaction") != null) {
                            transactions.add(((WsdlRunTestCaseTestStep) testStep).getTargetTestCase().getProperty("transaction").getValue());
                        }
                    }
                    testStep.getLabel();
                }
            }
        }
        return transactions;
    }

    public void initializeCustomPropertiesUsedForTestInstance() {
        List<CustomPropertyUsed> customPropertyUsedList = new ArrayList<>();
        for (CustomProperty customProperty : execution.getTestInstanceResult().getTestSuite().getCustomProperties()) {
            CustomPropertyUsed customPropertyUsed = new CustomPropertyUsed();
            customPropertyUsed.setExecution(execution);
            customPropertyUsed.setCustomProperty(customProperty);
            customPropertyUsed.setValue(customProperty.getDefaultValue());
            customPropertyUsedList.add(customPropertyUsed);
        }
        execution.setCustomPropertiesUsed(customPropertyUsedList);
    }

    public List<CustomPropertyUsed> getCustomPropertiesUsed(TestComponent testComponent) {
        List<CustomPropertyUsed> customPropertyUsedList = new ArrayList<>();
        if (testComponent instanceof GwtProject) {
            Hibernate.initialize(((GwtProject) testComponent).getTestSuites());
            customPropertyUsedList = getProjectCustomPropertiesUsed(customPropertyUsedList, (GwtProject) testComponent, true);
        } else if (testComponent instanceof GwtTestSuite) {
            customPropertyUsedList = getTestSuiteCustomPropertiesUsed(customPropertyUsedList, (GwtTestSuite) testComponent, true);
            customPropertyUsedList = getProjectCustomPropertiesUsed(customPropertyUsedList, ((GwtTestSuite) testComponent).getProject(), false);
        } else if (testComponent instanceof GwtTestCase) {
            customPropertyUsedList = getTestCaseCustomPropertiesUsed(customPropertyUsedList, (GwtTestCase) testComponent, true);
            customPropertyUsedList = getTestSuiteCustomPropertiesUsed(customPropertyUsedList, ((GwtTestCase) testComponent).getTestSuite(), false);
            customPropertyUsedList = getProjectCustomPropertiesUsed(customPropertyUsedList, ((GwtTestCase) testComponent).getTestSuite().getProject
                    (), false);
        }
        return customPropertyUsedList;
    }

    public List<CustomPropertyUsed> getProjectCustomPropertiesUsed(List<CustomPropertyUsed> customPropertyUsedList, GwtProject gwtProject, boolean
            propagate) {
        List<CustomProperty> customProperties = gwtProject.getCustomProperties();
        if (customProperties != null) {
            for (CustomProperty customProperty : customProperties) {
                addCustomPropertyUsedToCustomProperty(customPropertyUsedList, customProperty);
            }
        }
        if (propagate) {
            for (GwtTestSuite gwtTestSuite : gwtProject.getTestSuites()) {
                customPropertyUsedList = getTestSuiteCustomPropertiesUsed(customPropertyUsedList, gwtTestSuite, true);
            }
        }
        return customPropertyUsedList;
    }

    public List<CustomPropertyUsed> getTestSuiteCustomPropertiesUsed(List<CustomPropertyUsed> customPropertyUsedList, GwtTestSuite gwtTestSuite,
                                                                     boolean propagate) {
        List<CustomProperty> customProperties = gwtTestSuite.getCustomProperties();
        if (customProperties != null) {
            for (CustomProperty customProperty : customProperties) {
                addCustomPropertyUsedToCustomProperty(customPropertyUsedList, customProperty);
            }
        }
        if (propagate) {
            for (GwtTestCase gwtTestCase : gwtTestSuite.getTestCases()) {
                customPropertyUsedList = getTestCaseCustomPropertiesUsed(customPropertyUsedList, gwtTestCase, true);
            }
        }
        return customPropertyUsedList;
    }

    public List<CustomPropertyUsed> getTestCaseCustomPropertiesUsed(List<CustomPropertyUsed> customPropertyUsedList, GwtTestCase gwtTestCase,
                                                                    boolean propagate) {
        List<CustomProperty> customProperties = gwtTestCase.getCustomProperties();
        if (customProperties != null) {
            for (CustomProperty customProperty : customProperties) {
                addCustomPropertyUsedToCustomProperty(customPropertyUsedList, customProperty);
            }
        }
        return customPropertyUsedList;
    }

    public void addCustomPropertyUsedToCustomProperty(List<CustomPropertyUsed> customPropertyUsedList, CustomProperty customProperty) {
        if (customProperty.isToModify()) {
            CustomPropertyUsed customPropertyUsed = new CustomPropertyUsed(customProperty.getDefaultValue(), execution, customProperty);
            customPropertyUsedList.add(customPropertyUsed);
            if (customProperty.getCustomPropertyUsedList() == null) {
                List<CustomPropertyUsed> customPropertiesUsed = new ArrayList<>();
                customPropertiesUsed.add(customPropertyUsed);
                customProperty.setCustomPropertyUsedList(customPropertiesUsed);
            } else {
                customProperty.getCustomPropertyUsedList().add(customPropertyUsed);
            }
        }
    }

    public void addKeystore(Keystore keystore) {
        if (keystore != null) {
            if (keystore.getPassword() != null) {
                SoapUI.getSettings().setString(SSLSettings.KEYSTORE_PASSWORD, keystore.getPassword());
            }
            SoapUI.getSettings().setString(SSLSettings.KEYSTORE, keystore.getPath());
            SoapUI.getSettings().setBoolean(HttpSettings.AUTHENTICATE_PREEMPTIVELY, true);
        }
    }

    public boolean customPropertiesToModifyNull() {
        boolean propertiesToModifyNull = true;
        if (currentProject.getGwtProject().getCustomPropertiesToModify().size() > 0) {
            propertiesToModifyNull = false;
        }
        for (TestComponent testComponent : currentProject.getGwtProject().getTestSuitesWithCustomPropertiesToModify(componentToExecute)) {
            if (testComponent.getCustomPropertiesToModify().size() > 0) {
                propertiesToModifyNull = false;
            }
            for (GwtTestCase gwtTestCase : ((GwtTestSuite) testComponent).getTestCases()) {
                if (gwtTestCase.getCustomPropertiesToModify().size() > 0) {
                    propertiesToModifyNull = false;
                }
            }
        }
        return propertiesToModifyNull;
    }

    public String transactionToSoapProperty(String transaction) {
        switch (transaction) {
            case "ITI-18":
                return "endPointRegistry";
            case "ITI-41":
                return "endPointXDS";
            case "ITI-43":
                return "endPointRetrieve";
            case "ITI-45":
                return "endPointPIX";
            case "ITI-47":
                return "endPointPDQ";
            case "ITI-62":
                return "endPointDelete";
            case "RAD-69":
                return "endPointRetrieveImaging";
            case "CH:XUA Get X-User Assertion":
                return "endPointAssertions";
            case "ITI-57":
                return "endPointUpdate";
            default:
                return null;
        }
    }
}
