package net.ihe.gazelle.wstester.model;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.List;

@Entity
@DiscriminatorValue("testCase")
public class GwtTestCase extends TestComponent implements Serializable {

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "test_suite_id")
    private GwtTestSuite testSuite;

    @OneToMany(
            targetEntity = GwtTestStep.class,
            mappedBy = "testCase",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<GwtTestStep> testSteps;

    public GwtTestCase() {
        super();
    }

    public GwtTestCase(GwtTestSuite testSuite, List<GwtTestStep> testSteps) {
        this.testSuite = testSuite;
        this.testSteps = testSteps;
    }

    public GwtTestSuite getTestSuite() {
        return testSuite;
    }

    public void setTestSuite(GwtTestSuite testSuite) {
        this.testSuite = testSuite;
    }

    public List<GwtTestStep> getTestSteps() {
        return testSteps;
    }

    public void setTestSteps(List<GwtTestStep> testSteps) {
        this.testSteps = testSteps;
    }

    @Override
    public String getClassName() {
        return GwtTestCase.class.getSimpleName();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        GwtTestCase testCase = (GwtTestCase) o;

        if (!testSuite.equals(testCase.testSuite)) {
            return false;
        }
        return testSteps.equals(testCase.testSteps);
    }

    @Override
    public int hashCode() {
        int result = testSuite.hashCode();
        result = 31 * result + testSteps.hashCode();
        return result;
    }
}
