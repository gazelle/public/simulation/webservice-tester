package net.ihe.gazelle.wstester.model;

import net.ihe.gazelle.wstester.dao.ExecutionDao;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "gwt_execution")
@SequenceGenerator(name = "gwt_execution_sequence", sequenceName = "gwt_execution_id_seq", allocationSize = 1)
public class Execution implements Serializable {

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gwt_execution_sequence")
    private Integer id;

    @Column(name = "launch_by", nullable = false)
    private String launchBy;

    @Column(name = "company")
    private String company;

    @Column(name = "status")
    private String status;

    @Column(name = "launch_date", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date launchDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "soapui_project_id")
    private SoapuiProject soapuiProject;

    @ManyToOne(fetch = FetchType.LAZY)
    @Fetch(value = FetchMode.SELECT)
    @JoinColumn(name = "test_component_id")
    private TestComponent testComponent;

    @OneToMany(
            fetch = FetchType.LAZY,
            targetEntity = GwtTestStepResult.class,
            mappedBy = "execution",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @Fetch(value = FetchMode.SUBSELECT)
    private List<GwtTestStepResult> testStepResults;

    @OneToMany(fetch = FetchType.EAGER,
            targetEntity = GwtProjectResult.class,
            mappedBy = "execution",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @Fetch(value = FetchMode.SUBSELECT)
    private List<GwtProjectResult> projectResults;

    @OneToMany(
            fetch = FetchType.EAGER,
            targetEntity = CustomPropertyUsed.class,
            mappedBy = "execution",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @Fetch(value = FetchMode.SUBSELECT)
    private List<CustomPropertyUsed> customPropertiesUsed;

    @OneToOne(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    @JoinColumn(name = "execution")
    private TestInstanceResult testInstanceResult;

    @Transient
    private String testComponentToString;

    public Execution() {
        super();
    }

    public Execution(String launchBy, Date launchDate, List<GwtTestStepResult> testStepResults, List<GwtProjectResult> projectResults) {
        this.launchBy = launchBy;
        this.launchDate = new Date(launchDate.getTime());
        this.testStepResults = testStepResults;
        this.projectResults = projectResults;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLaunchBy() {
        return launchBy;
    }

    public void setLaunchBy(String launchBy) {
        this.launchBy = launchBy;
    }

    public Date getLaunchDate() {
        return new Date(launchDate.getTime());
    }

    public void setLaunchDate(Date launchDate) {
        this.launchDate = new Date(launchDate.getTime());
    }

    public List<GwtTestStepResult> getTestStepResults() {

        Collections.sort(testStepResults, new Comparator<GwtTestStepResult>() {
            @Override
            public int compare(GwtTestStepResult testStepResult2, GwtTestStepResult testStepResult1) {

                return testStepResult2.getId().compareTo(testStepResult1.getId());
            }
        });

        return testStepResults;
    }

    public void setTestStepResults(List<GwtTestStepResult> testStepResults) {
        this.testStepResults = testStepResults;
    }

    public List<GwtProjectResult> getProjectResults() {
        Collections.sort(projectResults, new Comparator<GwtProjectResult>() {
            @Override
            public int compare(GwtProjectResult projectResult2, GwtProjectResult projectResult1) {

                return projectResult2.getId().compareTo(projectResult1.getId());
            }
        });

        return projectResults;
    }

    public void setProjectResults(List<GwtProjectResult> projectResults) {
        if (this.projectResults == null) {
            this.projectResults = projectResults;
        }
    }

    public List<CustomPropertyUsed> getCustomPropertiesUsed() {
        return customPropertiesUsed;
    }

    public void setCustomPropertiesUsed(List<CustomPropertyUsed> customPropertiesUsed) {
        this.customPropertiesUsed = customPropertiesUsed;

    }

    public SoapuiProject getSoapuiProject() {
        return soapuiProject;
    }

    public void setSoapuiProject(SoapuiProject soapuiProject) {
        this.soapuiProject = soapuiProject;
    }

    public TestComponent getTestComponent() {
        return testComponent;
    }

    public void setTestComponent(TestComponent testComponent) {
        this.testComponent = testComponent;
    }

    public TestInstanceResult getTestInstanceResult() {
        return testInstanceResult;
    }

    public void setTestInstanceResult(TestInstanceResult testInstanceResult) {
        this.testInstanceResult = testInstanceResult;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public void setTestComponentToString(String testComponentToString) {
        this.testComponentToString = testComponentToString;
    }

    public String getStatus() {
        if (status == null) {
            ExecutionDao.mergeExecution(this); // this will update the status
        }
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void updateGlobalStatus() {
        if (testStepResults == null || testStepResults.size() == 0) {
            setStatus("UNKNOWN");
        } else {
            boolean isPassed = true;
            for (GwtTestStepResult testStepResult : testStepResults) {
                if (testStepResult.getStatus().equals("FAILED")
                        || (testStepResult.getResponseValidation() != null && testStepResult.getResponseValidation().getValidationStatus().equals("FAILED"))
                        || (testStepResult.getRequestValidation() != null && testStepResult.getRequestValidation().getValidationStatus().equals("FAILED"))) {
                    setStatus("FAILED");
                    isPassed = false;
                    break;
                }
                if (testStepResult.getStatus().equals("UNKNOWN")) {
                    setStatus(testStepResult.getStatus());
                    isPassed = false;
                    break;
                }

            }
            if (isPassed) {
                setStatus("PASSED");
            }
        }

    }

    public String getTestComponentToString() {
        if (testComponent != null) {
            if (testComponent instanceof GwtTestCase) {
                setTestComponentToString(((GwtTestCase) testComponent).getTestSuite().getLabel() + " > " + testComponent.getLabel());
            } else {
                setTestComponentToString(testComponent.getLabel());
            }
        }
        return testComponentToString;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Execution execution = (Execution) o;

        if (!launchBy.equals(execution.launchBy)) {
            return false;
        }
        if (company != null ? !company.equals(execution.company) : execution.company != null) {
            return false;
        }
        if (status != null ? !status.equals(execution.status) : execution.status != null) {
            return false;
        }
        if (!launchDate.equals(execution.launchDate)) {
            return false;
        }
        if (soapuiProject != null ? !soapuiProject.equals(execution.soapuiProject) : execution.soapuiProject != null) {
            return false;
        }
        if (testComponent != null ? !testComponent.equals(execution.testComponent) : execution.testComponent != null) {
            return false;
        }
        if (testStepResults != null ? !testStepResults.equals(execution.testStepResults) : execution.testStepResults != null) {
            return false;
        }
        if (projectResults != null ? !projectResults.equals(execution.projectResults) : execution.projectResults != null) {
            return false;
        }
        if (customPropertiesUsed != null ? !customPropertiesUsed.equals(execution.customPropertiesUsed) : execution.customPropertiesUsed != null) {
            return false;
        }
        if (testInstanceResult != null ? !testInstanceResult.equals(execution.testInstanceResult) : execution.testInstanceResult != null) {
            return false;
        }
        return testComponentToString != null ? testComponentToString.equals(execution.testComponentToString) : execution.testComponentToString ==
                null;
    }

    @Override
    public int hashCode() {
        int result = launchBy.hashCode();
        result = 31 * result + (company != null ? company.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + launchDate.hashCode();
        result = 31 * result + (soapuiProject != null ? soapuiProject.hashCode() : 0);
        result = 31 * result + (testComponent != null ? testComponent.hashCode() : 0);
        result = 31 * result + (testStepResults != null ? testStepResults.hashCode() : 0);
        result = 31 * result + (projectResults != null ? projectResults.hashCode() : 0);
        result = 31 * result + (customPropertiesUsed != null ? customPropertiesUsed.hashCode() : 0);
        result = 31 * result + (testInstanceResult != null ? testInstanceResult.hashCode() : 0);
        result = 31 * result + (testComponentToString != null ? testComponentToString.hashCode() : 0);
        return result;
    }
}
