package net.ihe.gazelle.wstester.action;

import com.uwyn.jhighlight.renderer.XhtmlRendererFactory;
import net.ihe.gazelle.evsclient.connector.api.EVSClientResults;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.simulator.message.action.AbstractTransactionInstanceDisplay;
import net.ihe.gazelle.simulator.message.model.MessageInstance;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.simulator.message.model.TransactionInstanceQuery;
import net.ihe.gazelle.wstester.dao.MockValidationResultDaoImpl;
import net.ihe.gazelle.wstester.model.MockValidation;
import net.ihe.gazelle.wstester.model.MockValidationResult;
import net.ihe.gazelle.wstester.util.DownloadFile;
import net.ihe.gazelle.wstester.util.Xml;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Map;

@Name("mockMessageDisplay")
@Scope(ScopeType.PAGE)
public class MockMessageDisplay extends AbstractTransactionInstanceDisplay {

    private static final Logger LOG = LoggerFactory.getLogger(MockMessageDisplay.class);

    private MockValidationResult requestValidationResult;
    private MockValidationResult responseValidationResult;
    private MockValidation selectedMockValidation;

    public MockValidation getSelectedMockValidation() {
        return selectedMockValidation;
    }

    public MockValidationResult getRequestValidationResult() {
        return requestValidationResult;
    }

    public MockValidationResult getResponseValidationResult() {
        return responseValidationResult;
    }

    @Create
    public void init() {
        Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        MockValidationResultDaoImpl mockValidationResultDaoImpl = new MockValidationResultDaoImpl();
        selectedMockValidation = new MockValidation();
        if (urlParams != null && urlParams.size() > 0) {
            if (urlParams.containsKey("id")) {
                super.getTransactionInstanceFromUrl();
                if (urlParams.containsKey("oid")) {
                    String id = urlParams.get("id");
                    String validationOid = EVSClientResults.getResultOidFromUrl(urlParams);
                    selectedMockValidation.init(id, validationOid);
                }
                if (selectedInstance != null) {
                    requestValidationResult = mockValidationResultDaoImpl.getLastMockValidationResultByMessageInstance(selectedInstance.getRequest());
                    responseValidationResult = mockValidationResultDaoImpl.getLastMockValidationResultByMessageInstance(selectedInstance.getResponse());
                }
            }
        }

    }

    @Override
    public String permanentLinkToTestReport() {
        return null;
    }

    @Override
    public void modifyQuery(HQLQueryBuilder<TransactionInstance> queryBuilder, Map<String, Object> var2) {
        super.modifyQuery(queryBuilder, var2);
        TransactionInstanceQuery query = new TransactionInstanceQuery();
        queryBuilder.addRestriction(query.request().issuer().neqRestriction("GWT"));
    }

    @Override
    public String getPrettyFormattedResult(MessageInstance messageInstance) {
        return null;
    }

    @Override
    public void validateRequest() {
        if (selectedInstance != null && selectedInstance.getRequest() != null) {
            validateRequest(selectedInstance);
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.WARN, "No request content registered");
        }
    }

    @Override
    public void validateResponse() {
        if (selectedInstance != null && selectedInstance.getResponse() != null) {
            validateResponse(selectedInstance);
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.WARN, "No response content registered");
        }
    }

    @Override
    public void validateRequest(TransactionInstance transactionInstance) {
        selectedMockValidation.setSelectedMessageInstanceToValidate(transactionInstance.getRequest());
        selectedMockValidation.validateMessage();
     }

    @Override
    public void validateResponse(TransactionInstance transactionInstance) {
        selectedMockValidation.setSelectedMessageInstanceToValidate(transactionInstance.getResponse());
        selectedMockValidation.validateMessage();
    }

    @Override
    public void replayMessage() {

    }


    /**
     * Retrieve the content of the request MessageInstance rendered in HTML
     *
     * @param messageInstance Message instance which content has to be formatted and displayed
     *
     * @return String Message instance content formatted
     */
    public String getRequestContent(MessageInstance messageInstance) {
        if (messageInstance != null && messageInstance.getContent() != null) {
            try {
                return getHTMLRenderer(messageInstance, "Request");
            } catch (IOException e) {
                LOG.error("getRequestContent() : " + e.getMessage());
                FacesMessages.instance().add(StatusMessage.Severity.WARN,"Unable to render the request in HTML format");
                return new String(messageInstance.getContent());
            }
        } else {
            return null;
        }
    }

    /**
     * Retrieve the content of the response MessageInstance rendered in HTML
     *
     * @param messageInstance Message instance which content has to be formatted and displayed
     *
     * @return String Message instance content formatted
     */
    public String getResponseContent(MessageInstance messageInstance) {
        if (messageInstance != null && messageInstance.getContent() != null &&
                !(new String(messageInstance.getContent()).equals("<missing response>"))) {
            try {
                return getHTMLRenderer(messageInstance, "Response");
            } catch (IOException e) {
                LOG.error("getResponseContent() : " + e.getMessage());
                FacesMessages.instance().add(StatusMessage.Severity.WARN,"Unable to render the response in HTML format");
                return new String(messageInstance.getContent());
            }
        } else {
            return null;
        }
    }

    /**
     * Render from byte array to String (HTML) the content of a message instance using its type (request or response)
     *
     * @param messageInstance Message instance which content has to be formatted and displayed
     * @param type Type of the message to format
     *
     * @return String message instance content formatted
     */
    private String getHTMLRenderer(MessageInstance messageInstance, String type) throws IOException {
        if (type != null && type.equals("Request") || type.equals("Response")) {
            return XhtmlRendererFactory.getRenderer("xml").highlight(type, Xml.prettyFormat(new String(messageInstance
                    .getContent(), StandardCharsets.UTF_8)), "UTF-8", false).replaceAll("<h1>.*</h1>", "");
        } else {
            return null;
        }
    }

    /**
     * Download the request of the transaction instance
     *
     */
    public void downloadRequest() {
        final byte [] content = selectedInstance.getRequest().getContent();
        final String fileName = "transactionInstance" + selectedInstance.getId() + "_request.xml";
        DownloadFile.downloadMessage(content, fileName);
    }

    /**
     * Download the response of the transaction instance
     *
     */
    public void downloadResponse() {
        final byte [] content = selectedInstance.getResponse().getContent();
        final String fileName = "transactionInstance" + selectedInstance.getId() + "_response.xml";
        DownloadFile.downloadMessage(content, fileName);
    }

}
