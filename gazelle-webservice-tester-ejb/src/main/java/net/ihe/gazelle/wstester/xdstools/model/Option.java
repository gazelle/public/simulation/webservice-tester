package net.ihe.gazelle.wstester.xdstools.model;

import org.jboss.seam.Component;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "gwt_option")
@SequenceGenerator(name = "gwt_option_sequence", sequenceName = "gwt_option_id_seq", allocationSize = 1)
public class Option implements Serializable {

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gwt_option_sequence")
    private Integer id;

    @Column(name = "xdstools_name", nullable = false)
    private String xdstoolsName;

    @Column(name = "tm_name")
    private String tmName;

    @Column(name = "description")
    private String description;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getXdstoolsName() {
        return xdstoolsName;
    }

    public void setXdstoolsName(String xdstoolsName) {
        this.xdstoolsName = xdstoolsName;
    }

    public String getTmName() {
        return tmName;
    }

    public void setTmName(String tmName) {
        this.tmName = tmName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static List<Option> getAllOption() {
        final OptionQuery query = new OptionQuery();
        return query.getListNullIfEmpty();
    }

    public static String getOptionByTmName(String tmName) {
        final OptionQuery query = new OptionQuery();
        List<Option> options = query.getListNullIfEmpty();
        for (Option option : options) {
            if (option.getTmName() != null) {
                for (String optionString : option.getTmName().split(",")) {
                    if (optionString.trim().equals(tmName)) {
                        return option.getXdstoolsName();
                    }
                }
            }
        }
        return null;
    }

    public void save() {
        final EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
        entityManager.merge(this);
        entityManager.flush();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Option option = (Option) o;

        if (!xdstoolsName.equals(option.xdstoolsName)) {
            return false;
        }
        return tmName != null ? tmName.equals(option.tmName) : option.tmName == null;
    }

    @Override
    public int hashCode() {
        int result = xdstoolsName.hashCode();
        result = 31 * result + (tmName != null ? tmName.hashCode() : 0);
        return result;
    }
}
