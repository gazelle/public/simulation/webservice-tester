package net.ihe.gazelle.wstester.dao;

import com.eviware.soapui.model.testsuite.TestStepResult;
import net.ihe.gazelle.wstester.model.GwtTestStep;
import net.ihe.gazelle.wstester.model.GwtTestStepQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GwtTestStepDao {

    private static final Logger LOG = LoggerFactory.getLogger(GwtTestStepDao.class);

    public static GwtTestStep getGwtTestStep(String labelTestStep, String labelTestCase, String labelTestSuite, String project) {
        GwtTestStepQuery query = new GwtTestStepQuery();
        query.label().eq(labelTestStep);
        query.testCase().label().eq(labelTestCase);
        query.testCase().testSuite().label().eq(labelTestSuite);
        query.testCase().testSuite().gwtProject().soapuiProject().label().eq(project);
        return query.getUniqueResult();
    }

    public static GwtTestStep getGwtTestStep(TestStepResult result, String projectLabel) {
        return GwtTestStepDao.getGwtTestStep(removeDisabledInLabel(result.getTestStep().getLabel()), removeDisabledInLabel(result.getTestStep()
                .getTestCase().getLabel()), removeDisabledInLabel(result.getTestStep().getTestCase().getTestSuite().getLabel()), projectLabel);
    }

    private static String removeDisabledInLabel(String label) {
        if (label.contains(" (disabled)")) {
            label = label.replace(" (disabled)", "");
        }
        return label;
    }
}
