package net.ihe.gazelle.wstester.xdstools.action;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.wstester.xdstools.model.Option;
import net.ihe.gazelle.wstester.xdstools.model.OptionQuery;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

import javax.faces.context.FacesContext;
import java.util.List;
import java.util.Map;

@Name("optionManager")
@Scope(ScopeType.PAGE)
public class OptionManager {
    private FilterDataModel<Option> options;
    private Filter<Option> filter;
    boolean addOption = false;
    Option newOption = null;

    public FilterDataModel<Option> getOptions() {
        return new FilterDataModel<Option>(getFilter()) {
            @Override
            protected Object getId(Option option) {
                return option.getId();
            }
        };
    }

    public Filter<Option> getFilter() {
        if (filter == null) {
            Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
            this.filter = new Filter(this.getHqlCriterions(), params);
        }
        return this.filter;
    }

    private HQLCriterionsForFilter getHqlCriterions() {
        OptionQuery query = new OptionQuery();
        HQLCriterionsForFilter criterion = query.getHQLCriterionsForFilter();
        criterion.addPath("tmName", query.tmName());
        criterion.addPath("xdstoolsName", query.xdstoolsName());
        criterion.addPath("description", query.description());
        return criterion;
    }

    public boolean isAddOption() {
        return addOption;
    }

    public void setAddOption(boolean addOption) {
        this.addOption = addOption;
    }

    public Option getNewOption() {
        return newOption;
    }

    public void setNewOption(Option newOption) {
        this.newOption = newOption;
    }

    public void merge() {
        merge(newOption);
        cancel();
    }

    public void merge(Option optionToMerge) {
        List<Option> optionList = Option.getAllOption();
        if (optionList != null) {
            for (Option option : optionList) {
                if (optionToMerge.getXdstoolsName().equals(option.getXdstoolsName()) && optionToMerge.getId().equals(option.getId())) {
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR, "There is already an option with this OID");
                    return;
                }
            }
        }
        optionToMerge.save();
    }

    public void cancel() {
        newOption = null;
        addOption = false;
    }

    public void addNewOption() {
        newOption = new Option();
        addOption = true;
    }

    public void reset() {
        if (this.filter != null) {
            this.filter.clear();
        }
    }
}
