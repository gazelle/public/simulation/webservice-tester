package net.ihe.gazelle.wstester.action;


import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.wstester.dao.ExecutionDao;
import net.ihe.gazelle.wstester.model.Execution;
import net.ihe.gazelle.wstester.model.GwtProjectResult;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import java.util.Map;

@Name("messageValidation")
@Scope(ScopeType.PAGE)
public class GwtValidation {

    String id;

    Execution execution;

    GwtTestStepValidation gwtTestStepValidation;

    GwtProjectXValidation gwtProjectXValidation;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Execution getExecution() {
        return execution;
    }

    public void setExecution(Execution execution) {
        this.execution = execution;
    }

    public GwtTestStepValidation getGwtTestStepValidation() {
        return gwtTestStepValidation;
    }

    public void setGwtTestStepValidation(GwtTestStepValidation gwtTestStepValidation) {
        this.gwtTestStepValidation = gwtTestStepValidation;
    }

    public GwtProjectXValidation getGwtProjectXValidation() {
        return gwtProjectXValidation;
    }

    public void setGwtProjectXValidation(GwtProjectXValidation gwtProjectXValidation) {
        this.gwtProjectXValidation = gwtProjectXValidation;
    }


    @Create
    public void initializePage() {

        Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (urlParams != null && urlParams.size() > 0) {

            if (urlParams.containsKey("id")) {
                id = urlParams.get("id");

                if (id != null) {

                    if(id.contains("_")){
                        String[] parts = id.split("_");
                        id = parts[0];
                    }

                    execution = ExecutionDao.getExecutionById(Integer.decode(id));

                    //Validation process
                    if (urlParams.containsKey("logoid")) {
                        //XValidation
                        gwtProjectXValidation = new GwtProjectXValidation();
                        gwtProjectXValidation.init();
                    } else {
                        //Validation
                        gwtTestStepValidation = new GwtTestStepValidation();
                        gwtTestStepValidation.init();
                    }
                }
            }
        }
    }


}
