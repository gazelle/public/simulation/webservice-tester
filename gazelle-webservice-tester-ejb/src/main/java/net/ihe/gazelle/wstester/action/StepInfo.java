package net.ihe.gazelle.wstester.action;

public class StepInfo {

    private String transaction;
    private int testRoleResponderId;
    private int testRoleInitiatorId;
    private int participantRoleInTestId;
    private String responderSystemKeyword;
    private String initiatorSystemKeyword;
    private String endpoint;
    private String roleInTest;
    private String transactionUsage;

    public StepInfo(String transaction, int testRoleResponderId) {
        this.transaction = transaction;
        this.testRoleResponderId = testRoleResponderId;
    }

    public String getTransaction() {
        return transaction;
    }

    public void setTransaction(String transaction) {
        this.transaction = transaction;
    }

    public int getTestRoleResponderId() {
        return testRoleResponderId;
    }

    public void setTestRoleResponderId(int testRoleResponderId) {
        this.testRoleResponderId = testRoleResponderId;
    }

    public int getParticipantRoleInTestId() {
        return participantRoleInTestId;
    }

    public void setParticipantRoleInTestId(int participantRoleInTestId) {
        this.participantRoleInTestId = participantRoleInTestId;
    }

    public int getTestRoleInitiatorId() {
        return testRoleInitiatorId;
    }

    public void setTestRoleInitiatorId(int testRoleInitiatorId) {
        this.testRoleInitiatorId = testRoleInitiatorId;
    }

    public String getResponderSystemKeyword() {
        return responderSystemKeyword;
    }

    public void setResponderSystemKeyword(String responderSystemKeyword) {
        this.responderSystemKeyword = responderSystemKeyword;
    }

    public String getInitiatorSystemKeyword() {
        return initiatorSystemKeyword;
    }

    public void setInitiatorSystemKeyword(String initiatorSystemKeyword) {
        this.initiatorSystemKeyword = initiatorSystemKeyword;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getRoleInTest() {
        return roleInTest;
    }

    public void setRoleInTest(String roleInTest) {
        this.roleInTest = roleInTest;
    }

    public String getTransactionUsage() {
        return transactionUsage;
    }

    public void setTransactionUsage(String transactionUsage) {
        this.transactionUsage = transactionUsage;
    }
}
