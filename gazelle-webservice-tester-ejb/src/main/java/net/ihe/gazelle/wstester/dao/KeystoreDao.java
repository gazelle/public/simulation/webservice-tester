package net.ihe.gazelle.wstester.dao;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.wstester.model.Keystore;
import net.ihe.gazelle.wstester.model.KeystoreQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.util.List;

public class KeystoreDao {

    private static final Logger LOG = LoggerFactory.getLogger(KeystoreDao.class);

    public static Keystore merge(Keystore keystore) {
        final EntityManager em = EntityManagerService.provideEntityManager();
        Keystore keystore1 = em.merge(keystore);
        em.flush();
        return keystore1;
    }

    public static List<Keystore> getAllKeystores() {
        KeystoreQuery query = new KeystoreQuery();
        query.getList();
        return query.getListNullIfEmpty();
    }

    public static Keystore getKeystoreByLabel(String label) {
        KeystoreQuery query = new KeystoreQuery();
        query.label().eq(label);
        return query.getUniqueResult();
    }

    public static Keystore getDefaultKeystore() {
        KeystoreQuery query = new KeystoreQuery();
        query.defaultKeystore().eq(true);
        return query.getUniqueResult();
    }
}
