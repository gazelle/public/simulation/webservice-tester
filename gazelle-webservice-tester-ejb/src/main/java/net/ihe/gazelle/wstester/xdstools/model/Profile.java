package net.ihe.gazelle.wstester.xdstools.model;

import org.jboss.seam.Component;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "gwt_profile")
@SequenceGenerator(name = "gwt_profile_sequence", sequenceName = "gwt_profile_id_seq", allocationSize = 1)
public class Profile implements Serializable {

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gwt_profile_sequence")
    private Integer id;

    @Column(name = "xdstools_name", nullable = false)
    private String xdstoolsName;

    @Column(name = "tm_name")
    private String tmName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getXdstoolsName() {
        return xdstoolsName;
    }

    public void setXdstoolsName(String xdstoolsName) {
        this.xdstoolsName = xdstoolsName;
    }

    public String getTmName() {
        return tmName;
    }

    public void setTmName(String tmName) {
        this.tmName = tmName;
    }

    public static List<Profile> getAllOption() {
        final ProfileQuery query = new ProfileQuery();
        return query.getListNullIfEmpty();
    }

    public static String getProfileByTmName(String tmName) {
        final ProfileQuery query = new ProfileQuery();
        List<Profile> profiles = query.getListNullIfEmpty();
        for (Profile profile : profiles) {
            if (profile.getTmName() != null) {
                for (String profileString : profile.getTmName().split(",")) {
                    if (profileString.trim().equals(tmName)) {
                        return profile.getXdstoolsName();
                    }
                }
            }
        }
        return null;
    }

    public void save() {
        final EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
        entityManager.merge(this);
        entityManager.flush();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Profile profile = (Profile) o;

        if (!xdstoolsName.equals(profile.xdstoolsName)) {
            return false;
        }
        return tmName != null ? tmName.equals(profile.tmName) : profile.tmName == null;
    }

    @Override
    public int hashCode() {
        int result = xdstoolsName.hashCode();
        result = 31 * result + (tmName != null ? tmName.hashCode() : 0);
        return result;
    }
}
