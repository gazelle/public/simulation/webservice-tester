package net.ihe.gazelle.wstester.util;

import net.ihe.gazelle.common.tree.GazelleTreeNodeImpl;
import net.ihe.gazelle.wstester.model.CustomProperty;
import net.ihe.gazelle.wstester.model.GwtProject;
import net.ihe.gazelle.wstester.model.GwtTestCase;
import net.ihe.gazelle.wstester.model.GwtTestStep;
import net.ihe.gazelle.wstester.model.GwtTestSuite;
import net.ihe.gazelle.wstester.model.TestComponent;
import org.hibernate.Hibernate;

public class Tree {

    public static GazelleTreeNodeImpl<TestComponent> getComponentAsTree(TestComponent testComponent, boolean withTestStep, boolean initialize) {
        GazelleTreeNodeImpl<TestComponent> treeRoot = new GazelleTreeNodeImpl<>();
        int index = 0;
        if (testComponent instanceof GwtProject) {
            getProjectAsTree((GwtProject) testComponent, treeRoot, index, withTestStep, initialize);
        } else if (testComponent instanceof GwtTestSuite) {
            getTestSuiteAsTree((GwtTestSuite) testComponent, treeRoot, index, withTestStep, initialize);
        } else if (testComponent instanceof GwtTestCase) {
            getTestCaseAsTree((GwtTestCase) testComponent, treeRoot, index, withTestStep, initialize, false);
        }
        return treeRoot;
    }

    private static void getProjectAsTree(GwtProject gwtProject, GazelleTreeNodeImpl<TestComponent> parentNode, int index, boolean withTestStep, boolean initialize) {
        if (initialize) {
            Hibernate.initialize(gwtProject.getCustomProperties());
            for (CustomProperty customProperty : gwtProject.getCustomProperties()) {
                Hibernate.initialize(customProperty.getCustomPropertyUsedList());
            }
        }
        GazelleTreeNodeImpl<TestComponent> nodeProject = new GazelleTreeNodeImpl<>();
        gwtProject.setExpendedInTree(true);
        nodeProject.setData(gwtProject);
        parentNode.addChild(index, nodeProject);
        index++;
        for (GwtTestSuite testSuite : gwtProject.getTestSuites()) {
            getTestSuiteAsTree(testSuite, nodeProject, index, withTestStep, initialize);
            index++;
        }
    }

    private static void getTestSuiteAsTree(GwtTestSuite testSuite, GazelleTreeNodeImpl<TestComponent> parentNode, int index, boolean withTestStep, boolean initialize) {
        if (initialize) {
            Hibernate.initialize(testSuite.getCustomProperties());
            for (CustomProperty customProperty : testSuite.getCustomProperties()) {
                Hibernate.initialize(customProperty.getCustomPropertyUsedList());
            }
        }
        if (!testSuite.isDisable()) {
            GazelleTreeNodeImpl<TestComponent> nodeSuite = new GazelleTreeNodeImpl<>();
            testSuite.setExpendedInTree(true);
            nodeSuite.setData(testSuite);
            parentNode.addChild(index, nodeSuite);
            for (GwtTestCase testCase : testSuite.getTestCases()) {
                getTestCaseAsTree(testCase, nodeSuite, index, withTestStep, initialize, false);
                index++;
            }
        } else {
            for (GwtTestCase testCase : testSuite.getTestCases()) {
                getTestCaseAsTree(testCase, null, index, withTestStep, initialize, true);
                index++;
            }
        }
    }

    private static void getTestCaseAsTree(GwtTestCase testCase, GazelleTreeNodeImpl<TestComponent> parentNode, int index, boolean withTestStep, boolean initialize, boolean isTestSuiteDisabled) {
        if (initialize) {
            Hibernate.initialize(testCase.getCustomProperties());
            for (CustomProperty customProperty : testCase.getCustomProperties()) {
                Hibernate.initialize(customProperty.getCustomPropertyUsedList());
            }
        }
        if (!isTestSuiteDisabled) {
            if (!testCase.isDisable()) {
                GazelleTreeNodeImpl<TestComponent> nodeCase = new GazelleTreeNodeImpl<>();
                testCase.setExpendedInTree(true);
                nodeCase.setData(testCase);
                parentNode.addChild(index, nodeCase);
                if (withTestStep) {
                    for (GwtTestStep testStep : testCase.getTestSteps()) {
                        if (initialize) {
                            Hibernate.initialize(testStep.getCustomProperties());
                            for (CustomProperty customProperty : testStep.getCustomProperties()) {
                                Hibernate.initialize(customProperty.getCustomPropertyUsedList());
                            }
                        }
                        if (!testStep.isDisable()) {
                            GazelleTreeNodeImpl<TestComponent> nodeStep = new GazelleTreeNodeImpl<>();
                            testStep.setExpendedInTree(true);
                            nodeStep.setData(testStep);
                            nodeCase.addChild(index, nodeStep);
                            index++;
                        }
                    }
                }
            }
        }
    }

    public static GazelleTreeNodeImpl<TestComponent> getComponentWithParentsAsTree(TestComponent testComponent) {
        GazelleTreeNodeImpl<TestComponent> treeRoot = new GazelleTreeNodeImpl<>();
        int index = 0;
        testComponent = HibernateUtil.initializeAndUnproxy(testComponent);
        if (testComponent instanceof GwtProject) {
            getProjectAsTree((GwtProject) testComponent, treeRoot, index, false, false);
        } else if (testComponent instanceof GwtTestSuite) {
            GazelleTreeNodeImpl<TestComponent> nodeProject = new GazelleTreeNodeImpl<>();
            ((GwtTestSuite) testComponent).getProject().setExpendedInTree(true);
            nodeProject.setData(((GwtTestSuite) testComponent).getProject());
            treeRoot.addChild(index, nodeProject);
            index++;
            getTestSuiteAsTree((GwtTestSuite) testComponent, nodeProject, index, false, false);
        } else if (testComponent instanceof GwtTestCase) {
            GazelleTreeNodeImpl<TestComponent> nodeProject = new GazelleTreeNodeImpl<>();
            ((GwtTestCase) testComponent).getTestSuite().getProject().setExpendedInTree(true);
            nodeProject.setData(((GwtTestCase) testComponent).getTestSuite().getProject());
            treeRoot.addChild(index, nodeProject);
            index++;

            GazelleTreeNodeImpl<TestComponent> nodeSuite = new GazelleTreeNodeImpl<>();
            ((GwtTestCase) testComponent).getTestSuite().setExpendedInTree(true);
            nodeSuite.setData(((GwtTestCase) testComponent).getTestSuite());
            nodeProject.addChild(index, nodeSuite);
            index++;

            getTestCaseAsTree((GwtTestCase) testComponent, nodeSuite, index, false, false, false);
        }
        return treeRoot;
    }
}
