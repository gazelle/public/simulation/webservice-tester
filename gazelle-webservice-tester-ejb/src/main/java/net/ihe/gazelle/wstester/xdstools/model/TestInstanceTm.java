package net.ihe.gazelle.wstester.xdstools.model;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import org.jboss.seam.annotations.Name;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.List;

@Entity
@Name("TestInstanceTm")
@Table(name = "gwt_test_instance_tm")
@SequenceGenerator(name = "gwt_test_instance_tm_sequence", sequenceName = "gwt_test_instance_tm_id_seq", allocationSize = 1)
public class TestInstanceTm implements Serializable {

    @Id
    @GeneratedValue(generator = "gwt_test_instance_tm_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "test_instance_id", nullable = false)
    private int testInstanceId;

    @Column(name = "system_keyword", nullable = false)
    private String systemKeyword;

    @Column(name = "testing_session")
    private int testingSession;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "nonce")
    private Nonce nonce;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "calling_tool_id")
    private CallingTool callingTool;

    public TestInstanceTm(int testInstanceId, String systemKeyword, int testingSession, Nonce nonce, CallingTool callingTool) {
        this.testInstanceId = testInstanceId;
        this.systemKeyword = systemKeyword;
        this.testingSession = testingSession;
        this.nonce = nonce;
        this.callingTool = callingTool;
    }

    public TestInstanceTm() {
    }

    public static List<TestInstanceTm> getTestInstance(int testingSession, String systemKeyword, CallingTool callingTool) {
        TestInstanceTmQuery nonceQuery = new TestInstanceTmQuery();
        nonceQuery.testingSession().eq(testingSession);
        nonceQuery.systemKeyword().eq(systemKeyword);
        nonceQuery.callingTool().eq(callingTool);
        return nonceQuery.getListNullIfEmpty();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getTestInstanceId() {
        return testInstanceId;
    }

    public void setTestInstanceId(int testInstanceId) {
        this.testInstanceId = testInstanceId;
    }

    public String getSystemKeyword() {
        return systemKeyword;
    }

    public void setSystemKeyword(String systemKeyword) {
        this.systemKeyword = systemKeyword;
    }

    public int getTestingSession() {
        return testingSession;
    }

    public void setTestingSession(int testingSession) {
        this.testingSession = testingSession;
    }

    public Nonce getNonce() {
        return nonce;
    }

    public void setNonce(Nonce nonce) {
        this.nonce = nonce;
    }

    public CallingTool getCallingTool() {
        return callingTool;
    }

    public void setCallingTool(CallingTool callingTool) {
        this.callingTool = callingTool;
    }

    public TestInstanceTm merge() {
        final EntityManager em = EntityManagerService.provideEntityManager();
        TestInstanceTm testInstanceTm = em.merge(this);
        em.flush();
        return testInstanceTm;
    }
}
