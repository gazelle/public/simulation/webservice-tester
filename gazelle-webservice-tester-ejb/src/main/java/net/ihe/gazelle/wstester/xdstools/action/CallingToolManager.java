package net.ihe.gazelle.wstester.xdstools.action;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.wstester.xdstools.model.CallingTool;
import net.ihe.gazelle.wstester.xdstools.model.CallingToolQuery;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

import javax.faces.context.FacesContext;
import java.util.List;
import java.util.Map;

@Name("callingToolManager")
@Scope(ScopeType.PAGE)
public class CallingToolManager {
    private FilterDataModel<CallingTool> callingTools;
    private Filter<CallingTool> filter;
    boolean addCallingTool = false;
    CallingTool newCallingTool = null;

    public FilterDataModel<CallingTool> getCallingTools() {
        return new FilterDataModel<CallingTool>(getFilter()) {
            @Override
            protected Object getId(CallingTool callingTool) {
                return callingTool.getId();
            }
        };
    }

    public Filter<CallingTool> getFilter() {
        if (filter == null) {
            Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
            this.filter = new Filter(this.getHqlCriterions(), params);
        }
        return this.filter;
    }

    private HQLCriterionsForFilter getHqlCriterions() {
        CallingToolQuery query = new CallingToolQuery();
        HQLCriterionsForFilter criterion = query.getHQLCriterionsForFilter();
        criterion.addPath("label", query.label());
        criterion.addPath("oid", query.oid());
        criterion.addPath("url", query.url());
        return criterion;
    }

    public boolean isAddCallingTool() {
        return addCallingTool;
    }

    public void setAddCallingTool(boolean addCallingTool) {
        this.addCallingTool = addCallingTool;
    }

    public CallingTool getNewCallingTool() {
        return newCallingTool;
    }

    public void setNewCallingTool(CallingTool newCallingTool) {
        this.newCallingTool = newCallingTool;
    }

    public void merge() {
        merge(newCallingTool);
    }

    public void merge(CallingTool callingToolToMerge) {
        List<CallingTool> callingToolList = CallingTool.getAllTool();
        if (callingToolList != null) {
            for (CallingTool callingTool : callingToolList) {
                if (callingToolToMerge.getLabel().equals(callingTool.getLabel())) {
                    if (addCallingTool || (callingToolToMerge.getId() != null && !callingToolToMerge.getId().equals(callingTool.getId()))) {
                        FacesMessages.instance().add(StatusMessage.Severity.ERROR, "There is already a calling tool with this name");
                        return;
                    }
                } else if (callingToolToMerge.getOid().equals(callingTool.getOid())) {
                    if (addCallingTool || (callingToolToMerge.getId() != null && !callingToolToMerge.getId().equals(callingTool.getId()))) {
                        FacesMessages.instance().add(StatusMessage.Severity.ERROR, "There is already a calling tool with this OID");
                        return;
                    }
                } else if (callingToolToMerge.getUrl().equals(callingTool.getUrl())) {
                    if (addCallingTool || (callingToolToMerge.getId() != null && !callingToolToMerge.getId().equals(callingTool.getId()))) {
                        FacesMessages.instance().add(StatusMessage.Severity.ERROR, "There is already a calling tool with this URL");
                        return;
                    }
                }
            }
        }
        callingToolToMerge.save();
        cancel();
    }

    public void cancel() {
        newCallingTool = null;
        addCallingTool = false;
    }

    public void addNewCallingTool() {
        newCallingTool = new CallingTool();
        addCallingTool = true;
    }

    public void reset() {
        if (this.filter != null) {
            this.filter.clear();
        }
    }
}
