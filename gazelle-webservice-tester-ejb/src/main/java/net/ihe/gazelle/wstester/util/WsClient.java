package net.ihe.gazelle.wstester.util;

import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.ws.client.GazelleProxyWebServiceNameStub;
import net.ihe.gazelle.ws.client.GazelleTRMServiceClient;
import net.ihe.gazelle.ws.client.GazelleTRMServiceStub;
import net.ihe.gazelle.ws.client.SOAPExceptionException;
import org.apache.axis2.AxisFault;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;

public class WsClient {

    private static final Logger LOG = LoggerFactory.getLogger(WsClient.class);

    public static GazelleTRMServiceStub.TrmTestInstance getTestInstanceById(int id) {
        try {
            GazelleTRMServiceClient gazelleTRMServiceClient = new GazelleTRMServiceClient(Application.getTMDomain(ApplicationConfiguration
                    .getValueOfVariable("tm_url")) + "gazelle-tm-ejb/GazelleTRMService/gazelleTRM");
            return gazelleTRMServiceClient.getTestInstanceById(id);
        } catch (AxisFault axisFault) {
            axisFault.printStackTrace();
        } catch (RemoteException e) {
            LOG.error(e.getMessage());
        } catch (SOAPExceptionException e) {
            LOG.error(e.getMessage());
        }
        return null;
    }

    public static GazelleProxyWebServiceNameStub.ListConfigurationsBySystemBySessionByTypeAsCSVResponse
    listConfigurationsBySystemBySessionByTypeAsCSV(String configurationType, String systemKeyword, String testingSessionId) {
        try {
            GazelleProxyWebServiceNameStub stub = new GazelleProxyWebServiceNameStub(Application.getTMDomain(ApplicationConfiguration
                    .getValueOfVariable("tm_url")) + "gazelle-tm-ejb/GazelleProxyWebServiceName/GazelleProxyWebService");

            stub._getServiceClient().getOptions().setProperty(org.apache.axis2.Constants.Configuration.DISABLE_SOAP_ACTION, true);

            GazelleProxyWebServiceNameStub.ListConfigurationsBySystemBySessionByTypeAsCSVE listConfigurationsBySystemBySessionByTypeAsCSVE = new
                    GazelleProxyWebServiceNameStub.ListConfigurationsBySystemBySessionByTypeAsCSVE();
            GazelleProxyWebServiceNameStub.ListConfigurationsBySystemBySessionByTypeAsCSV listConfigurationsBySystemBySessionByTypeAsCSV = new
                    GazelleProxyWebServiceNameStub.ListConfigurationsBySystemBySessionByTypeAsCSV();
            listConfigurationsBySystemBySessionByTypeAsCSV.setConfigurationType(configurationType);
            listConfigurationsBySystemBySessionByTypeAsCSV.setSystemKeyword(systemKeyword);
            listConfigurationsBySystemBySessionByTypeAsCSV.setTestingSessionId(testingSessionId);
            listConfigurationsBySystemBySessionByTypeAsCSVE.setListConfigurationsBySystemBySessionByTypeAsCSV
                    (listConfigurationsBySystemBySessionByTypeAsCSV);
            return stub.listConfigurationsBySystemBySessionByTypeAsCSV(listConfigurationsBySystemBySessionByTypeAsCSVE)
                    .getListConfigurationsBySystemBySessionByTypeAsCSVResponse();
        } catch (RemoteException e) {
            LOG.error(e.getMessage());
        } catch (SOAPExceptionException e) {
            LOG.error(e.getMessage());
        }
        return null;
    }

}
