package net.ihe.gazelle.wstester.action;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.wstester.dao.KeystoreDao;
import net.ihe.gazelle.wstester.model.Keystore;
import net.ihe.gazelle.wstester.model.KeystoreQuery;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Name("keystoreManager")
@Scope(ScopeType.PAGE)
public class KeystoreManager {
    private static final Logger LOG = LoggerFactory.getLogger(KeystoreManager.class);
    boolean addKeystore = false;
    Keystore newKeystore = null;
    Keystore keystoreToDelete = null;
    private FilterDataModel<Keystore> keystores;
    private Filter<Keystore> filter;

    public FilterDataModel<Keystore> getKeystores() {
        return new FilterDataModel<Keystore>(getFilter()) {
            @Override
            protected Object getId(Keystore keystore) {
                return keystore.getId();
            }
        };
    }

    public Filter<Keystore> getFilter() {
        if (filter == null) {
            Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
            this.filter = new Filter(this.getHqlCriterions(), params);
        }
        return this.filter;
    }

    private HQLCriterionsForFilter getHqlCriterions() {
        KeystoreQuery query = new KeystoreQuery();
        HQLCriterionsForFilter criterion = query.getHQLCriterionsForFilter();
        criterion.addPath("label", query.label());
        criterion.addPath("path", query.path());
        criterion.addPath("password", query.password());
        return criterion;
    }

    public boolean isAddKeystore() {
        return addKeystore;
    }

    public void setAddKeystore(boolean addKeystore) {
        this.addKeystore = addKeystore;
    }

    public Keystore getNewKeystore() {
        return newKeystore;
    }

    public void setNewKeystore(Keystore newKeystore) {
        this.newKeystore = newKeystore;
    }

    public void merge() {
        merge(newKeystore);
        cancel();
    }

    public void merge(Keystore keystoreToMerge) {
        List<Keystore> keystoreList = KeystoreDao.getAllKeystores();
        if (keystoreList != null) {
            for (Keystore callingTool : keystoreList) {
                if (keystoreToMerge.getLabel().equals(callingTool.getLabel()) && keystoreToMerge.getId().equals(callingTool.getId())) {
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR, "There is already a keystore with this name");
                    return;
                }
            }
        }
        keystoreToMerge.save();
    }

    public void uploadEventListener(final FileUploadEvent event) {
        FileOutputStream fos = null;
        try {
            final UploadedFile item = event.getUploadedFile();
            if (PreferenceService.getString("keystore_directory") != null) {
                newKeystore = new Keystore(item.getName(), PreferenceService.getString("keystore_directory") + UUID.randomUUID() + item.getName());
                newKeystore.setDefaultKeystore(false);
                File uploadedFile = new File(newKeystore.getPath());
                uploadedFile.getParentFile().mkdirs();
                if (uploadedFile.getParentFile().exists()) {
                    if (uploadedFile.createNewFile()) {
                        if (item.getData() != null && item.getData().length > 0) {
                            fos = new FileOutputStream(uploadedFile);
                            fos.write(item.getData());
                        } else {
                            newKeystore.setPath(null);
                        }
                    } else {
                        LOG.error("uploadEventListener() : file " + newKeystore.getPath() + " couldn't be created");
                    }
                } else {
                    LOG.error("uploadEventListener() : directory " + newKeystore.getPath() + " couldn't be created");
                }
            } else {
                LOG.error("The app preference project_directory is not set");
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The property project_directory is not set in the application " +
                        "configuration");
            }
        } catch (final IOException e) {
            LOG.error(e.getMessage());
        } catch (Exception e) {
            LOG.error(e.getMessage());
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (final IOException e) {
                    LOG.error("not able to close the FOS", e);
                }
            }
        }
    }

    public void setDefault(Keystore keystoreToDefault) {
        List<Keystore> keystoreList = KeystoreDao.getAllKeystores();
        for (Keystore keystore : keystoreList) {
            if (keystore.equals(keystoreToDefault)) {
                keystore.setDefaultKeystore(true);
            } else {
                keystore.setDefaultKeystore(false);
            }
        }
    }

    public Keystore getKeystoreToDelete() {
        return keystoreToDelete;
    }

    public void setKeystoreToDelete(Keystore keystoreToDelete) {
        this.keystoreToDelete = keystoreToDelete;
    }

    public void delete() {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        entityManager.remove(entityManager.contains(keystoreToDelete) ? keystoreToDelete : entityManager.merge(keystoreToDelete));
        entityManager.flush();
    }

    public void cancel() {
        newKeystore = null;
        addKeystore = false;
    }

    public void addNewKeystore() {
        newKeystore = new Keystore();
        addKeystore = true;
    }

    public void setMessageInvalidFileType() {
        FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Invalid file type. Expected file type is jks");
    }

    public void reset() {
        if (this.filter != null) {
            this.filter.clear();
        }
    }
}
