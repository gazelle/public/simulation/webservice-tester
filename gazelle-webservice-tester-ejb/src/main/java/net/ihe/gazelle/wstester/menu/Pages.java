package net.ihe.gazelle.wstester.menu;

import net.ihe.gazelle.common.pages.Authorization;
import net.ihe.gazelle.common.pages.Page;

public enum Pages implements Page {

    HOME("/home.seam", null, "Gazelle Webservice Tester", Authorizations.ALL),
    PROJECT_LIST("/project/projectList.seam", null, "Project List", Authorizations.ALL),
    PROJECT_DETAIL("/project/projectDetail.seam", null, "SoapUI project detail ", Authorizations.ALL),
    RUN_INSTANCE("/runInstance.seam", null, "Run", Authorizations.LOGGED),
    EXECUTION_LIST("/executionList.seam", null, "Execution List", Authorizations.LOGGED),
    EXECUTION_RESULT("/executionResult.seam", null, "Execution result", Authorizations.LOGGED),
    UPLOAD_PROJECT("/uploadProject.seam", null, "Upload a SoapUI project", Authorizations.LOGGED),
    LOGIN_REDIRECT("/login_redirect.seam", null, "User Not Logged In", Authorizations.ALL),
    CONFIGURATION("/administration/configure.seam", null, "Admin Configuration", Authorizations.ADMIN),
    CALLING_TOOL("/administration/callingTools.seam", null, "Calling tools", Authorizations.ADMIN),
    ERROR("/error.seam", null, "Error", Authorizations.ALL),
    ERROR_EXPIRED("/errorExpired.seam", null, "Error Expired", Authorizations.ALL),
    KEYSTORE("/administration/keystore.seam", null, "Keystore Manager", Authorizations.ADMIN),
    CONFORMANCE_TOOL("", null, "Conformance tool", Authorizations.ADMIN),
    OPTION("/administration/conformanceTool/option.seam", null, "Manage option", Authorizations.ADMIN),
    ACTOR_TYPE("/administration/conformanceTool/actorType.seam", null, "Manage actor type", Authorizations.ADMIN),
    PROFILE("/administration/conformanceTool/profile.seam", null, "Manage profile", Authorizations.ADMIN),
    MOCK_BROWSER("/mock/mockMessageBrowser.seam", null, "Mock Browser", Authorizations.LOGGED),
    MOCK_DISPLAY("/mock/mockMessageDisplay.seam", null, "Mock Message", Authorizations.LOGGED);



    private String link;
    private String icon;
    private String label;
    private Authorization[] authorizations;

    Pages(String link, String icon, String label, Authorization... authorizations) {
        this.link = link;
        this.icon = icon;
        this.label = label;
        this.authorizations = authorizations;
    }

    @Override
    public String getId() {
        return name();
    }

    @Override
    public String getLabel() {
        return label;
    }

    /**
     * with .xhtml extension
     *
     * @return
     */
    @Override
    public String getLink() {
        return link.replace(".seam", ".xhtml");
    }

    @Override
    public String getIcon() {
        return icon;
    }

    @Override
    public Authorization[] getAuthorizations() {
        return authorizations.clone();
    }

    @Override
    public String getMenuLink() {
        return link;
    }
}

