package net.ihe.gazelle.wstester.util;

import org.apache.commons.lang.ArrayUtils;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class DownloadFile {

    private static final Logger LOG = LoggerFactory.getLogger(DownloadFile.class);

    public static void downloadMessage(String content, String fileName) {
        try {
            HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext()
                    .getResponse();
            byte[] bytes = ArrayUtils.clone(content.getBytes(StandardCharsets.UTF_8));
            ServletOutputStream servletOutputStream = response.getOutputStream();
            response.setContentType("text/xml");
            response.setContentLength(bytes.length);
            response.setHeader("Content-Disposition", "attachment;filename=\"" + fileName + "\"");
            servletOutputStream.write(bytes);
            servletOutputStream.flush();
            servletOutputStream.close();
            FacesContext.getCurrentInstance().responseComplete();
        } catch (IOException e) {
            String msg = "Failed to download file";
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, msg, e);
            LOG.error(msg, e);
        }
    }

    public static void downloadMessage(byte[] content, String fileName) {
        try {
            HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext()
                    .getResponse();
            ServletOutputStream servletOutputStream = response.getOutputStream();
            response.setContentType("text/xml");
            response.setContentLength(content.length);
            response.setHeader("Content-Disposition", "attachment;filename=\"" + fileName + "\"");
            servletOutputStream.write(content);
            servletOutputStream.flush();
            servletOutputStream.close();
            FacesContext.getCurrentInstance().responseComplete();
        } catch (IOException e) {
            String msg = "Failed to download file";
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, msg, e);
            LOG.error(msg, e);
        }
    }
}
