package net.ihe.gazelle.wstester.model;

import net.ihe.gazelle.evsclient.connector.model.EVSClientCrossValidatedObject;
import net.ihe.gazelle.evsclient.connector.model.EVSClientValidatedObject;
import net.ihe.gazelle.evsclient.connector.utils.PartSourceUtils;
import org.apache.commons.httpclient.methods.multipart.PartSource;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "x_validator_input_info")
@SequenceGenerator(name = "x_validator_input_info_sequence", sequenceName = "x_validator_input_info_id_seq", allocationSize = 1)
public class XValidatorInputInfo implements EVSClientCrossValidatedObject, Serializable {

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "x_validator_input_info_sequence")
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "gwt_project_result_id")
    private GwtProjectResult gwtProjectResult;

    @Column(name = "input_name")
    private String inputName;

    @Column(name = "message_content")
    private byte[] messageContent;

    @Column(name = "input_type")
    String inputType;

    ///////////////////////////////////////////////////////////////////////////////
    ////////////////////    Getters and setters     /////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public GwtProjectResult getGwtProjectResult() {
        return gwtProjectResult;
    }

    public void setGwtProjectResult(GwtProjectResult gwtProjectResult) {
        this.gwtProjectResult = gwtProjectResult;
    }

    public String getInputName() {
        return inputName;
    }

    public void setInputName(String inputName) {
        this.inputName = inputName;
    }

    @Override
    public byte[] getMessageContent() {
        if (messageContent != null) {
            return messageContent.clone();
        } else {
            return null;
        }
    }

    public void setMessageContent(byte[] messageContent) {
        this.messageContent = messageContent;
    }

    @Override
    public String getInputType() {
        return inputType;
    }

    public void setInputType(String inputType) {
        this.inputType = inputType;
    }

    @Override
    public String getType() {
        return "xml";
    }

    public XValidatorInputInfo() {super();}

    public XValidatorInputInfo(GwtProjectResult projectResult, String inputName, byte[] messageContent, String inputType) {
        this.gwtProjectResult = projectResult;
        this.inputName = inputName;
        this.messageContent = messageContent;
        this.inputType = inputType;
    }

}

