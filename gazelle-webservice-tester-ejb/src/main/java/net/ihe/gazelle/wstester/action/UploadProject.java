package net.ihe.gazelle.wstester.action;

import com.eviware.soapui.impl.wsdl.WsdlProject;
import com.eviware.soapui.model.testsuite.TestCase;
import com.eviware.soapui.model.testsuite.TestProperty;
import com.eviware.soapui.model.testsuite.TestStep;
import com.eviware.soapui.model.testsuite.TestSuite;
import net.ihe.gazelle.common.tree.GazelleTreeNodeImpl;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.wstester.dao.KeystoreDao;
import net.ihe.gazelle.wstester.dao.SoapuiProjectDao;
import net.ihe.gazelle.wstester.model.CustomProperty;
import net.ihe.gazelle.wstester.model.GwtProject;
import net.ihe.gazelle.wstester.model.GwtTestCase;
import net.ihe.gazelle.wstester.model.GwtTestStep;
import net.ihe.gazelle.wstester.model.GwtTestSuite;
import net.ihe.gazelle.wstester.model.Keystore;
import net.ihe.gazelle.wstester.model.SoapuiProject;
import net.ihe.gazelle.wstester.model.TestComponent;
import net.ihe.gazelle.wstester.util.Application;
import net.ihe.gazelle.wstester.util.Tree;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.event.ValueChangeEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Name("uploadProject")
@Scope(ScopeType.PAGE)
public class UploadProject implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(UploadProject.class);
    private File uploadedFile;
    private SoapuiProject soapuiProject;
    private List<CustomProperty> customPropertyList;
    private GazelleTreeNodeImpl<TestComponent> tree;
    private boolean checkAllProperties;

    public SoapuiProject getSoapuiProject() {
        return soapuiProject;
    }

    public void setSoapuiProject(SoapuiProject soapuiProject) {
        this.soapuiProject = soapuiProject;
    }

    public File getUploadedFile() {
        return uploadedFile;
    }

    public void setUploadedFile(File uploadedFile) {
        this.uploadedFile = uploadedFile;
    }

    public List<CustomProperty> getCustomPropertyList() {
        return customPropertyList;
    }

    public void setCustomPropertyList(List<CustomProperty> customPropertyList) {
        this.customPropertyList = customPropertyList;
        boolean allToModify = true;
        for (CustomProperty customProperty : customPropertyList) {
            if (!customProperty.isToModify()) {
                allToModify = false;
            }
        }
        checkAllProperties = allToModify;
    }

    public boolean isCheckAllProperties() {
        return checkAllProperties;
    }

    public void setCheckAllProperties(boolean checkAllProperties) {
        this.checkAllProperties = checkAllProperties;
    }

    public GazelleTreeNodeImpl<TestComponent> getTree() {
        return tree;
    }

    public void setTree(GazelleTreeNodeImpl<TestComponent> tree) {
        this.tree = tree;
    }

    public void uploadEventListener(final FileUploadEvent event) {
        this.resetSimple();
        FileOutputStream fos = null;
        try {
            final UploadedFile item = event.getUploadedFile();
            if (PreferenceService.getString("project_directory") != null) {
                soapuiProject = new SoapuiProject(item.getName(), "/tmp/" + UUID.randomUUID() + ".xml", 1, true, Application.getUser(), new Date(),
                        null);
                uploadedFile = new File(soapuiProject.getXmlFilePath());
                uploadedFile.getParentFile().mkdirs();
                if (uploadedFile.getParentFile().exists()) {
                    if (this.uploadedFile.createNewFile()) {
                        if (item.getData() != null && item.getData().length > 0) {
                            fos = new FileOutputStream(uploadedFile);
                            fos.write(item.getData());
                        } else {
                            soapuiProject.setXmlFilePath(null);
                        }
                        WsdlProject wsdlProject = createWsdlProject();
                        if (wsdlProject != null) {
                            soapuiProject.setLabel(wsdlProject.getName());
                        } else {
                            FacesMessages.instance().add(StatusMessage.Severity.WARN, "Could not modify the label");
                        }
                        updateTestComponents();
                        tree = Tree.getComponentAsTree(soapuiProject.getGwtProject(), false, false);
                    } else {
                        LOGGER.error("uploadEventListener() : file " + soapuiProject.getXmlFilePath() + " couldn't be created");
                    }
                } else {
                    LOGGER.error("uploadEventListener() : directory " + soapuiProject.getXmlFilePath() + " couldn't be created");
                }
            } else {
                UploadProject.LOGGER.error("The app preference project_directory is not set");
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The property project_directory is not set in the application " +
                        "configuration");
            }
        } catch (final IOException e) {
            UploadProject.LOGGER.error(e.getMessage());
        } catch (Exception e) {
            UploadProject.LOGGER.error(e.getMessage());
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (final IOException e) {
                    UploadProject.LOGGER.error("not able to close the FOS", e);
                }
            }
        }
    }

    private WsdlProject createWsdlProject() {
        WsdlProject wsdlProject = null;
        try {
            wsdlProject = new WsdlProject(soapuiProject.getXmlFilePath());
        } catch (Exception e) {
            UploadProject.LOGGER.error(e.getMessage());
        }
        return wsdlProject;
    }

    private List<CustomProperty> updateCustomProperties(Map<String, TestProperty> testProperties, TestComponent parent) {
        List<CustomProperty> customPropertyList = new ArrayList<>();
        for (Map.Entry entry : testProperties.entrySet()) {
            CustomProperty customProperty = new CustomProperty();
            customProperty.setLabel(entry.getKey().toString());
            customProperty.setToModify(false);
            customProperty.setDefaultValue(((TestProperty) entry.getValue()).getValue());
            customProperty.setTestComponent(parent);
            customPropertyList.add(customProperty);
        }
        return customPropertyList;
    }

    private void updateTestComponents() {
        WsdlProject project = this.createWsdlProject();
        if (project != null) {
            Map<String, TestProperty> testProperties = project.getProperties();
            project.getName();
            GwtProject gwtProject = new GwtProject();
            gwtProject.setSoapuiProject(soapuiProject);
            gwtProject.setCustomProperties(updateCustomProperties(testProperties, gwtProject));
            gwtProject.setLabel(project.getName());
            List<TestSuite> testSuites = project.getTestSuiteList();
            List<GwtTestSuite> gwtTestSuites = new ArrayList<>();
            for (TestSuite suite : testSuites) {

                suite.getProject().getName();
                GwtTestSuite gwtTestSuite = new GwtTestSuite();
                gwtTestSuite.setLabel(suite.getName());
                gwtTestSuite.setProject(gwtProject);
                gwtTestSuite.setDisable(suite.isDisabled());

                testProperties = suite.getProperties();
                gwtTestSuite.setCustomProperties(updateCustomProperties(testProperties, gwtTestSuite));

                List<TestCase> testCases = suite.getTestCaseList();
                List<GwtTestCase> gwtTestCases = new ArrayList<>();
                for (TestCase testCase : testCases) {
                    GwtTestCase gwtTestCase = new GwtTestCase();
                    gwtTestCase.setLabel(testCase.getName());
                    gwtTestCase.setTestSuite(gwtTestSuite);
                    gwtTestCase.setDisable(testCase.isDisabled());

                    testProperties = testCase.getProperties();
                    gwtTestCase.setCustomProperties(updateCustomProperties(testProperties, gwtTestCase));

                    List<TestStep> testSteps = testCase.getTestStepList();
                    List<GwtTestStep> gwtTestSteps = new ArrayList<>();
                    for (TestStep testStep : testSteps) {
                        GwtTestStep gwtTestStep = new GwtTestStep();
                        String testStepName = testStep.getName();
                        gwtTestStep.setLabel(testStepName);
                        gwtTestStep.setTestCase(gwtTestCase);
                        gwtTestStep.setDisable(testStep.isDisabled());
                        gwtTestSteps.add(gwtTestStep);

                    }
                    gwtTestCase.setTestSteps(gwtTestSteps);
                    gwtTestCases.add(gwtTestCase);
                }

                gwtTestSuite.setTestCases(gwtTestCases);
                gwtTestSuites.add(gwtTestSuite);
            }

            gwtProject.setTestSuites(gwtTestSuites);
            soapuiProject.setGwtProject(gwtProject);
        } else {
            UploadProject.LOGGER.error("Could not create Wsdl Project");
        }
    }

    private void moveTmpFile() {
        soapuiProject = SoapuiProjectDao.mergeSoapuiProject(soapuiProject);
        String target = PreferenceService.getString("project_directory") + soapuiProject.getId() + soapuiProject.getLabel() + ".xml";
        try {
            Files.move(Paths.get(soapuiProject.getXmlFilePath()), Paths.get(target));
            soapuiProject.setXmlFilePath(target);
        } catch (IOException e) {
            UploadProject.LOGGER.error(e.getMessage());
        }
        soapuiProject = SoapuiProjectDao.mergeSoapuiProject(soapuiProject);
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "The soapui project has been uploaded");
    }

    private boolean checkLabelInDb() {
        if (SoapuiProjectDao.getSoapuiProjectByLabel(this.soapuiProject.getLabel()) != null) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "There is already a SoapUI project with the same label : " + soapuiProject
                    .getLabel());
            return false;
        }
        return true;
    }

    public void resetSimple() {
        this.soapuiProject = null;
        this.uploadedFile = null;
    }

    public List<Keystore> getAllKeystore() {
        return KeystoreDao.getAllKeystores();
    }

    public void setMessageInvalidFileType() {
        FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Invalid file type. Expected file type is xml");
    }

    public void checkAll(ValueChangeEvent event) {
        changeAllPropertiesToModify((boolean) event.getNewValue());
    }

    public void changeAllPropertiesToModify(boolean toModify) {
        for (CustomProperty customProperty : customPropertyList) {
            customProperty.setToModify(toModify);
        }
    }

    public String redirectToDetail() {
        if (checkLabelInDb()) {
            this.moveTmpFile();
            return "/project/projectDetail.seam?id=" + soapuiProject.getId();
        }
        return "";
    }
}
