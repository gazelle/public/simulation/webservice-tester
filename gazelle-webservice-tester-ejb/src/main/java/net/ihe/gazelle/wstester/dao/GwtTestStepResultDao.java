package net.ihe.gazelle.wstester.dao;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;

public class GwtTestStepResultDao {

    private static final Logger LOG = LoggerFactory.getLogger(GwtTestStepResultDao.class);

    public static GwtTestStepResultDao mergeGwtTestStepResult(GwtTestStepResultDao gwtTestStepResult) {
        if (gwtTestStepResult == null) {
            GwtTestStepResultDao.LOG.error("The test step result is null");
            return null;
        } else {
            final EntityManager em = EntityManagerService.provideEntityManager();
            GwtTestStepResultDao newGwtTestStepResult = em.merge(gwtTestStepResult);
            em.flush();
            return newGwtTestStepResult;
        }
    }
}
