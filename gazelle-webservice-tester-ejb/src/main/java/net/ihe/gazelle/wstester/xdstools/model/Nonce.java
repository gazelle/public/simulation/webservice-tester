package net.ihe.gazelle.wstester.xdstools.model;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import org.jboss.seam.annotations.Name;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Name("Nonce")
@Table(name = "gwt_nonce")
@SequenceGenerator(name = "gwt_nonce_sequence", sequenceName = "gwt_nonce_id_seq", allocationSize = 1)
public class Nonce implements Serializable {

    @Id
    @GeneratedValue(generator = "gwt_nonce_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "nonce")
    private String nonce;

    @Column(name = "url")
    @Pattern(regexp = "(^$)|(?i)(^http://.+$|^https://.+$)", message = "This attribute must be formatted as a valid URL")
    private String url;

    @OneToMany(
            targetEntity = TestInstanceTm.class,
            mappedBy = "nonce",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<TestInstanceTm> testInstanceTmList;


    public Nonce(String nonce, String url) {
        this.nonce = nonce;
        this.url = url;
    }

    public Nonce() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNonce() {
        return nonce;
    }

    public void setNonce(String nonce) {
        this.nonce = nonce;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<TestInstanceTm> getTestInstanceTmList() {
        return testInstanceTmList;
    }

    public void setTestInstanceTmList(List<TestInstanceTm> testInstanceTmList) {
        this.testInstanceTmList = testInstanceTmList;
    }

    public Nonce merge() {
        final EntityManager em = EntityManagerService.provideEntityManager();
        Nonce nonce = em.merge(this);
        em.flush();
        return nonce;
    }

    public void addTestInstanceTm(TestInstanceTm testInstanceTm) {
        if (testInstanceTmList == null) {
            testInstanceTmList = new ArrayList<>();
        }
        testInstanceTmList.add(testInstanceTm);
    }
}
