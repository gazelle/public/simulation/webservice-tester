package net.ihe.gazelle.wstester.menu;

import net.ihe.gazelle.common.pages.Authorization;
import org.jboss.seam.security.Identity;

public enum Authorizations implements Authorization {

    ALL,
    LOGGED,
    ADMIN,
    MONITOR;

    @Override
    public boolean isGranted(Object... context) {
        switch (this) {
            case ALL:
                return true;
            case LOGGED:
                return Identity.instance().isLoggedIn();
            case ADMIN:
                return Identity.instance().hasRole("admin_role");
            case MONITOR:
                return Identity.instance().hasRole("monitor_role");
            default:
                return false;
        }
    }
}
