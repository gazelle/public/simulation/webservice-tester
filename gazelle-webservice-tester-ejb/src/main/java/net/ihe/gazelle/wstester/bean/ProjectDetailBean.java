package net.ihe.gazelle.wstester.bean;

import net.ihe.gazelle.ssov7.gum.client.application.UserAttributeCommon;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.wstester.action.ProjectDetail;
import net.ihe.gazelle.wstester.dao.KeystoreDao;
import net.ihe.gazelle.wstester.dao.SoapuiProjectDao;
import net.ihe.gazelle.wstester.model.CustomProperty;
import net.ihe.gazelle.wstester.model.Keystore;
import net.ihe.gazelle.wstester.util.Tree;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.security.Identity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import java.util.List;
import java.util.Map;

@Name("projectDetail")
@Scope(ScopeType.PAGE)
public class ProjectDetailBean implements UserAttributeCommon {

    private static final Logger LOG = LoggerFactory.getLogger(ProjectDetailBean.class);

    private boolean editProject;
    private ProjectDetail projectDetail;
    private boolean checkAllProperties;

    @In(value="gumUserService")
    private UserService userService;

    @Create
    public void init() {
        editProject = false;
        Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String projectIdString = urlParams.get("id");
        projectDetail = new ProjectDetail();
        projectDetail.setSoapuiProject(SoapuiProjectDao.getSoapuiProjectById(Integer.parseInt(projectIdString)));
        projectDetail.setTree(Tree.getComponentAsTree(projectDetail.getSoapuiProject().getGwtProject(), true, false));
        String edit = urlParams.get("edit");
        if (edit != null && edit.equals("true") && Identity.instance().hasRole("admin_role")) {
            editProject = true;
        }
    }

    public boolean isEditProject() {
        return editProject;
    }

    public void setEditProject(boolean editProject) {
        this.editProject = editProject;
    }

    public ProjectDetail getProjectDetail() {
        return projectDetail;
    }

    public void setProjectDetail(ProjectDetail projectDetail) {
        this.projectDetail = projectDetail;
    }

    public boolean isCheckAllProperties() {
        return checkAllProperties;
    }

    public void setCheckAllProperties(boolean checkAllProperties) {
        this.checkAllProperties = checkAllProperties;
    }

    public void checkAll(ValueChangeEvent event) {
        changeAllPropertiesToModify((boolean) event.getNewValue());
    }

    public void changeAllPropertiesToModify(boolean toModify) {
        for (CustomProperty customProperty : projectDetail.getCustomPropertyList()) {
            customProperty.setToModify(toModify);
        }
    }

    public void setCustomPropertyList(List<CustomProperty> customPropertyList) {
        projectDetail.setCustomPropertyList(customPropertyList);
        boolean allToModify = true;
        for (CustomProperty customProperty : customPropertyList) {
            if (customProperty.isToModify() == false) {
                allToModify = false;
            }
        }
        checkAllProperties = allToModify;
    }

    public void saveEditProject() {
        projectDetail.saveEditProject();
        editProject = false;
    }

    public List<Keystore> getAllKeystores() {
        return KeystoreDao.getAllKeystores();
    }


    public String cancel() {
        return "/project/projectDetail.seam?id=" + projectDetail.getSoapuiProject().getId();
    }


    /**
     * Called in projectDetail.xhtml
     */
    @Override
    public String getUserName(String userId) {
        return userService.getUserDisplayNameWithoutException(userId);
    }
}
