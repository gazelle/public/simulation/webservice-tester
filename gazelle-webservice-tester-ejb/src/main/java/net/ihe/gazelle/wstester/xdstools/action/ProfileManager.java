package net.ihe.gazelle.wstester.xdstools.action;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.wstester.xdstools.model.Option;
import net.ihe.gazelle.wstester.xdstools.model.Profile;
import net.ihe.gazelle.wstester.xdstools.model.ProfileQuery;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

import javax.faces.context.FacesContext;
import java.util.List;
import java.util.Map;

@Name("profileManager")
@Scope(ScopeType.PAGE)
public class ProfileManager {
    private FilterDataModel<Profile> profiles;
    private Filter<Profile> filter;
    boolean addProfile = false;
    Profile newProfile = null;

    public FilterDataModel<Profile> getProfiles() {
        return new FilterDataModel<Profile>(getFilter()) {
            @Override
            protected Object getId(Profile profile) {
                return profile.getId();
            }
        };
    }

    public Filter<Profile> getFilter() {
        if (filter == null) {
            Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
            this.filter = new Filter(this.getHqlCriterions(), params);
        }
        return this.filter;
    }

    private HQLCriterionsForFilter getHqlCriterions() {
        ProfileQuery query = new ProfileQuery();
        HQLCriterionsForFilter criterion = query.getHQLCriterionsForFilter();
        criterion.addPath("tmName", query.tmName());
        criterion.addPath("xdstoolsName", query.xdstoolsName());
        return criterion;
    }

    public boolean isAddProfile() {
        return addProfile;
    }

    public void setAddProfile(boolean addProfile) {
        this.addProfile = addProfile;
    }

    public Profile getNewProfile() {
        return newProfile;
    }

    public void setNewProfile(Profile newProfile) {
        this.newProfile = newProfile;
    }

    public void merge() {
        merge(newProfile);
        cancel();
    }

    public void merge(Profile profileToMerge) {
        List<Option> optionList = Option.getAllOption();
        if (optionList != null) {
            for (Option option : optionList) {
                if (profileToMerge.getXdstoolsName().equals(option.getXdstoolsName()) && profileToMerge.getId().equals(option.getId())) {
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR, "There is already an option with this OID");
                    return;
                }
            }
        }
        profileToMerge.save();
    }

    public void cancel() {
        newProfile = null;
        addProfile = false;
    }

    public void addNewProfile() {
        newProfile = new Profile();
        addProfile = true;
    }

    public void reset() {
        if (this.filter != null) {
            this.filter.clear();
        }
    }
}
