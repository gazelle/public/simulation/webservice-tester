--
-- PostgreSQL database dump
--

-- Dumped from database version 14.6 (Debian 14.6-1.pgdg110+1)
-- Dumped by pg_dump version 14.10 (Ubuntu 14.10-0ubuntu0.22.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: update_soapui_project(integer); Type: FUNCTION; Schema: public; Owner: gazelle
--

CREATE FUNCTION public.update_soapui_project(project_id_var integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $$BEGIN
  IF (SELECT gwt_project FROM gwt_soapui_project where id = project_id_var) IS NULL THEN
    INSERT INTO gwt_test_component(component_type, id, label, project_id, test_case_id, test_suite_id, new_project_id, soapui_project)
    SELECT 'project', nextval('gwt_test_component_id_seq'), label, NULL, NULL, NULL, NULL, project_id_var
    FROM gwt_soapui_project
    WHERE gwt_soapui_project.id = project_id_var;

    UPDATE gwt_soapui_project SET gwt_project = (SELECT max(id) FROM gwt_test_component) WHERE id=project_id_var;

    UPDATE gwt_test_component SET new_project_id = (SELECT max(id) FROM gwt_test_component) WHERE project_id = project_id_var;

    UPDATE gwt_custom_property SET test_component_id =(SELECT id FROM gwt_test_component WHERE gwt_test_component.soapui_project = project_id_var) WHERE project_id = project_id_var;

  END IF;

  return 1 ;
END;$$;


ALTER FUNCTION public.update_soapui_project(project_id_var integer) OWNER TO gazelle;

--
-- Name: update_soapui_project_all(); Type: FUNCTION; Schema: public; Owner: gazelle
--

CREATE FUNCTION public.update_soapui_project_all() RETURNS boolean
    LANGUAGE plpgsql
    AS $$
DECLARE
  idColumn integer;
BEGIN
  idColumn = (select min( id ) from gwt_soapui_project);

  WHILE idColumn is not null LOOP
    PERFORM update_soapui_project(idColumn);
    idColumn = (select min( id ) from gwt_soapui_project where id > idColumn);
  END LOOP;

  return 1 ;
END;$$;


ALTER FUNCTION public.update_soapui_project_all() OWNER TO gazelle;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: affinity_domain; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.affinity_domain (
    id integer NOT NULL,
    keyword character varying(255),
    label_to_display character varying(255),
    profile character varying(255)
);


ALTER TABLE public.affinity_domain OWNER TO gazelle;

--
-- Name: affinity_domain_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.affinity_domain_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.affinity_domain_id_seq OWNER TO gazelle;

--
-- Name: affinity_domain_transactions; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.affinity_domain_transactions (
    affinity_domain_id integer NOT NULL,
    transaction_id integer NOT NULL
);


ALTER TABLE public.affinity_domain_transactions OWNER TO gazelle;

--
-- Name: app_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.app_configuration (
    id integer NOT NULL,
    value character varying(255),
    variable character varying(255)
);


ALTER TABLE public.app_configuration OWNER TO gazelle;

--
-- Name: app_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.app_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cfg_configuration (
    id integer NOT NULL,
    comment character varying(255),
    approved boolean NOT NULL,
    is_secured boolean,
    host_id integer
);


ALTER TABLE public.cfg_configuration OWNER TO gazelle;

--
-- Name: cfg_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cfg_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cfg_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_dicom_scp_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cfg_dicom_scp_configuration (
    id integer NOT NULL,
    comments character varying(255),
    ae_title character varying(16) NOT NULL,
    modality_type character varying(255),
    port integer,
    port_secure integer,
    transfer_role character varying(255) NOT NULL,
    configuration_id integer NOT NULL,
    gs_system_id integer,
    sop_class_id integer NOT NULL
);


ALTER TABLE public.cfg_dicom_scp_configuration OWNER TO gazelle;

--
-- Name: cfg_dicom_scp_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cfg_dicom_scp_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cfg_dicom_scp_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_dicom_scu_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cfg_dicom_scu_configuration (
    id integer NOT NULL,
    comments character varying(255),
    ae_title character varying(16) NOT NULL,
    modality_type character varying(255),
    port integer,
    port_secure integer,
    transfer_role character varying(255) NOT NULL,
    configuration_id integer NOT NULL,
    gs_system_id integer,
    sop_class_id integer NOT NULL
);


ALTER TABLE public.cfg_dicom_scu_configuration OWNER TO gazelle;

--
-- Name: cfg_dicom_scu_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cfg_dicom_scu_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cfg_dicom_scu_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_hl7_initiator_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cfg_hl7_initiator_configuration (
    id integer NOT NULL,
    comments character varying(255),
    assigning_authority character varying(255),
    sending_receiving_application character varying(255) NOT NULL,
    sending_receiving_facility character varying(255) NOT NULL,
    configuration_id integer NOT NULL,
    gs_system_id integer
);


ALTER TABLE public.cfg_hl7_initiator_configuration OWNER TO gazelle;

--
-- Name: cfg_hl7_initiator_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cfg_hl7_initiator_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cfg_hl7_initiator_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_hl7_responder_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cfg_hl7_responder_configuration (
    id integer NOT NULL,
    comments character varying(255),
    assigning_authority character varying(255),
    sending_receiving_application character varying(255) NOT NULL,
    sending_receiving_facility character varying(255) NOT NULL,
    port integer,
    port_out integer,
    port_secure integer,
    configuration_id integer NOT NULL,
    gs_system_id integer
);


ALTER TABLE public.cfg_hl7_responder_configuration OWNER TO gazelle;

--
-- Name: cfg_hl7_responder_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cfg_hl7_responder_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cfg_hl7_responder_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_hl7_v3_initiator_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cfg_hl7_v3_initiator_configuration (
    id integer NOT NULL,
    comments character varying(255),
    assigning_authority character varying(255),
    sending_receiving_application character varying(255) NOT NULL,
    sending_receiving_facility character varying(255) NOT NULL,
    configuration_id integer NOT NULL,
    gs_system_id integer
);


ALTER TABLE public.cfg_hl7_v3_initiator_configuration OWNER TO gazelle;

--
-- Name: cfg_hl7_v3_initiator_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cfg_hl7_v3_initiator_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cfg_hl7_v3_initiator_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_hl7_v3_responder_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cfg_hl7_v3_responder_configuration (
    id integer NOT NULL,
    comments character varying(255),
    assigning_authority character varying(255),
    sending_receiving_application character varying(255) NOT NULL,
    sending_receiving_facility character varying(255) NOT NULL,
    port integer,
    port_secured integer,
    url character varying(255) NOT NULL,
    usage character varying(255),
    configuration_id integer NOT NULL,
    gs_system_id integer
);


ALTER TABLE public.cfg_hl7_v3_responder_configuration OWNER TO gazelle;

--
-- Name: cfg_hl7_v3_responder_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cfg_hl7_v3_responder_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cfg_hl7_v3_responder_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_host; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cfg_host (
    id integer NOT NULL,
    alias character varying(255),
    comment character varying(255),
    hostname character varying(255) NOT NULL,
    ip character varying(255)
);


ALTER TABLE public.cfg_host OWNER TO gazelle;

--
-- Name: cfg_host_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cfg_host_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cfg_host_id_seq OWNER TO gazelle;

--
-- Name: cfg_sop_class; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cfg_sop_class (
    id integer NOT NULL,
    keyword character varying(255) NOT NULL,
    name character varying(255)
);


ALTER TABLE public.cfg_sop_class OWNER TO gazelle;

--
-- Name: cfg_sop_class_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cfg_sop_class_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cfg_sop_class_id_seq OWNER TO gazelle;

--
-- Name: cfg_syslog_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cfg_syslog_configuration (
    id integer NOT NULL,
    comments character varying(255),
    port integer,
    configuration_id integer NOT NULL,
    gs_system_id integer
);


ALTER TABLE public.cfg_syslog_configuration OWNER TO gazelle;

--
-- Name: cfg_syslog_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cfg_syslog_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cfg_syslog_configuration_id_seq OWNER TO gazelle;

--
-- Name: cfg_web_service_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cfg_web_service_configuration (
    id integer NOT NULL,
    comments character varying(255),
    port integer,
    port_secured integer,
    url character varying(255) NOT NULL,
    usage character varying(255),
    assigning_authority character varying(255),
    configuration_id integer NOT NULL,
    gs_system_id integer
);


ALTER TABLE public.cfg_web_service_configuration OWNER TO gazelle;

--
-- Name: cfg_web_service_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cfg_web_service_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cfg_web_service_configuration_id_seq OWNER TO gazelle;

--
-- Name: cmn_company_details; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cmn_company_details (
    id integer NOT NULL,
    company_keyword character varying(255)
);


ALTER TABLE public.cmn_company_details OWNER TO gazelle;

--
-- Name: cmn_company_details_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cmn_company_details_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmn_company_details_id_seq OWNER TO gazelle;

--
-- Name: cmn_home; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cmn_home (
    id integer NOT NULL,
    home_title character varying(255),
    iso3_language character varying(255),
    main_content text
);


ALTER TABLE public.cmn_home OWNER TO gazelle;

--
-- Name: cmn_home_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cmn_home_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmn_home_id_seq OWNER TO gazelle;

--
-- Name: cmn_ip_address; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cmn_ip_address (
    id integer NOT NULL,
    added_by character varying(255),
    added_on timestamp without time zone,
    value character varying(255),
    company_details_id integer
);


ALTER TABLE public.cmn_ip_address OWNER TO gazelle;

--
-- Name: cmn_ip_address_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cmn_ip_address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmn_ip_address_id_seq OWNER TO gazelle;

--
-- Name: cmn_message_instance; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cmn_message_instance (
    id integer NOT NULL,
    content bytea,
    issuer character varying(255),
    type character varying(255),
    validation_detailed_result bytea,
    validation_status character varying(255),
    issuing_actor integer,
    issuer_ip_address character varying(255),
    payload bytea
);


ALTER TABLE public.cmn_message_instance OWNER TO gazelle;

--
-- Name: cmn_message_instance_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cmn_message_instance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmn_message_instance_id_seq OWNER TO gazelle;

--
-- Name: cmn_message_instance_metadata; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cmn_message_instance_metadata (
    id integer NOT NULL,
    label character varying(255),
    value character varying(255),
    message_instance_id integer
);


ALTER TABLE public.cmn_message_instance_metadata OWNER TO gazelle;

--
-- Name: cmn_message_instance_metadata_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cmn_message_instance_metadata_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmn_message_instance_metadata_id_seq OWNER TO gazelle;

--
-- Name: cmn_receiver_console; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cmn_receiver_console (
    id integer NOT NULL,
    code character varying(255),
    comment text,
    message_identifier character varying(255),
    message_type character varying(255),
    sut character varying(255),
    "timestamp" timestamp without time zone,
    domain_id integer,
    simulated_actor_id integer,
    transaction_id integer
);


ALTER TABLE public.cmn_receiver_console OWNER TO gazelle;

--
-- Name: cmn_receiver_console_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cmn_receiver_console_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmn_receiver_console_id_seq OWNER TO gazelle;

--
-- Name: cmn_transaction_instance; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cmn_transaction_instance (
    id integer NOT NULL,
    standard character varying(64),
    "timestamp" timestamp without time zone,
    username character varying(255),
    is_visible boolean,
    domain_id integer,
    request_id integer,
    response_id integer,
    simulated_actor_id integer,
    transaction_id integer,
    company_keyword character varying(255)
);


ALTER TABLE public.cmn_transaction_instance OWNER TO gazelle;

--
-- Name: cmn_transaction_instance_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cmn_transaction_instance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmn_transaction_instance_id_seq OWNER TO gazelle;

--
-- Name: cmn_validator_usage; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cmn_validator_usage (
    id integer NOT NULL,
    caller character varying(255),
    date date,
    status character varying(255),
    type character varying(255)
);


ALTER TABLE public.cmn_validator_usage OWNER TO gazelle;

--
-- Name: cmn_validator_usage_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cmn_validator_usage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmn_validator_usage_id_seq OWNER TO gazelle;

--
-- Name: cmn_value_set; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cmn_value_set (
    id integer NOT NULL,
    accessible boolean,
    last_check timestamp without time zone,
    usage character varying(255),
    value_set_keyword character varying(255),
    value_set_name character varying(255),
    value_set_oid character varying(255)
);


ALTER TABLE public.cmn_value_set OWNER TO gazelle;

--
-- Name: cmn_value_set_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cmn_value_set_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmn_value_set_id_seq OWNER TO gazelle;

--
-- Name: gs_contextual_information; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gs_contextual_information (
    id integer NOT NULL,
    label character varying(255) NOT NULL,
    value character varying(255),
    path integer NOT NULL
);


ALTER TABLE public.gs_contextual_information OWNER TO gazelle;

--
-- Name: gs_contextual_information_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.gs_contextual_information_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gs_contextual_information_id_seq OWNER TO gazelle;

--
-- Name: gs_contextual_information_instance; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gs_contextual_information_instance (
    id integer NOT NULL,
    form character varying(255),
    value character varying(255),
    contextual_information_id integer NOT NULL,
    test_steps_instance_id integer NOT NULL
);


ALTER TABLE public.gs_contextual_information_instance OWNER TO gazelle;

--
-- Name: gs_contextual_information_instance_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.gs_contextual_information_instance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gs_contextual_information_instance_id_seq OWNER TO gazelle;

--
-- Name: gs_message; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gs_message (
    id integer NOT NULL,
    message_content bytea,
    message_type_id character varying(255),
    time_stamp timestamp without time zone,
    test_instance_participants_receiver_id integer,
    test_instance_participants_sender_id integer,
    transaction_id integer
);


ALTER TABLE public.gs_message OWNER TO gazelle;

--
-- Name: gs_message_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.gs_message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gs_message_id_seq OWNER TO gazelle;

--
-- Name: gs_system; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gs_system (
    id integer NOT NULL,
    institution_keyword character varying(255),
    keyword character varying(255),
    system_owner character varying(255)
);


ALTER TABLE public.gs_system OWNER TO gazelle;

--
-- Name: gs_system_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.gs_system_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gs_system_id_seq OWNER TO gazelle;

--
-- Name: gs_test_instance; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gs_test_instance (
    id integer NOT NULL,
    server_test_instance_id character varying(255) NOT NULL,
    test_instance_status_id integer
);


ALTER TABLE public.gs_test_instance OWNER TO gazelle;

--
-- Name: gs_test_instance_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.gs_test_instance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gs_test_instance_id_seq OWNER TO gazelle;

--
-- Name: gs_test_instance_oid; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gs_test_instance_oid (
    oid_configuration_id integer NOT NULL,
    test_instance_id integer NOT NULL
);


ALTER TABLE public.gs_test_instance_oid OWNER TO gazelle;

--
-- Name: gs_test_instance_participants; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gs_test_instance_participants (
    id integer NOT NULL,
    server_aipo_id character varying(255) NOT NULL,
    server_test_instance_participants_id character varying(255) NOT NULL,
    aipo_id integer,
    system_id integer
);


ALTER TABLE public.gs_test_instance_participants OWNER TO gazelle;

--
-- Name: gs_test_instance_participants_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.gs_test_instance_participants_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gs_test_instance_participants_id_seq OWNER TO gazelle;

--
-- Name: gs_test_instance_status; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gs_test_instance_status (
    id integer NOT NULL,
    description character varying(255),
    keyword character varying(255),
    label_to_display character varying(255)
);


ALTER TABLE public.gs_test_instance_status OWNER TO gazelle;

--
-- Name: gs_test_instance_status_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.gs_test_instance_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gs_test_instance_status_id_seq OWNER TO gazelle;

--
-- Name: gs_test_instance_test_instance_participants; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gs_test_instance_test_instance_participants (
    test_instance_participants_id integer NOT NULL,
    test_instance_id integer NOT NULL
);


ALTER TABLE public.gs_test_instance_test_instance_participants OWNER TO gazelle;

--
-- Name: gs_test_steps_instance; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gs_test_steps_instance (
    id integer NOT NULL,
    dicom_scp_config_id integer,
    dicom_scu_config_id integer,
    hl7v2_initiator_config_id integer,
    hl7v2_responder_config_id integer,
    hl7v3_initiator_config_id integer,
    hl7v3_responder_config_id integer,
    syslog_config_id integer,
    testinstance_id integer,
    web_service_config_id integer
);


ALTER TABLE public.gs_test_steps_instance OWNER TO gazelle;

--
-- Name: gs_test_steps_instance_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.gs_test_steps_instance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gs_test_steps_instance_id_seq OWNER TO gazelle;

--
-- Name: gwt_actor_type; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gwt_actor_type (
    id integer NOT NULL,
    description character varying(255),
    tm_name character varying(255),
    xdstools_name character varying(255) NOT NULL
);


ALTER TABLE public.gwt_actor_type OWNER TO gazelle;

--
-- Name: gwt_actor_type_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.gwt_actor_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gwt_actor_type_id_seq OWNER TO gazelle;

--
-- Name: gwt_attachement; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gwt_attachement (
    id integer NOT NULL,
    content bytea,
    content_id character varying(255),
    content_type character varying(255),
    encoding character varying(255),
    name character varying(255) NOT NULL,
    part character varying(255),
    size bigint,
    url character varying(255)
);


ALTER TABLE public.gwt_attachement OWNER TO gazelle;

--
-- Name: gwt_attachement_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.gwt_attachement_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gwt_attachement_id_seq OWNER TO gazelle;

--
-- Name: gwt_calling_tool; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gwt_calling_tool (
    id integer NOT NULL,
    label character varying(255) NOT NULL,
    oid character varying(255) NOT NULL,
    url character varying(255) NOT NULL
);


ALTER TABLE public.gwt_calling_tool OWNER TO gazelle;

--
-- Name: gwt_calling_tool_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.gwt_calling_tool_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gwt_calling_tool_id_seq OWNER TO gazelle;

--
-- Name: gwt_cross_validation_result; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gwt_cross_validation_result (
    id integer NOT NULL,
    log_oid character varying(255),
    permanent_link character varying(255),
    xvalidation_date character varying(255),
    xvalidation_status character varying(255),
    gwt_project_result_id integer,
    test_step_result_id integer
);


ALTER TABLE public.gwt_cross_validation_result OWNER TO gazelle;

--
-- Name: gwt_cross_validation_result_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.gwt_cross_validation_result_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gwt_cross_validation_result_id_seq OWNER TO gazelle;

--
-- Name: gwt_custom_property; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gwt_custom_property (
    id integer NOT NULL,
    default_value text,
    label character varying(255) NOT NULL,
    to_modify boolean,
    project_id integer,
    test_component_id integer
);


ALTER TABLE public.gwt_custom_property OWNER TO gazelle;

--
-- Name: gwt_custom_property_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.gwt_custom_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gwt_custom_property_id_seq OWNER TO gazelle;

--
-- Name: gwt_custom_property_used; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gwt_custom_property_used (
    id integer NOT NULL,
    value text NOT NULL,
    custom_property_id integer,
    execution_id integer,
    sut integer
);


ALTER TABLE public.gwt_custom_property_used OWNER TO gazelle;

--
-- Name: gwt_custom_property_used_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.gwt_custom_property_used_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gwt_custom_property_used_id_seq OWNER TO gazelle;

--
-- Name: gwt_execution; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gwt_execution (
    id integer NOT NULL,
    company character varying(255),
    launch_by character varying(255) NOT NULL,
    launch_date timestamp without time zone NOT NULL,
    soapui_project_id integer,
    test_component_id integer,
    execution integer,
    status character varying(255)
);


ALTER TABLE public.gwt_execution OWNER TO gazelle;

--
-- Name: gwt_execution_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.gwt_execution_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gwt_execution_id_seq OWNER TO gazelle;

--
-- Name: gwt_keystore; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gwt_keystore (
    id integer NOT NULL,
    label character varying(255) NOT NULL,
    path character varying(255) NOT NULL,
    soapui_project integer,
    password character varying(255),
    execution integer,
    default_keystore boolean
);


ALTER TABLE public.gwt_keystore OWNER TO gazelle;

--
-- Name: gwt_keystore_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.gwt_keystore_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gwt_keystore_id_seq OWNER TO gazelle;

--
-- Name: gwt_mock_message; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gwt_mock_message (
    id integer NOT NULL,
    message_instance_id integer
);


ALTER TABLE public.gwt_mock_message OWNER TO gazelle;

--
-- Name: gwt_mock_message_gwt_mock_validation_result; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gwt_mock_message_gwt_mock_validation_result (
    gwt_mock_message_id integer NOT NULL,
    mockvalidationresultlist_id integer NOT NULL
);


ALTER TABLE public.gwt_mock_message_gwt_mock_validation_result OWNER TO gazelle;

--
-- Name: gwt_mock_message_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.gwt_mock_message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gwt_mock_message_id_seq OWNER TO gazelle;

--
-- Name: gwt_mock_validation_result; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gwt_mock_validation_result (
    id integer NOT NULL,
    oid character varying(255),
    permanent_link character varying(255),
    type character varying(255),
    validation_date character varying(255),
    validation_status character varying(255),
    mock_message_id integer,
    message_instance_id integer
);


ALTER TABLE public.gwt_mock_validation_result OWNER TO gazelle;

--
-- Name: gwt_mock_validation_result_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.gwt_mock_validation_result_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gwt_mock_validation_result_id_seq OWNER TO gazelle;

--
-- Name: gwt_nonce; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gwt_nonce (
    id integer NOT NULL,
    nonce character varying(255),
    url character varying(255)
);


ALTER TABLE public.gwt_nonce OWNER TO gazelle;

--
-- Name: gwt_nonce_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.gwt_nonce_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gwt_nonce_id_seq OWNER TO gazelle;

--
-- Name: gwt_option; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gwt_option (
    id integer NOT NULL,
    description character varying(255),
    tm_name character varying(255),
    xdstools_name character varying(255) NOT NULL
);


ALTER TABLE public.gwt_option OWNER TO gazelle;

--
-- Name: gwt_option_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.gwt_option_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gwt_option_id_seq OWNER TO gazelle;

--
-- Name: gwt_profile; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gwt_profile (
    id integer NOT NULL,
    tm_name character varying(255),
    xdstools_name character varying(255) NOT NULL
);


ALTER TABLE public.gwt_profile OWNER TO gazelle;

--
-- Name: gwt_profile_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.gwt_profile_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gwt_profile_id_seq OWNER TO gazelle;

--
-- Name: gwt_project_result; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gwt_project_result (
    id integer NOT NULL,
    xvalidatoroid character varying(255),
    xvalidatoroidindex character varying(255),
    execution_id integer,
    project_id integer,
    x_validation_result_id integer
);


ALTER TABLE public.gwt_project_result OWNER TO gazelle;

--
-- Name: gwt_project_result_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.gwt_project_result_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gwt_project_result_id_seq OWNER TO gazelle;

--
-- Name: gwt_project_result_test_step_info; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gwt_project_result_test_step_info (
    gwt_project_result_id integer NOT NULL,
    teststepinfolist_id integer NOT NULL
);


ALTER TABLE public.gwt_project_result_test_step_info OWNER TO gazelle;

--
-- Name: gwt_project_result_x_validator_input_info; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gwt_project_result_x_validator_input_info (
    gwt_project_result_id integer NOT NULL,
    xvalidatorinputinfolist_id integer NOT NULL
);


ALTER TABLE public.gwt_project_result_x_validator_input_info OWNER TO gazelle;

--
-- Name: gwt_soapui_messages; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gwt_soapui_messages (
    gwt_test_step_result_id integer NOT NULL,
    soapui_messages text
);


ALTER TABLE public.gwt_soapui_messages OWNER TO gazelle;

--
-- Name: gwt_soapui_project; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gwt_soapui_project (
    id integer NOT NULL,
    active boolean NOT NULL,
    creation_date timestamp without time zone NOT NULL,
    label character varying(255) NOT NULL,
    owner character varying(255) NOT NULL,
    version integer NOT NULL,
    xml_file_path character varying(255) NOT NULL,
    gwt_project integer,
    keystore integer
);


ALTER TABLE public.gwt_soapui_project OWNER TO gazelle;

--
-- Name: gwt_soapui_project_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.gwt_soapui_project_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gwt_soapui_project_id_seq OWNER TO gazelle;

--
-- Name: gwt_test_component; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gwt_test_component (
    component_type character varying(31) NOT NULL,
    id integer NOT NULL,
    label character varying(255) NOT NULL,
    test_case_id integer,
    test_suite_id integer,
    project_id integer,
    soapui_project integer,
    executable boolean,
    disable boolean,
    oid_xval character varying(255)
);


ALTER TABLE public.gwt_test_component OWNER TO gazelle;

--
-- Name: gwt_test_component_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.gwt_test_component_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gwt_test_component_id_seq OWNER TO gazelle;

--
-- Name: gwt_test_component_oid_property_info; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gwt_test_component_oid_property_info (
    gwt_test_component_id integer NOT NULL,
    oidpropertylist_id integer NOT NULL
);


ALTER TABLE public.gwt_test_component_oid_property_info OWNER TO gazelle;

--
-- Name: gwt_test_instance_result; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gwt_test_instance_result (
    id integer NOT NULL,
    laststatus character varying(255),
    testinstance integer NOT NULL,
    testkeyword character varying(255),
    testname character varying(255),
    test_execution_id integer,
    test_suite_id integer
);


ALTER TABLE public.gwt_test_instance_result OWNER TO gazelle;

--
-- Name: gwt_test_instance_result_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.gwt_test_instance_result_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gwt_test_instance_result_id_seq OWNER TO gazelle;

--
-- Name: gwt_test_instance_tm; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gwt_test_instance_tm (
    id integer NOT NULL,
    system_keyword character varying(255) NOT NULL,
    test_instance_id integer NOT NULL,
    testing_session integer,
    calling_tool_id integer,
    nonce integer
);


ALTER TABLE public.gwt_test_instance_tm OWNER TO gazelle;

--
-- Name: gwt_test_instance_tm_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.gwt_test_instance_tm_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gwt_test_instance_tm_id_seq OWNER TO gazelle;

--
-- Name: gwt_test_step_result; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gwt_test_step_result (
    id integer NOT NULL,
    endpoint character varying(255),
    status character varying(255) NOT NULL,
    time_taken bigint NOT NULL,
    execution_id integer,
    messages integer,
    test_step_id integer,
    type character varying(255),
    x_validation_result_id integer
);


ALTER TABLE public.gwt_test_step_result OWNER TO gazelle;

--
-- Name: gwt_test_step_result_attachment_request; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gwt_test_step_result_attachment_request (
    test_step_id integer NOT NULL,
    attachment_id integer NOT NULL
);


ALTER TABLE public.gwt_test_step_result_attachment_request OWNER TO gazelle;

--
-- Name: gwt_test_step_result_attachment_response; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gwt_test_step_result_attachment_response (
    test_step_id integer NOT NULL,
    attachment_id integer NOT NULL
);


ALTER TABLE public.gwt_test_step_result_attachment_response OWNER TO gazelle;

--
-- Name: gwt_test_step_result_gwt_validation_result; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gwt_test_step_result_gwt_validation_result (
    gwt_test_step_result_id integer NOT NULL,
    validationresults_id integer NOT NULL
);


ALTER TABLE public.gwt_test_step_result_gwt_validation_result OWNER TO gazelle;

--
-- Name: gwt_test_step_result_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.gwt_test_step_result_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gwt_test_step_result_id_seq OWNER TO gazelle;

--
-- Name: gwt_validation_result; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gwt_validation_result (
    id integer NOT NULL,
    oid character varying(255),
    permanent_link character varying(255),
    type character varying(255),
    validation_date character varying(255),
    validation_status character varying(255),
    test_step_result_id integer
);


ALTER TABLE public.gwt_validation_result OWNER TO gazelle;

--
-- Name: gwt_validation_result_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.gwt_validation_result_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gwt_validation_result_id_seq OWNER TO gazelle;

--
-- Name: oid_property_info; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.oid_property_info (
    id integer NOT NULL,
    index character varying(255),
    oid character varying(255),
    gwt_project_id integer
);


ALTER TABLE public.oid_property_info OWNER TO gazelle;

--
-- Name: oid_property_info_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.oid_property_info_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.oid_property_info_id_seq OWNER TO gazelle;

--
-- Name: sys_conf_type_usages; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.sys_conf_type_usages (
    system_configuration_id integer NOT NULL,
    listusages_id integer NOT NULL
);


ALTER TABLE public.sys_conf_type_usages OWNER TO gazelle;

--
-- Name: system_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.system_configuration (
    id integer NOT NULL,
    is_available boolean,
    is_public boolean,
    name character varying(255),
    owner character varying(255),
    owner_company character varying(255),
    system_name character varying(255),
    url character varying(255)
);


ALTER TABLE public.system_configuration OWNER TO gazelle;

--
-- Name: system_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.system_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.system_configuration_id_seq OWNER TO gazelle;

--
-- Name: test_step_info; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.test_step_info (
    id integer NOT NULL,
    input_type character varying(255),
    test_step_name character varying(255),
    gwt_project_result_id integer
);


ALTER TABLE public.test_step_info OWNER TO gazelle;

--
-- Name: test_step_info_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.test_step_info_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.test_step_info_id_seq OWNER TO gazelle;

--
-- Name: tf_actor; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_actor (
    id integer NOT NULL,
    description character varying(2048),
    keyword character varying(128) NOT NULL,
    name character varying(128),
    can_act_as_responder boolean
);


ALTER TABLE public.tf_actor OWNER TO gazelle;

--
-- Name: tf_actor_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_actor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_actor_id_seq OWNER TO gazelle;

--
-- Name: tf_actor_integration_profile; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_actor_integration_profile (
    id integer NOT NULL,
    actor_id integer,
    integration_profile_id integer
);


ALTER TABLE public.tf_actor_integration_profile OWNER TO gazelle;

--
-- Name: tf_actor_integration_profile_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_actor_integration_profile_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_actor_integration_profile_id_seq OWNER TO gazelle;

--
-- Name: tf_actor_integration_profile_option; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_actor_integration_profile_option (
    id integer NOT NULL,
    actor_integration_profile_id integer NOT NULL,
    integration_profile_option_id integer
);


ALTER TABLE public.tf_actor_integration_profile_option OWNER TO gazelle;

--
-- Name: tf_actor_integration_profile_option_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_actor_integration_profile_option_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_actor_integration_profile_option_id_seq OWNER TO gazelle;

--
-- Name: tf_domain; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_domain (
    id integer NOT NULL,
    description character varying(2048),
    keyword character varying(128) NOT NULL,
    name character varying(128)
);


ALTER TABLE public.tf_domain OWNER TO gazelle;

--
-- Name: tf_domain_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_domain_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_domain_id_seq OWNER TO gazelle;

--
-- Name: tf_domain_integration_profiles; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_domain_integration_profiles (
    domain_id integer NOT NULL,
    integration_profile_id integer NOT NULL
);


ALTER TABLE public.tf_domain_integration_profiles OWNER TO gazelle;

--
-- Name: tf_integration_profile; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_integration_profile (
    id integer NOT NULL,
    description character varying(2048),
    keyword character varying(128) NOT NULL,
    name character varying(128)
);


ALTER TABLE public.tf_integration_profile OWNER TO gazelle;

--
-- Name: tf_integration_profile_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_integration_profile_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_integration_profile_id_seq OWNER TO gazelle;

--
-- Name: tf_integration_profile_option; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_integration_profile_option (
    id integer NOT NULL,
    description character varying(2048),
    keyword character varying(128) NOT NULL,
    name character varying(128)
);


ALTER TABLE public.tf_integration_profile_option OWNER TO gazelle;

--
-- Name: tf_integration_profile_option_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_integration_profile_option_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_integration_profile_option_id_seq OWNER TO gazelle;

--
-- Name: tf_transaction; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_transaction (
    id integer NOT NULL,
    description character varying(2048),
    keyword character varying(128) NOT NULL,
    name character varying(128)
);


ALTER TABLE public.tf_transaction OWNER TO gazelle;

--
-- Name: tf_transaction_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_transaction_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_transaction_id_seq OWNER TO gazelle;

--
-- Name: tm_oid; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_oid (
    id integer NOT NULL,
    label character varying(255) NOT NULL,
    oid character varying(255) NOT NULL,
    system_id integer
);


ALTER TABLE public.tm_oid OWNER TO gazelle;

--
-- Name: tm_oid_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_oid_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_oid_id_seq OWNER TO gazelle;

--
-- Name: tm_path; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_path (
    id integer NOT NULL,
    description character varying(255) NOT NULL,
    keyword character varying(255) NOT NULL,
    type character varying(255)
);


ALTER TABLE public.tm_path OWNER TO gazelle;

--
-- Name: tm_path_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_path_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_path_id_seq OWNER TO gazelle;

--
-- Name: usage_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.usage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usage_id_seq OWNER TO gazelle;

--
-- Name: usage_metadata; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.usage_metadata (
    id integer NOT NULL,
    action character varying(255),
    keyword character varying(255),
    affinity_id integer,
    transaction_id integer
);


ALTER TABLE public.usage_metadata OWNER TO gazelle;

--
-- Name: x_validator_input_info; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.x_validator_input_info (
    id integer NOT NULL,
    input_name character varying(255),
    input_type character varying(255),
    message_content bytea,
    gwt_project_result_id integer
);


ALTER TABLE public.x_validator_input_info OWNER TO gazelle;

--
-- Name: x_validator_input_info_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.x_validator_input_info_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.x_validator_input_info_id_seq OWNER TO gazelle;

--
-- Name: x_validator_oids_map; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.x_validator_oids_map (
    project_id integer NOT NULL,
    x_validator_oid character varying(255),
    index character varying(255) NOT NULL
);


ALTER TABLE public.x_validator_oids_map OWNER TO gazelle;

--
-- Name: affinity_domain affinity_domain_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.affinity_domain
    ADD CONSTRAINT affinity_domain_pkey PRIMARY KEY (id);


--
-- Name: app_configuration app_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.app_configuration
    ADD CONSTRAINT app_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_configuration cfg_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_configuration
    ADD CONSTRAINT cfg_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_dicom_scp_configuration cfg_dicom_scp_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_dicom_scp_configuration
    ADD CONSTRAINT cfg_dicom_scp_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_dicom_scu_configuration cfg_dicom_scu_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_dicom_scu_configuration
    ADD CONSTRAINT cfg_dicom_scu_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_hl7_initiator_configuration cfg_hl7_initiator_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_initiator_configuration
    ADD CONSTRAINT cfg_hl7_initiator_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_hl7_responder_configuration cfg_hl7_responder_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_responder_configuration
    ADD CONSTRAINT cfg_hl7_responder_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_hl7_v3_initiator_configuration cfg_hl7_v3_initiator_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_v3_initiator_configuration
    ADD CONSTRAINT cfg_hl7_v3_initiator_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_hl7_v3_responder_configuration cfg_hl7_v3_responder_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_v3_responder_configuration
    ADD CONSTRAINT cfg_hl7_v3_responder_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_host cfg_host_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_host
    ADD CONSTRAINT cfg_host_pkey PRIMARY KEY (id);


--
-- Name: cfg_sop_class cfg_sop_class_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_sop_class
    ADD CONSTRAINT cfg_sop_class_pkey PRIMARY KEY (id);


--
-- Name: cfg_syslog_configuration cfg_syslog_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_syslog_configuration
    ADD CONSTRAINT cfg_syslog_configuration_pkey PRIMARY KEY (id);


--
-- Name: cfg_web_service_configuration cfg_web_service_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_web_service_configuration
    ADD CONSTRAINT cfg_web_service_configuration_pkey PRIMARY KEY (id);


--
-- Name: cmn_company_details cmn_company_details_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_company_details
    ADD CONSTRAINT cmn_company_details_pkey PRIMARY KEY (id);


--
-- Name: cmn_home cmn_home_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_home
    ADD CONSTRAINT cmn_home_pkey PRIMARY KEY (id);


--
-- Name: cmn_ip_address cmn_ip_address_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_ip_address
    ADD CONSTRAINT cmn_ip_address_pkey PRIMARY KEY (id);


--
-- Name: cmn_message_instance_metadata cmn_message_instance_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_message_instance_metadata
    ADD CONSTRAINT cmn_message_instance_metadata_pkey PRIMARY KEY (id);


--
-- Name: cmn_message_instance cmn_message_instance_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_message_instance
    ADD CONSTRAINT cmn_message_instance_pkey PRIMARY KEY (id);


--
-- Name: cmn_receiver_console cmn_receiver_console_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_receiver_console
    ADD CONSTRAINT cmn_receiver_console_pkey PRIMARY KEY (id);


--
-- Name: cmn_transaction_instance cmn_transaction_instance_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_transaction_instance
    ADD CONSTRAINT cmn_transaction_instance_pkey PRIMARY KEY (id);


--
-- Name: cmn_validator_usage cmn_validator_usage_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_validator_usage
    ADD CONSTRAINT cmn_validator_usage_pkey PRIMARY KEY (id);


--
-- Name: cmn_value_set cmn_value_set_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_value_set
    ADD CONSTRAINT cmn_value_set_pkey PRIMARY KEY (id);


--
-- Name: gs_contextual_information_instance gs_contextual_information_instance_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_contextual_information_instance
    ADD CONSTRAINT gs_contextual_information_instance_pkey PRIMARY KEY (id);


--
-- Name: gs_contextual_information gs_contextual_information_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_contextual_information
    ADD CONSTRAINT gs_contextual_information_pkey PRIMARY KEY (id);


--
-- Name: gs_message gs_message_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_message
    ADD CONSTRAINT gs_message_pkey PRIMARY KEY (id);


--
-- Name: gs_system gs_system_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_system
    ADD CONSTRAINT gs_system_pkey PRIMARY KEY (id);


--
-- Name: gs_test_instance_participants gs_test_instance_participants_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance_participants
    ADD CONSTRAINT gs_test_instance_participants_pkey PRIMARY KEY (id);


--
-- Name: gs_test_instance gs_test_instance_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance
    ADD CONSTRAINT gs_test_instance_pkey PRIMARY KEY (id);


--
-- Name: gs_test_instance_status gs_test_instance_status_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance_status
    ADD CONSTRAINT gs_test_instance_status_pkey PRIMARY KEY (id);


--
-- Name: gs_test_steps_instance gs_test_steps_instance_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_steps_instance
    ADD CONSTRAINT gs_test_steps_instance_pkey PRIMARY KEY (id);


--
-- Name: gwt_actor_type gwt_actor_type_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_actor_type
    ADD CONSTRAINT gwt_actor_type_pkey PRIMARY KEY (id);


--
-- Name: gwt_attachement gwt_attachement_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_attachement
    ADD CONSTRAINT gwt_attachement_pkey PRIMARY KEY (id);


--
-- Name: gwt_calling_tool gwt_calling_tool_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_calling_tool
    ADD CONSTRAINT gwt_calling_tool_pkey PRIMARY KEY (id);


--
-- Name: gwt_cross_validation_result gwt_cross_validation_result_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_cross_validation_result
    ADD CONSTRAINT gwt_cross_validation_result_pkey PRIMARY KEY (id);


--
-- Name: gwt_custom_property gwt_custom_property_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_custom_property
    ADD CONSTRAINT gwt_custom_property_pkey PRIMARY KEY (id);


--
-- Name: gwt_custom_property_used gwt_custom_property_used_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_custom_property_used
    ADD CONSTRAINT gwt_custom_property_used_pkey PRIMARY KEY (id);


--
-- Name: gwt_execution gwt_execution_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_execution
    ADD CONSTRAINT gwt_execution_pkey PRIMARY KEY (id);


--
-- Name: gwt_keystore gwt_keystore_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_keystore
    ADD CONSTRAINT gwt_keystore_pkey PRIMARY KEY (id);


--
-- Name: gwt_mock_message gwt_mock_message_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_mock_message
    ADD CONSTRAINT gwt_mock_message_pkey PRIMARY KEY (id);


--
-- Name: gwt_mock_validation_result gwt_mock_validation_result_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_mock_validation_result
    ADD CONSTRAINT gwt_mock_validation_result_pkey PRIMARY KEY (id);


--
-- Name: gwt_nonce gwt_nonce_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_nonce
    ADD CONSTRAINT gwt_nonce_pkey PRIMARY KEY (id);


--
-- Name: gwt_option gwt_option_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_option
    ADD CONSTRAINT gwt_option_pkey PRIMARY KEY (id);


--
-- Name: gwt_profile gwt_profile_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_profile
    ADD CONSTRAINT gwt_profile_pkey PRIMARY KEY (id);


--
-- Name: gwt_project_result gwt_project_result_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_project_result
    ADD CONSTRAINT gwt_project_result_pkey PRIMARY KEY (id);


--
-- Name: gwt_soapui_project gwt_soapui_project_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_soapui_project
    ADD CONSTRAINT gwt_soapui_project_pkey PRIMARY KEY (id);


--
-- Name: gwt_test_component gwt_test_component_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_test_component
    ADD CONSTRAINT gwt_test_component_pkey PRIMARY KEY (id);


--
-- Name: gwt_test_instance_result gwt_test_instance_result_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_test_instance_result
    ADD CONSTRAINT gwt_test_instance_result_pkey PRIMARY KEY (id);


--
-- Name: gwt_test_instance_tm gwt_test_instance_tm_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_test_instance_tm
    ADD CONSTRAINT gwt_test_instance_tm_pkey PRIMARY KEY (id);


--
-- Name: gwt_test_step_result gwt_test_step_result_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_test_step_result
    ADD CONSTRAINT gwt_test_step_result_pkey PRIMARY KEY (id);


--
-- Name: gwt_validation_result gwt_validation_result_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_validation_result
    ADD CONSTRAINT gwt_validation_result_pkey PRIMARY KEY (id);


--
-- Name: oid_property_info oid_property_info_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.oid_property_info
    ADD CONSTRAINT oid_property_info_pkey PRIMARY KEY (id);


--
-- Name: system_configuration system_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.system_configuration
    ADD CONSTRAINT system_configuration_pkey PRIMARY KEY (id);


--
-- Name: test_step_info test_step_info_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.test_step_info
    ADD CONSTRAINT test_step_info_pkey PRIMARY KEY (id);


--
-- Name: tf_actor_integration_profile_option tf_actor_integration_profile_option_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile_option
    ADD CONSTRAINT tf_actor_integration_profile_option_pkey PRIMARY KEY (id);


--
-- Name: tf_actor_integration_profile tf_actor_integration_profile_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile
    ADD CONSTRAINT tf_actor_integration_profile_pkey PRIMARY KEY (id);


--
-- Name: tf_actor tf_actor_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor
    ADD CONSTRAINT tf_actor_pkey PRIMARY KEY (id);


--
-- Name: tf_domain tf_domain_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain
    ADD CONSTRAINT tf_domain_pkey PRIMARY KEY (id);


--
-- Name: tf_integration_profile_option tf_integration_profile_option_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_option
    ADD CONSTRAINT tf_integration_profile_option_pkey PRIMARY KEY (id);


--
-- Name: tf_integration_profile tf_integration_profile_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile
    ADD CONSTRAINT tf_integration_profile_pkey PRIMARY KEY (id);


--
-- Name: tf_transaction tf_transaction_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction
    ADD CONSTRAINT tf_transaction_pkey PRIMARY KEY (id);


--
-- Name: tm_oid tm_oid_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_oid
    ADD CONSTRAINT tm_oid_pkey PRIMARY KEY (id);


--
-- Name: tm_path tm_path_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_path
    ADD CONSTRAINT tm_path_pkey PRIMARY KEY (id);


--
-- Name: app_configuration uk_20rnkdjn5jvlvmsb5f7io4b1o; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.app_configuration
    ADD CONSTRAINT uk_20rnkdjn5jvlvmsb5f7io4b1o UNIQUE (variable);


--
-- Name: system_configuration uk_2iwxlu65fuwpbhmeg96ebawhw; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.system_configuration
    ADD CONSTRAINT uk_2iwxlu65fuwpbhmeg96ebawhw UNIQUE (name);


--
-- Name: tf_domain uk_436tct1jl8811q2xgd8bjth9q; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain
    ADD CONSTRAINT uk_436tct1jl8811q2xgd8bjth9q UNIQUE (keyword);


--
-- Name: tf_transaction uk_6nt35dm69gbrrj0aegkkqalr2; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction
    ADD CONSTRAINT uk_6nt35dm69gbrrj0aegkkqalr2 UNIQUE (keyword);


--
-- Name: gs_test_instance_participants uk_7rrch4mgjwqsbug2p4fgu9nwm; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance_participants
    ADD CONSTRAINT uk_7rrch4mgjwqsbug2p4fgu9nwm UNIQUE (server_aipo_id, server_test_instance_participants_id);


--
-- Name: tf_actor_integration_profile_option uk_8b4tb6bcp3eh7mtsucokgh7fx; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile_option
    ADD CONSTRAINT uk_8b4tb6bcp3eh7mtsucokgh7fx UNIQUE (actor_integration_profile_id, integration_profile_option_id);


--
-- Name: tm_path uk_8ufv5mgjsoifbtbq28b64r87s; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_path
    ADD CONSTRAINT uk_8ufv5mgjsoifbtbq28b64r87s UNIQUE (keyword);


--
-- Name: gwt_keystore uk_au1iprtlts849d30m0ywkt8nh; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_keystore
    ADD CONSTRAINT uk_au1iprtlts849d30m0ywkt8nh UNIQUE (label);


--
-- Name: tf_actor_integration_profile uk_b6ejd87o8v27xinqw27nn1hss; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile
    ADD CONSTRAINT uk_b6ejd87o8v27xinqw27nn1hss UNIQUE (actor_id, integration_profile_id);


--
-- Name: gwt_mock_message_gwt_mock_validation_result uk_bi7rblc3njsm7861oppjoyku2; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_mock_message_gwt_mock_validation_result
    ADD CONSTRAINT uk_bi7rblc3njsm7861oppjoyku2 UNIQUE (mockvalidationresultlist_id);


--
-- Name: tf_actor uk_fpb6vpa9bnw6cjsb1pucuuivw; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor
    ADD CONSTRAINT uk_fpb6vpa9bnw6cjsb1pucuuivw UNIQUE (keyword);


--
-- Name: gwt_keystore uk_g9iomwvsx2npntvtg7t2s5gis; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_keystore
    ADD CONSTRAINT uk_g9iomwvsx2npntvtg7t2s5gis UNIQUE (path);


--
-- Name: gwt_project_result_test_step_info uk_h13qgverov22o82yekwxlatlc; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_project_result_test_step_info
    ADD CONSTRAINT uk_h13qgverov22o82yekwxlatlc UNIQUE (teststepinfolist_id);


--
-- Name: affinity_domain uk_ia6h2gkv0i2omgnnsnwyqwnqs; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.affinity_domain
    ADD CONSTRAINT uk_ia6h2gkv0i2omgnnsnwyqwnqs UNIQUE (keyword);


--
-- Name: gwt_test_step_result_attachment_response uk_ikt38y2y0jrdjvlkmlctvfrtt; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_test_step_result_attachment_response
    ADD CONSTRAINT uk_ikt38y2y0jrdjvlkmlctvfrtt UNIQUE (attachment_id);


--
-- Name: tf_domain_integration_profiles uk_iw9qgddyj9xsshmek3gr6u588; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain_integration_profiles
    ADD CONSTRAINT uk_iw9qgddyj9xsshmek3gr6u588 UNIQUE (integration_profile_id, domain_id);


--
-- Name: gwt_soapui_project uk_jw3350klmaesinmg1rc84nvjg; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_soapui_project
    ADD CONSTRAINT uk_jw3350klmaesinmg1rc84nvjg UNIQUE (xml_file_path);


--
-- Name: gs_test_instance_participants uk_l1ffr6swiqyeuvjixaptkpsk9; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance_participants
    ADD CONSTRAINT uk_l1ffr6swiqyeuvjixaptkpsk9 UNIQUE (server_test_instance_participants_id);


--
-- Name: gwt_test_step_result_attachment_request uk_lyuddfoyqfjnp9i3dmp8miwj7; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_test_step_result_attachment_request
    ADD CONSTRAINT uk_lyuddfoyqfjnp9i3dmp8miwj7 UNIQUE (attachment_id);


--
-- Name: gwt_project_result_x_validator_input_info uk_ma5kfp0gfmdheo64upnoepqdr; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_project_result_x_validator_input_info
    ADD CONSTRAINT uk_ma5kfp0gfmdheo64upnoepqdr UNIQUE (xvalidatorinputinfolist_id);


--
-- Name: cmn_home uk_mv2quil5gwcd8bxyc76v4vyy1; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_home
    ADD CONSTRAINT uk_mv2quil5gwcd8bxyc76v4vyy1 UNIQUE (iso3_language);


--
-- Name: tf_integration_profile uk_ny4glgtyovt2w045nwct19arx; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile
    ADD CONSTRAINT uk_ny4glgtyovt2w045nwct19arx UNIQUE (keyword);


--
-- Name: gwt_soapui_project uk_o6710d2ppucfeorem9l5c37i4; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_soapui_project
    ADD CONSTRAINT uk_o6710d2ppucfeorem9l5c37i4 UNIQUE (label);


--
-- Name: gs_test_instance uk_qprd2o4wqtcw4eify2dmrvprn; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance
    ADD CONSTRAINT uk_qprd2o4wqtcw4eify2dmrvprn UNIQUE (server_test_instance_id);


--
-- Name: affinity_domain_transactions uk_r4g9ud7n9b2gx7tia9engpipe; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.affinity_domain_transactions
    ADD CONSTRAINT uk_r4g9ud7n9b2gx7tia9engpipe UNIQUE (affinity_domain_id, transaction_id);


--
-- Name: cmn_company_details uk_r6osh086b3nqbuxpswcp1hcc2; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_company_details
    ADD CONSTRAINT uk_r6osh086b3nqbuxpswcp1hcc2 UNIQUE (company_keyword);


--
-- Name: tf_integration_profile_option uk_thqc1vykug4qhn8jdij04d1bh; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_option
    ADD CONSTRAINT uk_thqc1vykug4qhn8jdij04d1bh UNIQUE (keyword);


--
-- Name: gwt_test_component_oid_property_info uk_toeo9nolljfy5cljxhlj7th28; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_test_component_oid_property_info
    ADD CONSTRAINT uk_toeo9nolljfy5cljxhlj7th28 UNIQUE (oidpropertylist_id);


--
-- Name: gwt_test_step_result_gwt_validation_result uk_tqj54a337qb33u4xrmc3ygseu; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_test_step_result_gwt_validation_result
    ADD CONSTRAINT uk_tqj54a337qb33u4xrmc3ygseu UNIQUE (validationresults_id);


--
-- Name: usage_metadata unique_usage_metadata; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usage_metadata
    ADD CONSTRAINT unique_usage_metadata UNIQUE (transaction_id, affinity_id);


--
-- Name: usage_metadata usage_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usage_metadata
    ADD CONSTRAINT usage_metadata_pkey PRIMARY KEY (id);


--
-- Name: x_validator_input_info x_validator_input_info_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.x_validator_input_info
    ADD CONSTRAINT x_validator_input_info_pkey PRIMARY KEY (id);


--
-- Name: x_validator_oids_map x_validator_oids_map_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.x_validator_oids_map
    ADD CONSTRAINT x_validator_oids_map_pkey PRIMARY KEY (project_id, index);


--
-- Name: cfg_dicom_scu_configuration fk_1uyssmhimn7bgvrpbosg76akr; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_dicom_scu_configuration
    ADD CONSTRAINT fk_1uyssmhimn7bgvrpbosg76akr FOREIGN KEY (configuration_id) REFERENCES public.cfg_configuration(id);


--
-- Name: gwt_execution fk_23dmk9d9hb2dq5vlgcbmjb4wb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_execution
    ADD CONSTRAINT fk_23dmk9d9hb2dq5vlgcbmjb4wb FOREIGN KEY (test_component_id) REFERENCES public.gwt_test_component(id);


--
-- Name: gwt_keystore fk_26nu5sogjhq58f5s5pkppukoy; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_keystore
    ADD CONSTRAINT fk_26nu5sogjhq58f5s5pkppukoy FOREIGN KEY (execution) REFERENCES public.gwt_execution(id);


--
-- Name: gs_test_instance_participants fk_2721m2mcive6uurmopwl26osu; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance_participants
    ADD CONSTRAINT fk_2721m2mcive6uurmopwl26osu FOREIGN KEY (aipo_id) REFERENCES public.tf_actor_integration_profile_option(id);


--
-- Name: gwt_custom_property_used fk_28vb0eg0vpi6wxy241leyd9f0; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_custom_property_used
    ADD CONSTRAINT fk_28vb0eg0vpi6wxy241leyd9f0 FOREIGN KEY (custom_property_id) REFERENCES public.gwt_custom_property(id);


--
-- Name: gwt_cross_validation_result fk_2e9nltgokijonxh287bfvhx3o; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_cross_validation_result
    ADD CONSTRAINT fk_2e9nltgokijonxh287bfvhx3o FOREIGN KEY (test_step_result_id) REFERENCES public.gwt_test_step_result(id);


--
-- Name: gs_test_instance_test_instance_participants fk_2k4qd5vu7dd0i5qim72ixbkqw; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance_test_instance_participants
    ADD CONSTRAINT fk_2k4qd5vu7dd0i5qim72ixbkqw FOREIGN KEY (test_instance_id) REFERENCES public.gs_test_instance(id);


--
-- Name: gwt_execution fk_2p0itd3f9t0pmqjwd8xqossjf; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_execution
    ADD CONSTRAINT fk_2p0itd3f9t0pmqjwd8xqossjf FOREIGN KEY (soapui_project_id) REFERENCES public.gwt_soapui_project(id);


--
-- Name: gs_test_instance_test_instance_participants fk_2suwyu48vy0hed31ggom84fmu; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance_test_instance_participants
    ADD CONSTRAINT fk_2suwyu48vy0hed31ggom84fmu FOREIGN KEY (test_instance_participants_id) REFERENCES public.gs_test_instance_participants(id);


--
-- Name: gwt_soapui_project fk_2wpcv463w5oxrjumpk3ddwiss; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_soapui_project
    ADD CONSTRAINT fk_2wpcv463w5oxrjumpk3ddwiss FOREIGN KEY (keystore) REFERENCES public.gwt_keystore(id);


--
-- Name: affinity_domain_transactions fk_3oy78tmux2c99fvrqkegtiq6v; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.affinity_domain_transactions
    ADD CONSTRAINT fk_3oy78tmux2c99fvrqkegtiq6v FOREIGN KEY (transaction_id) REFERENCES public.tf_transaction(id);


--
-- Name: gs_test_instance fk_3u278s3asb3u74gte7oems8hr; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance
    ADD CONSTRAINT fk_3u278s3asb3u74gte7oems8hr FOREIGN KEY (test_instance_status_id) REFERENCES public.gs_test_instance_status(id);


--
-- Name: x_validator_input_info fk_4ckd66164x6xxmelyebvkf7lx; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.x_validator_input_info
    ADD CONSTRAINT fk_4ckd66164x6xxmelyebvkf7lx FOREIGN KEY (gwt_project_result_id) REFERENCES public.gwt_project_result(id);


--
-- Name: cfg_hl7_responder_configuration fk_4f82i9wrql9oc6mnvetcy7is9; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_responder_configuration
    ADD CONSTRAINT fk_4f82i9wrql9oc6mnvetcy7is9 FOREIGN KEY (gs_system_id) REFERENCES public.gs_system(id);


--
-- Name: sys_conf_type_usages fk_4j6febpxhud8qe1uabp31nl19; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.sys_conf_type_usages
    ADD CONSTRAINT fk_4j6febpxhud8qe1uabp31nl19 FOREIGN KEY (system_configuration_id) REFERENCES public.system_configuration(id);


--
-- Name: gs_message fk_4lkwhox4s91pq6sjfftcajcn4; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_message
    ADD CONSTRAINT fk_4lkwhox4s91pq6sjfftcajcn4 FOREIGN KEY (transaction_id) REFERENCES public.tf_transaction(id);


--
-- Name: x_validator_oids_map fk_5269k7yfd6jmpnprrs3a4ajpo; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.x_validator_oids_map
    ADD CONSTRAINT fk_5269k7yfd6jmpnprrs3a4ajpo FOREIGN KEY (project_id) REFERENCES public.gwt_test_component(id);


--
-- Name: cfg_hl7_v3_initiator_configuration fk_57xerkmwir86jmw899uw3ykpf; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_v3_initiator_configuration
    ADD CONSTRAINT fk_57xerkmwir86jmw899uw3ykpf FOREIGN KEY (configuration_id) REFERENCES public.cfg_configuration(id);


--
-- Name: gwt_project_result_x_validator_input_info fk_59lljc0hss6d9wh7cfdh030ba; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_project_result_x_validator_input_info
    ADD CONSTRAINT fk_59lljc0hss6d9wh7cfdh030ba FOREIGN KEY (gwt_project_result_id) REFERENCES public.gwt_project_result(id);


--
-- Name: cfg_hl7_v3_initiator_configuration fk_5fhnf9kr45rd5m1gd9riur2ev; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_v3_initiator_configuration
    ADD CONSTRAINT fk_5fhnf9kr45rd5m1gd9riur2ev FOREIGN KEY (gs_system_id) REFERENCES public.gs_system(id);


--
-- Name: tf_actor_integration_profile fk_5ilf7sfvvixf9f51lmphnwyoa; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile
    ADD CONSTRAINT fk_5ilf7sfvvixf9f51lmphnwyoa FOREIGN KEY (integration_profile_id) REFERENCES public.tf_integration_profile(id);


--
-- Name: gwt_mock_validation_result fk_5vbs0s332uo4rm35piijp7hj5; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_mock_validation_result
    ADD CONSTRAINT fk_5vbs0s332uo4rm35piijp7hj5 FOREIGN KEY (message_instance_id) REFERENCES public.cmn_message_instance(id);


--
-- Name: tf_actor_integration_profile fk_5yubxkv7cw5mqpv5u0axd4f1x; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile
    ADD CONSTRAINT fk_5yubxkv7cw5mqpv5u0axd4f1x FOREIGN KEY (actor_id) REFERENCES public.tf_actor(id);


--
-- Name: gs_test_steps_instance fk_60pebu4cydl5h0hj12nkoisje; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_steps_instance
    ADD CONSTRAINT fk_60pebu4cydl5h0hj12nkoisje FOREIGN KEY (syslog_config_id) REFERENCES public.cfg_syslog_configuration(id);


--
-- Name: gs_message fk_60qoisu4eph1wty4x7e55co98; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_message
    ADD CONSTRAINT fk_60qoisu4eph1wty4x7e55co98 FOREIGN KEY (test_instance_participants_receiver_id) REFERENCES public.gs_test_instance_participants(id);


--
-- Name: gwt_mock_validation_result fk_66uxd5ixkkmk3g00s9n3jvlnx; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_mock_validation_result
    ADD CONSTRAINT fk_66uxd5ixkkmk3g00s9n3jvlnx FOREIGN KEY (mock_message_id) REFERENCES public.gwt_mock_message(id);


--
-- Name: gwt_test_instance_tm fk_6fncvn5ggy07tampecjjj125x; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_test_instance_tm
    ADD CONSTRAINT fk_6fncvn5ggy07tampecjjj125x FOREIGN KEY (nonce) REFERENCES public.gwt_nonce(id);


--
-- Name: cmn_message_instance_metadata fk_6pxpnca029j7ewvr7dus8q3sf; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_message_instance_metadata
    ADD CONSTRAINT fk_6pxpnca029j7ewvr7dus8q3sf FOREIGN KEY (message_instance_id) REFERENCES public.cmn_message_instance(id);


--
-- Name: cmn_receiver_console fk_6s2mr59uvd93xmp2dpkepfdvp; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_receiver_console
    ADD CONSTRAINT fk_6s2mr59uvd93xmp2dpkepfdvp FOREIGN KEY (domain_id) REFERENCES public.tf_domain(id);


--
-- Name: gwt_test_component fk_6vfc6w6gj1a4lstwm1c6ogd6x; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_test_component
    ADD CONSTRAINT fk_6vfc6w6gj1a4lstwm1c6ogd6x FOREIGN KEY (test_case_id) REFERENCES public.gwt_test_component(id);


--
-- Name: cmn_message_instance fk_6vrp0236251naw1qr1sk7y58r; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_message_instance
    ADD CONSTRAINT fk_6vrp0236251naw1qr1sk7y58r FOREIGN KEY (issuing_actor) REFERENCES public.tf_actor(id);


--
-- Name: gwt_custom_property_used fk_7v3dge33m0vldanflhx9bvllx; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_custom_property_used
    ADD CONSTRAINT fk_7v3dge33m0vldanflhx9bvllx FOREIGN KEY (sut) REFERENCES public.system_configuration(id);


--
-- Name: cmn_ip_address fk_8b3pqht8rw1vyiu1o0dm1ewtt; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_ip_address
    ADD CONSTRAINT fk_8b3pqht8rw1vyiu1o0dm1ewtt FOREIGN KEY (company_details_id) REFERENCES public.cmn_company_details(id);


--
-- Name: gs_contextual_information fk_8j7rd6ceveqykc8jl4gufcl75; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_contextual_information
    ADD CONSTRAINT fk_8j7rd6ceveqykc8jl4gufcl75 FOREIGN KEY (path) REFERENCES public.tm_path(id);


--
-- Name: gwt_execution fk_8nus684yiech8v4yypm29nqys; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_execution
    ADD CONSTRAINT fk_8nus684yiech8v4yypm29nqys FOREIGN KEY (execution) REFERENCES public.gwt_test_instance_result(id);


--
-- Name: gwt_validation_result fk_93otr4dtpahf6xba1x4vfbcdq; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_validation_result
    ADD CONSTRAINT fk_93otr4dtpahf6xba1x4vfbcdq FOREIGN KEY (test_step_result_id) REFERENCES public.gwt_test_step_result(id);


--
-- Name: gwt_custom_property_used fk_9l30gc82t28ol1jec5mtsksa1; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_custom_property_used
    ADD CONSTRAINT fk_9l30gc82t28ol1jec5mtsksa1 FOREIGN KEY (execution_id) REFERENCES public.gwt_execution(id);


--
-- Name: cfg_syslog_configuration fk_9n8k78nx4ygpn1aiv61gdhx8n; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_syslog_configuration
    ADD CONSTRAINT fk_9n8k78nx4ygpn1aiv61gdhx8n FOREIGN KEY (gs_system_id) REFERENCES public.gs_system(id);


--
-- Name: cfg_hl7_initiator_configuration fk_9uygh1fl3hk66l9fhpnov62ov; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_initiator_configuration
    ADD CONSTRAINT fk_9uygh1fl3hk66l9fhpnov62ov FOREIGN KEY (configuration_id) REFERENCES public.cfg_configuration(id);


--
-- Name: cfg_syslog_configuration fk_a5s6a6cfwata8c4iae37f30md; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_syslog_configuration
    ADD CONSTRAINT fk_a5s6a6cfwata8c4iae37f30md FOREIGN KEY (configuration_id) REFERENCES public.cfg_configuration(id);


--
-- Name: gwt_test_step_result_attachment_response fk_aotepuj24dd8fv8nkyr4ppxaq; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_test_step_result_attachment_response
    ADD CONSTRAINT fk_aotepuj24dd8fv8nkyr4ppxaq FOREIGN KEY (test_step_id) REFERENCES public.gwt_test_step_result(id);


--
-- Name: gwt_soapui_project fk_aqej0fw8d4nlkh2y9o984frgb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_soapui_project
    ADD CONSTRAINT fk_aqej0fw8d4nlkh2y9o984frgb FOREIGN KEY (gwt_project) REFERENCES public.gwt_test_component(id) ON DELETE CASCADE;


--
-- Name: cfg_dicom_scu_configuration fk_atmtmfff6dh3cfmgymfvuv32x; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_dicom_scu_configuration
    ADD CONSTRAINT fk_atmtmfff6dh3cfmgymfvuv32x FOREIGN KEY (gs_system_id) REFERENCES public.gs_system(id);


--
-- Name: cfg_dicom_scp_configuration fk_bb0i01umf0nkm6s0ntwf16jve; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_dicom_scp_configuration
    ADD CONSTRAINT fk_bb0i01umf0nkm6s0ntwf16jve FOREIGN KEY (sop_class_id) REFERENCES public.cfg_sop_class(id);


--
-- Name: gwt_mock_message_gwt_mock_validation_result fk_bi7rblc3njsm7861oppjoyku2; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_mock_message_gwt_mock_validation_result
    ADD CONSTRAINT fk_bi7rblc3njsm7861oppjoyku2 FOREIGN KEY (mockvalidationresultlist_id) REFERENCES public.gwt_mock_validation_result(id);


--
-- Name: gs_test_steps_instance fk_bq3udwuexl1gy795hpwru5dqe; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_steps_instance
    ADD CONSTRAINT fk_bq3udwuexl1gy795hpwru5dqe FOREIGN KEY (dicom_scu_config_id) REFERENCES public.cfg_dicom_scu_configuration(id);


--
-- Name: gwt_test_component fk_bxh70kqj2bd3emkrk6js8k1th; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_test_component
    ADD CONSTRAINT fk_bxh70kqj2bd3emkrk6js8k1th FOREIGN KEY (soapui_project) REFERENCES public.gwt_soapui_project(id);


--
-- Name: cfg_web_service_configuration fk_cam5r7r5pfmnofleffgk0e32f; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_web_service_configuration
    ADD CONSTRAINT fk_cam5r7r5pfmnofleffgk0e32f FOREIGN KEY (configuration_id) REFERENCES public.cfg_configuration(id);


--
-- Name: gwt_test_component fk_cjjvebeudouimg8pqqbbujfoc; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_test_component
    ADD CONSTRAINT fk_cjjvebeudouimg8pqqbbujfoc FOREIGN KEY (test_suite_id) REFERENCES public.gwt_test_component(id);


--
-- Name: cfg_hl7_responder_configuration fk_cnmpmi83qdgkf29ca5vp0nlk0; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_responder_configuration
    ADD CONSTRAINT fk_cnmpmi83qdgkf29ca5vp0nlk0 FOREIGN KEY (configuration_id) REFERENCES public.cfg_configuration(id);


--
-- Name: gs_test_steps_instance fk_d6l3ub5y7jlicc47udu6pc8gf; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_steps_instance
    ADD CONSTRAINT fk_d6l3ub5y7jlicc47udu6pc8gf FOREIGN KEY (hl7v2_responder_config_id) REFERENCES public.cfg_hl7_responder_configuration(id);


--
-- Name: gs_test_steps_instance fk_d7nna7jtx4je8ruwukta41upc; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_steps_instance
    ADD CONSTRAINT fk_d7nna7jtx4je8ruwukta41upc FOREIGN KEY (dicom_scp_config_id) REFERENCES public.cfg_dicom_scp_configuration(id);


--
-- Name: oid_property_info fk_dam2ulbbhewysqxohrcxlk1w; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.oid_property_info
    ADD CONSTRAINT fk_dam2ulbbhewysqxohrcxlk1w FOREIGN KEY (gwt_project_id) REFERENCES public.gwt_test_component(id);


--
-- Name: tf_actor_integration_profile_option fk_dd4cllr6xpdtoxp5wnjv4i2jn; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile_option
    ADD CONSTRAINT fk_dd4cllr6xpdtoxp5wnjv4i2jn FOREIGN KEY (actor_integration_profile_id) REFERENCES public.tf_actor_integration_profile(id);


--
-- Name: tf_domain_integration_profiles fk_ditnlab3yy7ipby4do2dl4k9v; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain_integration_profiles
    ADD CONSTRAINT fk_ditnlab3yy7ipby4do2dl4k9v FOREIGN KEY (integration_profile_id) REFERENCES public.tf_integration_profile(id);


--
-- Name: cmn_receiver_console fk_dvjns4jx634sntdq34baohlny; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_receiver_console
    ADD CONSTRAINT fk_dvjns4jx634sntdq34baohlny FOREIGN KEY (transaction_id) REFERENCES public.tf_transaction(id);


--
-- Name: gs_test_steps_instance fk_e47nbep6bi6div7m4h09j40yx; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_steps_instance
    ADD CONSTRAINT fk_e47nbep6bi6div7m4h09j40yx FOREIGN KEY (hl7v3_responder_config_id) REFERENCES public.cfg_hl7_v3_responder_configuration(id);


--
-- Name: gs_message fk_f0ffk1767j7rom46uil2fojou; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_message
    ADD CONSTRAINT fk_f0ffk1767j7rom46uil2fojou FOREIGN KEY (test_instance_participants_sender_id) REFERENCES public.gs_test_instance_participants(id);


--
-- Name: gwt_test_step_result fk_f3ftfw03qv1uvwcvp2vc0udmx; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_test_step_result
    ADD CONSTRAINT fk_f3ftfw03qv1uvwcvp2vc0udmx FOREIGN KEY (messages) REFERENCES public.cmn_transaction_instance(id);


--
-- Name: gs_test_instance_participants fk_g5b1negarlf7einpqd9dyxhy5; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance_participants
    ADD CONSTRAINT fk_g5b1negarlf7einpqd9dyxhy5 FOREIGN KEY (system_id) REFERENCES public.gs_system(id);


--
-- Name: cfg_web_service_configuration fk_g9fr1fk5hwvt5qd0b151x68b9; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_web_service_configuration
    ADD CONSTRAINT fk_g9fr1fk5hwvt5qd0b151x68b9 FOREIGN KEY (gs_system_id) REFERENCES public.gs_system(id);


--
-- Name: gs_test_steps_instance fk_gci5txhtlwlkonbqjdfeue6k9; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_steps_instance
    ADD CONSTRAINT fk_gci5txhtlwlkonbqjdfeue6k9 FOREIGN KEY (web_service_config_id) REFERENCES public.cfg_web_service_configuration(id);


--
-- Name: cfg_dicom_scp_configuration fk_gmhl29xoaas6axade58e6h18y; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_dicom_scp_configuration
    ADD CONSTRAINT fk_gmhl29xoaas6axade58e6h18y FOREIGN KEY (gs_system_id) REFERENCES public.gs_system(id);


--
-- Name: gwt_custom_property fk_grdcqr60pa1me2sj9y0i93p1n; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_custom_property
    ADD CONSTRAINT fk_grdcqr60pa1me2sj9y0i93p1n FOREIGN KEY (test_component_id) REFERENCES public.gwt_test_component(id);


--
-- Name: gwt_project_result_test_step_info fk_h13qgverov22o82yekwxlatlc; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_project_result_test_step_info
    ADD CONSTRAINT fk_h13qgverov22o82yekwxlatlc FOREIGN KEY (teststepinfolist_id) REFERENCES public.test_step_info(id);


--
-- Name: gwt_project_result fk_h3ctk0n9ovk2fmlj5vituawut; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_project_result
    ADD CONSTRAINT fk_h3ctk0n9ovk2fmlj5vituawut FOREIGN KEY (x_validation_result_id) REFERENCES public.gwt_cross_validation_result(id);


--
-- Name: usage_metadata fk_h6b2lqp7q4crn70h4noft57dm; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usage_metadata
    ADD CONSTRAINT fk_h6b2lqp7q4crn70h4noft57dm FOREIGN KEY (transaction_id) REFERENCES public.tf_transaction(id);


--
-- Name: tm_oid fk_hcooq91f0xb0dt3lw69f4mhrp; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_oid
    ADD CONSTRAINT fk_hcooq91f0xb0dt3lw69f4mhrp FOREIGN KEY (system_id) REFERENCES public.gs_system(id);


--
-- Name: gs_test_instance_oid fk_he8jcif34crxu9bx6bjpf70ro; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance_oid
    ADD CONSTRAINT fk_he8jcif34crxu9bx6bjpf70ro FOREIGN KEY (test_instance_id) REFERENCES public.gs_test_instance(id);


--
-- Name: gs_test_steps_instance fk_hhvsfxik1plfet5oenwrhmmsq; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_steps_instance
    ADD CONSTRAINT fk_hhvsfxik1plfet5oenwrhmmsq FOREIGN KEY (testinstance_id) REFERENCES public.gs_test_instance(id);


--
-- Name: gwt_project_result fk_hk6pdhuh7h85w9j25fbs56v3e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_project_result
    ADD CONSTRAINT fk_hk6pdhuh7h85w9j25fbs56v3e FOREIGN KEY (execution_id) REFERENCES public.gwt_execution(id);


--
-- Name: gwt_test_instance_tm fk_hp60tqynfj2v6caexhxyqtj6u; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_test_instance_tm
    ADD CONSTRAINT fk_hp60tqynfj2v6caexhxyqtj6u FOREIGN KEY (calling_tool_id) REFERENCES public.gwt_calling_tool(id);


--
-- Name: tf_actor_integration_profile_option fk_ichumu56164vfu1j44qb0uos3; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile_option
    ADD CONSTRAINT fk_ichumu56164vfu1j44qb0uos3 FOREIGN KEY (integration_profile_option_id) REFERENCES public.tf_integration_profile_option(id);


--
-- Name: gs_test_steps_instance fk_ijvev88dlp0g3wyawbewr0k98; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_steps_instance
    ADD CONSTRAINT fk_ijvev88dlp0g3wyawbewr0k98 FOREIGN KEY (hl7v2_initiator_config_id) REFERENCES public.cfg_hl7_initiator_configuration(id);


--
-- Name: gwt_test_step_result_attachment_response fk_ikt38y2y0jrdjvlkmlctvfrtt; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_test_step_result_attachment_response
    ADD CONSTRAINT fk_ikt38y2y0jrdjvlkmlctvfrtt FOREIGN KEY (attachment_id) REFERENCES public.gwt_attachement(id);


--
-- Name: cmn_transaction_instance fk_iuc2f38isetr9m12flpqj049i; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_transaction_instance
    ADD CONSTRAINT fk_iuc2f38isetr9m12flpqj049i FOREIGN KEY (transaction_id) REFERENCES public.tf_transaction(id);


--
-- Name: tf_domain_integration_profiles fk_j3fpkysr19ckcu98bb7hbj8nu; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain_integration_profiles
    ADD CONSTRAINT fk_j3fpkysr19ckcu98bb7hbj8nu FOREIGN KEY (domain_id) REFERENCES public.tf_domain(id);


--
-- Name: gs_test_instance_oid fk_j4ksfu8dbved4pd1jgcrtagqf; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_instance_oid
    ADD CONSTRAINT fk_j4ksfu8dbved4pd1jgcrtagqf FOREIGN KEY (oid_configuration_id) REFERENCES public.tm_oid(id);


--
-- Name: gwt_test_instance_result fk_jpoxek93vmpytxw0s2laa0ah; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_test_instance_result
    ADD CONSTRAINT fk_jpoxek93vmpytxw0s2laa0ah FOREIGN KEY (test_suite_id) REFERENCES public.gwt_test_component(id);


--
-- Name: gwt_test_component_oid_property_info fk_k8jea9c2kwewop7tjwl8d0a8j; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_test_component_oid_property_info
    ADD CONSTRAINT fk_k8jea9c2kwewop7tjwl8d0a8j FOREIGN KEY (gwt_test_component_id) REFERENCES public.gwt_test_component(id);


--
-- Name: gs_test_steps_instance fk_knidtr35pirurpxk347rf9l6d; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_test_steps_instance
    ADD CONSTRAINT fk_knidtr35pirurpxk347rf9l6d FOREIGN KEY (hl7v3_initiator_config_id) REFERENCES public.cfg_hl7_v3_initiator_configuration(id);


--
-- Name: cmn_transaction_instance fk_kp300ev0h6s2ocpy5j8djco4v; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_transaction_instance
    ADD CONSTRAINT fk_kp300ev0h6s2ocpy5j8djco4v FOREIGN KEY (request_id) REFERENCES public.cmn_message_instance(id);


--
-- Name: gwt_cross_validation_result fk_kvtyxc7kwx5cbeohfgkwhf8l0; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_cross_validation_result
    ADD CONSTRAINT fk_kvtyxc7kwx5cbeohfgkwhf8l0 FOREIGN KEY (gwt_project_result_id) REFERENCES public.gwt_project_result(id);


--
-- Name: cmn_transaction_instance fk_leqiasjr73fuy7ffhqmrhhsaa; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_transaction_instance
    ADD CONSTRAINT fk_leqiasjr73fuy7ffhqmrhhsaa FOREIGN KEY (response_id) REFERENCES public.cmn_message_instance(id);


--
-- Name: cfg_configuration fk_lpgg35tul90orrn8vvvtyb80r; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_configuration
    ADD CONSTRAINT fk_lpgg35tul90orrn8vvvtyb80r FOREIGN KEY (host_id) REFERENCES public.cfg_host(id);


--
-- Name: gwt_test_step_result_attachment_request fk_lyuddfoyqfjnp9i3dmp8miwj7; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_test_step_result_attachment_request
    ADD CONSTRAINT fk_lyuddfoyqfjnp9i3dmp8miwj7 FOREIGN KEY (attachment_id) REFERENCES public.gwt_attachement(id);


--
-- Name: gwt_project_result_x_validator_input_info fk_ma5kfp0gfmdheo64upnoepqdr; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_project_result_x_validator_input_info
    ADD CONSTRAINT fk_ma5kfp0gfmdheo64upnoepqdr FOREIGN KEY (xvalidatorinputinfolist_id) REFERENCES public.x_validator_input_info(id);


--
-- Name: gwt_custom_property fk_mij7168nd69ss7aq2nbqp44oh; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_custom_property
    ADD CONSTRAINT fk_mij7168nd69ss7aq2nbqp44oh FOREIGN KEY (project_id) REFERENCES public.gwt_soapui_project(id);


--
-- Name: sys_conf_type_usages fk_mvt7iq3yl2ublo7ab7rpg98s; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.sys_conf_type_usages
    ADD CONSTRAINT fk_mvt7iq3yl2ublo7ab7rpg98s FOREIGN KEY (listusages_id) REFERENCES public.usage_metadata(id);


--
-- Name: gwt_test_step_result_gwt_validation_result fk_nsdtmls8bblf4ye2gq97ugjkr; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_test_step_result_gwt_validation_result
    ADD CONSTRAINT fk_nsdtmls8bblf4ye2gq97ugjkr FOREIGN KEY (gwt_test_step_result_id) REFERENCES public.gwt_test_step_result(id);


--
-- Name: gwt_mock_message fk_nsrb2gwsuaqixu8gtf5ckwqng; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_mock_message
    ADD CONSTRAINT fk_nsrb2gwsuaqixu8gtf5ckwqng FOREIGN KEY (message_instance_id) REFERENCES public.cmn_message_instance(id);


--
-- Name: gwt_test_step_result fk_nwtrlsv48j4jiv9jh962ei4sp; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_test_step_result
    ADD CONSTRAINT fk_nwtrlsv48j4jiv9jh962ei4sp FOREIGN KEY (test_step_id) REFERENCES public.gwt_test_component(id);


--
-- Name: cfg_dicom_scp_configuration fk_o8cpt1ws21yuupo411fqdvf0t; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_dicom_scp_configuration
    ADD CONSTRAINT fk_o8cpt1ws21yuupo411fqdvf0t FOREIGN KEY (configuration_id) REFERENCES public.cfg_configuration(id);


--
-- Name: affinity_domain_transactions fk_oonw5dnptgu8fqtebob7173p5; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.affinity_domain_transactions
    ADD CONSTRAINT fk_oonw5dnptgu8fqtebob7173p5 FOREIGN KEY (affinity_domain_id) REFERENCES public.affinity_domain(id);


--
-- Name: gwt_test_component fk_p2aarfq2fm4yf8i3ajlx0mlmg; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_test_component
    ADD CONSTRAINT fk_p2aarfq2fm4yf8i3ajlx0mlmg FOREIGN KEY (project_id) REFERENCES public.gwt_test_component(id);


--
-- Name: cfg_dicom_scu_configuration fk_p47iuej292p3f2sawsexrx4m2; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_dicom_scu_configuration
    ADD CONSTRAINT fk_p47iuej292p3f2sawsexrx4m2 FOREIGN KEY (sop_class_id) REFERENCES public.cfg_sop_class(id);


--
-- Name: cfg_hl7_initiator_configuration fk_p641ssuuyy3ag6tg9uunvy1pf; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_initiator_configuration
    ADD CONSTRAINT fk_p641ssuuyy3ag6tg9uunvy1pf FOREIGN KEY (gs_system_id) REFERENCES public.gs_system(id);


--
-- Name: test_step_info fk_p7qrx93t3iro83et4ovrf19dk; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.test_step_info
    ADD CONSTRAINT fk_p7qrx93t3iro83et4ovrf19dk FOREIGN KEY (gwt_project_result_id) REFERENCES public.gwt_project_result(id);


--
-- Name: gs_contextual_information_instance fk_pa62xsmkceyisp0dclhsy36tl; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_contextual_information_instance
    ADD CONSTRAINT fk_pa62xsmkceyisp0dclhsy36tl FOREIGN KEY (test_steps_instance_id) REFERENCES public.gs_test_steps_instance(id);


--
-- Name: gwt_project_result_test_step_info fk_pepotcjwormiv6obw03rmjg52; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_project_result_test_step_info
    ADD CONSTRAINT fk_pepotcjwormiv6obw03rmjg52 FOREIGN KEY (gwt_project_result_id) REFERENCES public.gwt_project_result(id);


--
-- Name: cfg_hl7_v3_responder_configuration fk_phc7kx0qg1jjfrs9b13uaclta; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_v3_responder_configuration
    ADD CONSTRAINT fk_phc7kx0qg1jjfrs9b13uaclta FOREIGN KEY (gs_system_id) REFERENCES public.gs_system(id);


--
-- Name: gwt_test_step_result fk_pj7jew3abmdu0e7iakuh8xlcu; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_test_step_result
    ADD CONSTRAINT fk_pj7jew3abmdu0e7iakuh8xlcu FOREIGN KEY (execution_id) REFERENCES public.gwt_execution(id);


--
-- Name: usage_metadata fk_pl8yqxsqsua7w6nk02nmibrym; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usage_metadata
    ADD CONSTRAINT fk_pl8yqxsqsua7w6nk02nmibrym FOREIGN KEY (affinity_id) REFERENCES public.affinity_domain(id);


--
-- Name: cfg_hl7_v3_responder_configuration fk_pwniunln1hw1lb8tb8jp3cxd9; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cfg_hl7_v3_responder_configuration
    ADD CONSTRAINT fk_pwniunln1hw1lb8tb8jp3cxd9 FOREIGN KEY (configuration_id) REFERENCES public.cfg_configuration(id);


--
-- Name: gwt_test_step_result fk_qajs568fte6dhelgriy77mj91; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_test_step_result
    ADD CONSTRAINT fk_qajs568fte6dhelgriy77mj91 FOREIGN KEY (x_validation_result_id) REFERENCES public.gwt_cross_validation_result(id);


--
-- Name: cmn_transaction_instance fk_qosa869w00k0f6cp3u3i4i6y8; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_transaction_instance
    ADD CONSTRAINT fk_qosa869w00k0f6cp3u3i4i6y8 FOREIGN KEY (simulated_actor_id) REFERENCES public.tf_actor(id);


--
-- Name: gs_contextual_information_instance fk_r37ybyowksux2ytociytsbeij; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gs_contextual_information_instance
    ADD CONSTRAINT fk_r37ybyowksux2ytociytsbeij FOREIGN KEY (contextual_information_id) REFERENCES public.gs_contextual_information(id);


--
-- Name: gwt_keystore fk_rdbf6rdjltwfel2rivviix3y8; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_keystore
    ADD CONSTRAINT fk_rdbf6rdjltwfel2rivviix3y8 FOREIGN KEY (soapui_project) REFERENCES public.gwt_soapui_project(id);


--
-- Name: gwt_mock_message_gwt_mock_validation_result fk_rve2h9r8vl7anp3jlwvd2riqa; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_mock_message_gwt_mock_validation_result
    ADD CONSTRAINT fk_rve2h9r8vl7anp3jlwvd2riqa FOREIGN KEY (gwt_mock_message_id) REFERENCES public.gwt_mock_message(id);


--
-- Name: cmn_transaction_instance fk_s1syfhsdftpjsr8wk5pvk6idr; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_transaction_instance
    ADD CONSTRAINT fk_s1syfhsdftpjsr8wk5pvk6idr FOREIGN KEY (domain_id) REFERENCES public.tf_domain(id);


--
-- Name: gwt_test_step_result_attachment_request fk_spbgakoovyibt66qfoaqi6n4n; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_test_step_result_attachment_request
    ADD CONSTRAINT fk_spbgakoovyibt66qfoaqi6n4n FOREIGN KEY (test_step_id) REFERENCES public.gwt_test_step_result(id);


--
-- Name: gwt_soapui_messages fk_t2hs0805h5kw2k9o643bqb3c; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_soapui_messages
    ADD CONSTRAINT fk_t2hs0805h5kw2k9o643bqb3c FOREIGN KEY (gwt_test_step_result_id) REFERENCES public.gwt_test_step_result(id);


--
-- Name: gwt_test_instance_result fk_tdnm9dlf6e38lgc7okaywpq7b; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_test_instance_result
    ADD CONSTRAINT fk_tdnm9dlf6e38lgc7okaywpq7b FOREIGN KEY (test_execution_id) REFERENCES public.gwt_execution(id);


--
-- Name: cmn_receiver_console fk_tnagd28ej30pkampt9gjs28d; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_receiver_console
    ADD CONSTRAINT fk_tnagd28ej30pkampt9gjs28d FOREIGN KEY (simulated_actor_id) REFERENCES public.tf_actor(id);


--
-- Name: gwt_project_result fk_to949gxiouf1pdwoc24lqbsii; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_project_result
    ADD CONSTRAINT fk_to949gxiouf1pdwoc24lqbsii FOREIGN KEY (project_id) REFERENCES public.gwt_test_component(id);


--
-- Name: gwt_test_component_oid_property_info fk_toeo9nolljfy5cljxhlj7th28; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_test_component_oid_property_info
    ADD CONSTRAINT fk_toeo9nolljfy5cljxhlj7th28 FOREIGN KEY (oidpropertylist_id) REFERENCES public.oid_property_info(id);


--
-- Name: gwt_test_step_result_gwt_validation_result fk_tqj54a337qb33u4xrmc3ygseu; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gwt_test_step_result_gwt_validation_result
    ADD CONSTRAINT fk_tqj54a337qb33u4xrmc3ygseu FOREIGN KEY (validationresults_id) REFERENCES public.gwt_validation_result(id);


--
-- PostgreSQL database dump complete
--

