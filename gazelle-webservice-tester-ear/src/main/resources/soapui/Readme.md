
 **EN** 
 # __How to use this Soapui Project__ 

This project contain the script which allow to save messages exchanged with a mock up in a data base ("gazelle-webservice-tester", in tables cmn_transaction_instance_id_seq and cmn_message_instance_id_seq )
The script is write in test case which is save in Library test suite.
The Mock up and the other test suite are an exemples, they allow to show how script run.

####__To use this script__
* Add the test case "Save Mock Messages Exchanged", to a library or other test suite
* Fill in the properties of the test case :
	- The name of the request that you wish to save in a database
	- The name of the test case and the test suite where the request is found
	- If the request uses a mock up, indicate its name
	- The address of the database gazelle-webservice-tester
	- As well as the information on the request (Domain, Actor and Transaction) essential for the insertion in base

<br><br>	 
**FR**
# __Comment utiliser ce Projet Soapui__ 

Ce projet contient le script qui permet d'enregistrer les messages échangés avec un mock service dans une base de données ("gazelle-webservice-tester", dans les tables cmn_transaction_instance_id_seq et cmn_message_instance_id_seq)
Le script est écrit dans un cas de test qui est enregistré dans la suite de tests Library.
Le Mock up et l'autre suite de tests sont des exemples, ils permettent de montrer comment fonctionne le script.

####__Pour utiliser le script__
* Ajouter le cas de test "Save Mock Messages Exchanged" dans une suite de test librairie ou autre
* Renseigner dans les propriétés du cas de test :	
	- Le nom de la requete qu'on souhaite sauvegarder dans une base de donnée 
	- Le nom du cas de test et de la suite où ce trouve la requete
	- Si la requete utilise un mock, indiqué son nom
	- L'adresse de la base de donnée gazelle-webservice-tester
	- Ainsi que les informations sur la requete (Domaine, Actor et Transaction) indispensable pour l'insertion en base  
